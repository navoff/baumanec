// ====================== Includes ===============================
#define mega8
#define FCLK 8000000ul // �������� �������
#define FDIS 5000 // ������� �������������

#include <avr/io.h>
#include <avr/pgmspace.h>
#include "ffft.h"
#include "USARTUnit.c"

#define VER_2013

// ====================== Definition =============================

#ifdef VER_2013

#define LED32On()  ( PORTC |= (1<<PC4) )
#define LED32Off() ( PORTC &= ~(1<<PC4) )
#define LED31On()  ( PORTB |= (1<<PB0) )
#define LED31Off() ( PORTB &= ~(1<<PB0) )
#define LED30On()  ( PORTD |= (1<<PD3) )
#define LED30Off() ( PORTD &= ~(1<<PD3) )

#define LED22On()  ( PORTC |=  (1<<PC3) )
#define LED22Off() ( PORTC &= ~(1<<PC3) )
#define LED21On()  ( PORTD |=  (1<<PD7) )
#define LED21Off() ( PORTD &= ~(1<<PD7) )
#define LED20On()  ( PORTD |=  (1<<PD2) )
#define LED20Off() ( PORTD &= ~(1<<PD2) )

#define LED12On()  ( PORTC |=  (1<<PC2) )
#define LED12Off() ( PORTC &= ~(1<<PC2) )
#define LED11On()  ( PORTD |=  (1<<PD6) )
#define LED11Off() ( PORTD &= ~(1<<PD6) )
#define LED10On()  ( PORTD |=  (1<<PD1) )
#define LED10Off() ( PORTD &= ~(1<<PD1) )

#define LED02On()  ( PORTC |=  (1<<PC1) )
#define LED02Off() ( PORTC &= ~(1<<PC1) )
#define LED01On()  ( PORTD |=  (1<<PD5) )
#define LED01Off() ( PORTD &= ~(1<<PD5) )
#define LED00On()  ( PORTD |=  (1<<PD0) )
#define LED00Off() ( PORTD &= ~(1<<PD0) )


//#define LED32On()  ( PORTC |= (1<<PC1) )
//#define LED32Off() ( PORTC &= ~(1<<PC1) )
//#define LED31On()  ( PORTB |= (1<<PB0) )
//#define LED31Off() ( PORTB &= ~(1<<PB0) )
//#define LED30On()  ( PORTx |= (1<<PD3) )
//#define LED30Off() ( PORTD &= ~(1<<PD3) )
//
//#define LED22On()  ( PORTC |=  (1<<PC2) )
//#define LED22Off() ( PORTC &= ~(1<<PC2) )
//#define LED21On()  ( PORTD |=  (1<<PD7) )
//#define LED21Off() ( PORTD &= ~(1<<PD7) )
//#define LED20On()  ( PORTD |=  (1<<PD2) )
//#define LED20Off() ( PORTD &= ~(1<<PD2) )
//
//#define LED12On()  ( PORTC |=  (1<<PC3) )
//#define LED12Off() ( PORTC &= ~(1<<PC3) )
//#define LED11On()  ( PORTD |=  (1<<PD6) )
//#define LED11Off() ( PORTD &= ~(1<<PD6) )
//#define LED10On()  ( PORTD |=  (1<<PD1) )
//#define LED10Off() ( PORTD &= ~(1<<PD1) )
//
//#define LED02On()  ( PORTC |=  (1<<PC4) )
//#define LED02Off() ( PORTC &= ~(1<<PC4) )
//#define LED01On()  ( PORTD |=  (1<<PD5) )
//#define LED01Off() ( PORTD &= ~(1<<PD5) )
//#define LED00On()  ( PORTD |=  (1<<PD0) )
//#define LED00Off() ( PORTD &= ~(1<<PD0) )

#else

  #define LED02On() ( PORTC |= (1<<PC4) )
  #define LED02Off() ( PORTC &= ~(1<<PC4) )
  #define LED01On() ( PORTC |= (1<<PC3) )
  #define LED01Off() ( PORTC &= ~(1<<PC3) )
  #define LED00On() ( PORTC |= (1<<PC2) )
  #define LED00Off() ( PORTC &= ~(1<<PC2) )


  #define LED12On() ( PORTC |= (1<<PC1) )
  #define LED12Off() ( PORTC &= ~(1<<PC1) )
  #define LED11On() ( PORTC |= (1<<PC0) )
  #define LED11Off() ( PORTC &= ~(1<<PC0) )
  #define LED10On() ( PORTB |= (1<<PB0) )
  #define LED10Off() ( PORTB &= ~(1<<PB0) )

  #define LED22On() ( PORTD |= (1<<PD7) )
  #define LED22Off() ( PORTD &= ~(1<<PD7) )
  #define LED21On() ( PORTD |= (1<<PD6) )
  #define LED21Off() ( PORTD &= ~(1<<PD6) )
  #define LED20On() ( PORTD |= (1<<PD5) )
  #define LED20Off() ( PORTD &= ~(1<<PD5) )

  #define LED32On() ( PORTB |= (1<<PB7) )
  #define LED32Off() ( PORTB &= ~(1<<PB7) )
  #define LED31On() ( PORTD |= (1<<PD4) )
  #define LED31Off() ( PORTD &= ~(1<<PD4) )
  #define LED30On() ( PORTD |= (1<<PD3) )
  #define LED30Off() ( PORTD &= ~(1<<PD3) )
#endif


uint16_t time;

// ======================= Implementation ========================

void capture_wave (int16_t *buffer, uint16_t count){
#if defined(mega64)
  ADMUX = (0<<REFS1)|(1<<REFS0)|(1<<ADLAR)|
    (0<<MUX4)|(0<<MUX3)|(0<<MUX2)|(0<<MUX1)|(0<<MUX0);
#elif defined(mega128)
  ADMUX = (0<<REFS1)|(1<<REFS0)|(1<<ADLAR)|
    (0<<MUX4)|(0<<MUX3)|(0<<MUX2)|(0<<MUX1)|(0<<MUX0);
  // ��� mega8
#elif defined(mega8)
  ADMUX = (0<<REFS1)|(1<<REFS0)|(1<<ADLAR)|(0<<MUX3)|(1<<MUX2)|(0<<MUX1)|(1<<MUX0);
  ADCSRA = (1<<ADEN)|(1<<ADSC)|(1<<ADFR)|(1<<ADIF)|(0<<ADIE)|
    (0<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
#elif defined(mega48)
  ADMUX = (0<<REFS1)|(1<<REFS0)|(1<<ADLAR)|(0<<MUX3)|(1<<MUX2)|(0<<MUX1)|(1<<MUX0);
  ADCSRA = (1<<ADEN)|(1<<ADSC)|(1<<ADATE)|(1<<ADIF)|(0<<ADIE)|
    (0<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);
  ADCSRB = 0;
#endif

  while(bit_is_clear(ADCSRA, ADIF)); // �������� ���
  ADCSRA |= (1<<ADIF);
// ��� ���������� ������ ������� �������
  TCNT1=0;
  TCCR1B = (1<<CS11);
// ��� �������� ����� ���������
#if defined(mega48)
  OCR2A=(FCLK>>4)/FDIS-1;
  TIFR2 |= (1<<OCF2A);
  TCNT2=0;
  TCCR2A = (1<<WGM21)|(0<<WGM20); // CTC
  TCCR2B = (0<<CS22)|(1<<CS21)|(0<<CS20); // ������� �� 8
#else
  OCR2=(FCLK>>4)/FDIS-1;
  TIFR |= (1<<OCF2);
  TCNT2=0;
  TCCR2 = (1<<WGM21)|(0<<WGM20)|(0<<CS22)|(1<<CS21)|(0<<CS20); // ������� �� 8
#endif
  do {
    while(bit_is_clear(ADCSRA, ADIF));
    ADCSRA |= (1<<ADIF);
    *buffer++ = ADC - 32768;
#if defined mega48
    while(bit_is_clear(TIFR2, OCF2A));
    TIFR2 |= (1<<OCF2A);
#else
    while(bit_is_clear(TIFR, OCF2));
    TIFR |= (1<<OCF2);
#endif
  } while(--count);
  time = TCNT1;
  TCCR1B=0;
#if defined mega48
  TCCR2A=0;
  TCCR2B=0;
#else
  TCCR2=0;
#endif
  ADCSRA = 0;
}
int16_t capture[FFT_N];
complex_t bfly_buff[FFT_N];
uint16_t spektrum[FFT_N/2];

int main (void){
  DDRD |= (1<<PD7)|(1<<PD6)|(1<<PD5)|(1<<PD4)|(1<<PD3)|(1<<PD2)|(1<<PD1)|(1<<PD0);
  DDRB |= (1<<PB0)|(1<<PB7);
  DDRC |= (1<<PC0)|(1<<PC1)|(1<<PC2)|(1<<PC3)|(1<<PC4);

//  LED02On();
//  LED01On();
//  LED00On();
//  while(1);

//  LED12On();
//  LED11On();
//  LED10On();
//  while(1);

//  LED22On();
//  LED21On();
//  LED20On();
//  while(1);

//  LED32On();
//  LED31On();
//  LED30On();
//  while(1);

  while(1){
    capture_wave(capture, FFT_N);
    fft_input(capture, bfly_buff);
    fft_execute(bfly_buff);

    fft_output(bfly_buff, spektrum);

    uint32_t qqq=spektrum[2];
    if (qqq>400) LED02On(); else LED02Off();
    if (qqq>150) LED01On(); else LED01Off();
    if (qqq>50) LED00On(); else LED00Off();

    qqq = spektrum[4];
    if (qqq>400) LED12On(); else LED12Off();
    if (qqq>150) LED11On(); else LED11Off();
    if (qqq>50) LED10On(); else LED10Off();

    qqq = spektrum[7];

    if (qqq>400) LED22On(); else LED22Off();
    if (qqq>150) LED21On(); else LED21Off();
    if (qqq>50) LED20On(); else LED20Off();

    qqq = spektrum[10];

    if (qqq>400) LED32On(); else LED32Off();
    if (qqq>150) LED31On(); else LED31Off();
    if (qqq>50) LED30On(); else LED30Off();

#if defined mega48
    if (bit_is_set(UCSR0A,RXC0))
      switch (UDR0){
#else
    if (bit_is_set(UCSRA,RXC))
      switch (UDR){
#endif
        case 's':
          USARTTran((uint8_t *)spektrum,FFT_N);

          uint32_t USARTbuf = (FCLK>>3)*FFT_N/time/2;
          USARTTran((uint8_t *) &USARTbuf,3);

          USARTbuf = FFT_N/2;
          USARTTran((uint8_t *) &USARTbuf,2);
        break;
      }
  }
}
