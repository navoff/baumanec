// ====================== Definition =============================

// ======================= Function ==============================

// ======================= Implementation ========================
/*
void USARTInit (void){
	UCSR0A |= (1<<U2X);
	UCSR0B = (0<<RXCIE)| // ���������� ���������� ��� ���������� ������
			(0<<TXCIE)| // ������ ���������� ��� ���������� ��������
			(0<<UDRIE)| // ������ ���������� ��� ������� ������
			(1<<RXEN)|(1<<TXEN)| // ���������� ������ � ��������
			(0<<UCSZ2)| // 8 ��� ������
			(0<<RXB8)|(0<<TXB8);
	UCSR0C = (0<<UMSEL)| // ����������� �����
			(0<<UPM1)|(0<<UPM0)| // �������� ���������
			(0<<USBS)| // ��������� 1-�� ��������� ����
			(1<<UCSZ1)|(1<<UCSZ0)| // 8 ��� ������
			(0<<UCPOL);
	UBRR0H = 0;
	UBRR0L = 3;
}
void USARTTran (uint8_t *buf,uint16_t size){
	while (size--){ // ���� �� ��������� ��� �����
		UDR0=*buf++;
		while (!(UCSR0A&(1<<TXC))){} // ����� ��������� �������� �����
		UCSR0A |= (1<<TXC); // �������� ����
	}
}
void USARTSendChar (uint8_t data){
	UDR0 = data;
	while (!(UCSR0A&(1<<TXC))){} // ����� ��������� �������� �����
	UCSR0A |= (1<<TXC); // �������� ����
}
uint8_t USARTGetChar (void){
	while (bit_is_clear(UCSR0A,RXC)){}
	return UDR0;
}
*/
#if defined mega8
void USARTInit (void){
	UCSRA |= (1<<U2X);
	UCSRB = (0<<RXCIE)| // ���������� ���������� ��� ���������� ������
			(0<<TXCIE)| // ������ ���������� ��� ���������� ��������
			(0<<UDRIE)| // ������ ���������� ��� ������� ������
			(1<<RXEN)|(1<<TXEN)| // ���������� ������ � ��������
			(0<<UCSZ2)| // 8 ��� ������
			(0<<RXB8)|(0<<TXB8);
	UCSRC = (1<<URSEL)| // ��������� � �������� UCSRC
			(0<<UMSEL)| // ����������� �����
			(0<<UPM1)|(0<<UPM0)| // �������� ���������
			(0<<USBS)| // ��������� 1-�� ��������� ����
			(1<<UCSZ1)|(1<<UCSZ0)| // 8 ��� ������
			(0<<UCPOL);
	UBRRH = 0;
	UBRRL = 7;
}
void USARTTran (uint8_t *buf,uint16_t size){
	while (size--){ // ���� �� ��������� ��� �����
		UDR=*buf++;
		while (!(UCSRA&(1<<TXC))){} // ����� ��������� �������� �����
		UCSRA |= (1<<TXC); // �������� ����
	}
}
void USARTSendChar (uint8_t data){
	UDR = data;
	while (!(UCSRA&(1<<TXC))){} // ����� ��������� �������� �����
	UCSRA |= (1<<TXC); // �������� ����
}
uint8_t USARTGetChar (void){
	while (bit_is_clear(UCSRA,RXC)){}
	return UDR;
}
#elif defined mega48
void USARTInit (void){
	UCSR0A |= (1<<U2X0);
	UCSR0B = (0<<RXCIE0)| // ���������� ���������� ��� ���������� ������
			(0<<TXCIE0)| // ������ ���������� ��� ���������� ��������
			(0<<UDRIE0)| // ������ ���������� ��� ������� ������
			(1<<RXEN0)|(1<<TXEN0)| // ���������� ������ � ��������
			(0<<UCSZ02)| // 8 ��� ������
			(0<<RXB80)|(0<<TXB80);
	UCSR0C = (0<<UMSEL01)| // ����������� �����
			(0<<UMSEL00)| // ����������� �����
			(0<<UPM01)|(0<<UPM00)| // �������� ���������
			(0<<USBS0)| // ��������� 1-�� ��������� ����
			(1<<UCSZ01)|(1<<UCSZ00)| // 8 ��� ������
			(0<<UCPOL0);
	UBRR0H = 0;
	UBRR0L = 3; // 250000
}
void USARTTran (uint8_t *buf,uint16_t size){
	while (size--){ // ���� �� ��������� ��� �����
		UDR0=*buf++;
		while (!(UCSR0A&(1<<TXC0))){} // ����� ��������� �������� �����
		UCSR0A |= (1<<TXC0); // �������� ����
	}
}
void USARTSendChar (uint8_t data){
	UDR0 = data;
	while (!(UCSR0A&(1<<TXC0))){} // ����� ��������� �������� �����
	UCSR0A |= (1<<TXC0); // �������� ����
}
uint8_t USARTGetChar (void){
	while (bit_is_clear(UCSR0A,RXC0)){}
	return UDR0;
}
#endif

//======================== Interuption ===========================
