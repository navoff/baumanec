/**
 * @file main.c
 * @brief
 * @author ������� �.�.
 * @date 10.06.2011
 * @addtogroup
 * @{
*/

#include <stdint.h>

#include <avr\io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

//================================= Definition ================================

#define LEDS_PLAYER_AMOUNT              4

// ������ �����, �������� ��� ������� � 2013-��.
// ���������� ������������� �����������: ��� ������� ���������� � ������ ����.
//#define VER_2013

#ifdef VER_2013
  #define LED0_ON()       PORTB |= (1<<PB2)
  #define LED0_OFF()      PORTB &= ~(1<<PB2)
  #define LED1_ON()       PORTB |= (1<<PB1)
  #define LED1_OFF()      PORTB &= ~(1<<PB1)
  #define LED2_ON()       PORTD |= (1<<PD4)
  #define LED2_OFF()      PORTD &= ~(1<<PD4)
  #define LED3_ON()       PORTB |= (1<<PB6)
  #define LED3_OFF()      PORTB &= ~(1<<PB6)
  #define LED4_ON()       PORTB |= (1<<PB7)
  #define LED4_OFF()      PORTB &= ~(1<<PB7)
  #define LED5_ON()       PORTD |= (1<<PD5)
  #define LED5_OFF()      PORTD &= ~(1<<PD5)
  #define LED6_ON()       PORTD |= (1<<PD6)
  #define LED6_OFF()      PORTD &= ~(1<<PD6)
  #define LED7_ON()       PORTD |= (1<<PD7)
  #define LED7_OFF()      PORTD &= ~(1<<PD7)
  #define LED8_ON()       PORTB |= (1<<PB0)
  #define LED8_OFF()      PORTB &= ~(1<<PB0)
#else
  #define LED0_ON()       PORTD |= (1<<PD4)
  #define LED0_OFF()      PORTD &= ~(1<<PD4)
  #define LED1_ON()       PORTB |= (1<<PB6)
  #define LED1_OFF()      PORTB &= ~(1<<PB6)
  #define LED2_ON()       PORTB |= (1<<PB7)
  #define LED2_OFF()      PORTB &= ~(1<<PB7)
  #define LED3_ON()       PORTD |= (1<<PD5)
  #define LED3_OFF()      PORTD &= ~(1<<PD5)
  #define LED4_ON()       PORTD |= (1<<PD6)
  #define LED4_OFF()      PORTD &= ~(1<<PD6)
  #define LED5_ON()       PORTD |= (1<<PD7)
  #define LED5_OFF()      PORTD &= ~(1<<PD7)
  #define LED6_ON()       PORTB |= (1<<PB0)
  #define LED6_OFF()      PORTB &= ~(1<<PB0)
  #define LED7_ON()       PORTB |= (1<<PB1)
  #define LED7_OFF()      PORTB &= ~(1<<PB1)
  #define LED8_ON()       PORTB |= (1<<PB2)
  #define LED8_OFF()      PORTB &= ~(1<<PB2)
#endif

#if defined(__AVR_ATmega48__)

#elif defined(__AVR_ATmega8__)
  #define EXTERN_INT_EN()             GICR |= (1<<INT0)|(1<<INT1)
  #define EXTERN_INT_DIS()            GICR &= ~((1<<INT0)|(1<<INT1))
  #define EXTERN_INT_FLAG_RESET()     GIFR = (1<<INTF1)|(1<<INTF0)
#else
  #error MCU not defined
#endif

//================================ Declaration ================================

void led_all_off (void);
void led_all_on (void);
void led_pl0_on (void);
void led_pl0_off (void);
void led_pl1_on (void);
void led_pl1_off (void);
uint8_t led_refresh (uint8_t idx0, uint8_t idx1);

volatile struct{
  enum{
    GAME_ST__RESTART,
    GAME_ST__WAIT,
    GAME_ST__PREPARE,
    GAME_ST__IN_PROG,
  }state;
  uint8_t player_0_idx;
  uint8_t player_1_idx;
  uint16_t wait_timer;
}game;

// ============================== Implementation ==============================

int main(void){

  DDRB |= (1<<PB7)|(1<<PB6)|(1<<PB0)|(1<<PB1)|(1<<PB2);
  DDRD |= (1<<PD7)|(1<<PD6)|(1<<PD5)|(1<<PD4);

  PORTD |= (1<<PD2)|(1<<PD3)|(1<<PD0); // ������������� ���������

#if defined(__AVR_ATmega48__)
  EICRA |= (1<<ISC11)|(0<<ISC10)|(1<<ISC01)|(0<<ISC00);
#elif defined(__AVR_ATmega8__)
  MCUCR |= (1<<ISC11)|(0<<ISC10)|(1<<ISC01)|(0<<ISC00);
#endif

  /*
   * ��������� �������, ������������� ��� ������������ �������� �����
   * ������� ������.
   */
  OCR2 = 80;
  TCCR2 |= (1<<WGM21)|(1<<CS20);

  game.state = GAME_ST__RESTART;

  sei();
  uint8_t buf_pin = 0;
  while(1)
  {
    uint8_t btn_press = 0;
    if (bit_is_set(PIND,PD0))
    {
      buf_pin = 1;
    }
    if (bit_is_clear(PIND,PD0) && (buf_pin == 1))
    {
      buf_pin = 0;
      btn_press = 1;
    }
    switch (game.state)
    {
      case GAME_ST__RESTART:
        EXTERN_INT_DIS();
        led_all_on();
        game.state = GAME_ST__WAIT;
      break;

      case GAME_ST__WAIT:
        if (btn_press)
        {
          game.wait_timer = TCNT2 + 20;
          led_all_off();
          game.state = GAME_ST__PREPARE;
        }
      break;

      case GAME_ST__PREPARE:
        if (btn_press)
        {
          game.state = GAME_ST__RESTART;
          break;
        }

        if (game.wait_timer)
        {
          game.wait_timer--;
          _delay_ms(50);
        }
        else
        {
          game.player_0_idx = LEDS_PLAYER_AMOUNT + 1;
          game.player_1_idx = LEDS_PLAYER_AMOUNT + 1;

          EXTERN_INT_FLAG_RESET();
          EXTERN_INT_EN();

          game.state = GAME_ST__IN_PROG;
        }
      break;

      case GAME_ST__IN_PROG:
        if (btn_press)
        {
          game.state = GAME_ST__RESTART;
          break;
        }
        if (led_refresh(game.player_0_idx, game.player_1_idx))
        {
          game.state = GAME_ST__RESTART;
          break;
        }
      break;
    }
  }
}

ISR(INT1_vect)
{
  if (game.player_1_idx > 0)
  {
    game.player_0_idx++;
    game.player_1_idx--;
  }
}

ISR(INT0_vect)
{
  if (game.player_0_idx > 0)
  {
    game.player_1_idx++;
    game.player_0_idx--;
  }
}

uint8_t led_refresh (uint8_t idx0, uint8_t idx1)
{
  uint8_t res = 0;
  led_all_off();

  // ���� ������ ����� ��������
  if (idx0 == 0)
  {
    led_pl0_on();
    _delay_ms(300);
    led_pl0_off();
    _delay_ms(300);
    led_pl0_on();
    _delay_ms(300);
    led_pl0_off();
    _delay_ms(300);
    led_pl0_on();
    _delay_ms(300);
    led_pl0_off();
    res = 1;
  }
  // ���� ������ ����� ��������
  else if (idx1 == 0)
  {
    led_pl1_on();
    _delay_ms(300);
    led_pl1_off();
    _delay_ms(300);
    led_pl1_on();
    _delay_ms(300);
    led_pl1_off();
    _delay_ms(300);
    led_pl1_on();
    _delay_ms(300);
    led_pl1_off();
    res = 1;
  }
  else
  {
    switch (idx0)
    {
      case 1: LED0_ON(); break;
      case 2: LED1_ON(); break;
      case 3: LED2_ON(); break;
      case 4: LED3_ON(); break;
      case 5: LED4_ON(); break;
      case 6: LED5_ON(); break;
      case 7: LED6_ON(); break;
      case 8: LED7_ON(); break;
      case 9: LED8_ON(); break;
    }
  }

  return res;
}

void led_all_off (void)
{
  LED0_OFF();
  LED1_OFF();
  LED2_OFF();
  LED3_OFF();
  LED4_OFF();
  LED5_OFF();
  LED6_OFF();
  LED7_OFF();
  LED8_OFF();
}

void led_all_on (void)
{
  LED0_ON();
  LED1_ON();
  LED2_ON();
  LED3_ON();
  LED4_ON();
  LED5_ON();
  LED6_ON();
  LED7_ON();
  LED8_ON();
}

void led_pl0_on (void)
{
  LED0_ON();
  LED1_ON();
  LED2_ON();
  LED3_ON();
}

void led_pl0_off (void)
{
  LED0_OFF();
  LED1_OFF();
  LED2_OFF();
  LED3_OFF();
}

void led_pl1_on (void)
{
  LED5_ON();
  LED6_ON();
  LED7_ON();
  LED8_ON();
}

void led_pl1_off (void)
{
  LED5_OFF();
  LED6_OFF();
  LED7_OFF();
  LED8_OFF();
}

/** @} */
