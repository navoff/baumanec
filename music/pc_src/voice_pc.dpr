program voice_pc;

uses
  Vcl.Forms,
  main in 'main.pas' {Form1},
  CPort in 'CPort\CPort.pas',
  CPortCtl in 'CPort\CPortCtl.pas',
  CPortEsc in 'CPort\CPortEsc.pas',
  CPortSetup in 'CPort\CPortSetup.pas' {ComSetupFrm},
  CPortTrmSet in 'CPort\CPortTrmSet.pas' {ComTrmSetForm};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
