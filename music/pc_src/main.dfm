object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'voice_pc'
  ClientHeight = 555
  ClientWidth = 478
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  DesignSize = (
    478
    555)
  PixelsPerInch = 96
  TextHeight = 13
  object ComLed1: TComLed
    Left = 148
    Top = 0
    Width = 25
    Height = 25
    LedSignal = lsConn
    Kind = lkRedLight
  end
  object m_log: TMemo
    Left = 0
    Top = 368
    Width = 479
    Height = 185
    Anchors = [akLeft, akTop, akRight, akBottom]
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object ComComboBox1: TComComboBox
    Left = 0
    Top = 0
    Width = 145
    Height = 21
    Style = csDropDownList
    ItemIndex = -1
    TabOrder = 1
    OnDropDown = ComComboBox1DropDown
  end
  object btn_open_port: TButton
    Left = 176
    Top = 0
    Width = 87
    Height = 25
    Caption = #1054#1090#1082#1088#1099#1090#1100
    TabOrder = 2
    OnClick = btn_open_portClick
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 28
    Width = 393
    Height = 189
    ActivePage = TabSheet2
    TabOrder = 3
    object TabSheet1: TTabSheet
      Caption = #1043#1086#1083#1086#1089#1086#1074#1086#1081' '#1092#1072#1081#1083
      ExplicitWidth = 329
      ExplicitHeight = 169
      DesignSize = (
        385
        161)
      object Label1: TLabel
        Left = 4
        Top = 0
        Width = 96
        Height = 13
        Caption = #1055#1072#1087#1082#1072' '#1089' '#1092#1086#1085#1077#1084#1072#1084#1080':'
      end
      object Label2: TLabel
        Left = 4
        Top = 40
        Width = 94
        Height = 13
        Caption = #1055#1072#1087#1082#1072' '#1089' '#1087#1088#1086#1077#1082#1090#1086#1084':'
      end
      object Label3: TLabel
        Left = 4
        Top = 80
        Width = 141
        Height = 13
        Caption = #1055#1072#1087#1082#1072' '#1089' '#1075#1086#1083#1086#1089#1086#1074#1099#1084' '#1092#1072#1081#1083#1086#1084':'
      end
      object AudioDirEdit: TEdit
        Left = 0
        Top = 16
        Width = 353
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
        ExplicitWidth = 253
      end
      object Button1: TButton
        Left = 0
        Top = 124
        Width = 385
        Height = 33
        Anchors = [akLeft, akTop, akRight]
        Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1075#1086#1083#1086#1089#1086#1074#1086#1081' '#1092#1072#1081#1083
        TabOrder = 1
        OnClick = Button1Click
        ExplicitWidth = 329
      end
      object ProjectDirEdit: TEdit
        Left = 0
        Top = 56
        Width = 353
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 2
        ExplicitWidth = 253
      end
      object VoiceDirEdit: TEdit
        Left = 0
        Top = 96
        Width = 353
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 3
        ExplicitWidth = 253
      end
      object SelectAudioDirButton: TButton
        Left = 356
        Top = 14
        Width = 29
        Height = 25
        Anchors = [akTop, akRight]
        Caption = '...'
        TabOrder = 4
        OnClick = SelectAudioDirButtonClick
        ExplicitLeft = 256
      end
      object SelectProjectDirButton: TButton
        Left = 356
        Top = 54
        Width = 29
        Height = 25
        Anchors = [akTop, akRight]
        Caption = '...'
        TabOrder = 5
        OnClick = SelectProjectDirButtonClick
        ExplicitLeft = 256
      end
      object SelectVoiceDirButton: TButton
        Left = 356
        Top = 94
        Width = 29
        Height = 25
        Anchors = [akTop, akRight]
        Caption = '...'
        TabOrder = 6
        OnClick = SelectVoiceDirButtonClick
        ExplicitLeft = 256
      end
    end
    object TabSheet2: TTabSheet
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100
      ImageIndex = 1
      ExplicitWidth = 281
      ExplicitHeight = 165
      DesignSize = (
        385
        161)
      object Label4: TLabel
        Left = 4
        Top = 0
        Width = 86
        Height = 13
        Caption = #1043#1086#1083#1086#1089#1086#1074#1086#1081' '#1092#1072#1081#1083':'
      end
      object VoiceFileName: TEdit
        Left = 0
        Top = 16
        Width = 353
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
        ExplicitWidth = 305
      end
      object Button2: TButton
        Left = 356
        Top = 14
        Width = 29
        Height = 25
        Anchors = [akTop, akRight]
        Caption = '...'
        TabOrder = 1
        OnClick = Button2Click
        ExplicitLeft = 308
      end
      object btn_load_voice_file: TButton
        Left = 0
        Top = 64
        Width = 385
        Height = 33
        Anchors = [akLeft, akTop, akRight]
        Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1075#1086#1083#1086#1089#1086#1074#1086#1081' '#1092#1072#1081#1083
        TabOrder = 2
        OnClick = btn_load_voice_fileClick
        ExplicitWidth = 349
      end
      object ProgressBar1: TProgressBar
        Left = 0
        Top = 44
        Width = 385
        Height = 16
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 3
        ExplicitWidth = 349
      end
    end
    object TabSheet3: TTabSheet
      Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1086
      ImageIndex = 2
      ExplicitLeft = 0
      object FlashGetStatusRegButton: TButton
        Left = 4
        Top = 4
        Width = 119
        Height = 25
        Caption = #1057#1090#1072#1090#1091#1089#1085#1099#1081' '#1088#1077#1075#1080#1089#1090#1088
        TabOrder = 0
        OnClick = FlashGetStatusRegButtonClick
      end
      object FlashEraseButton: TButton
        Left = 4
        Top = 32
        Width = 119
        Height = 25
        Caption = #1054#1095#1080#1089#1090#1080#1090#1100' Flash'
        TabOrder = 1
        OnClick = FlashEraseButtonClick
      end
      object FlashTestEraseReadButton: TButton
        Left = 4
        Top = 60
        Width = 173
        Height = 25
        Caption = #1058#1077#1089#1090': '#1086#1095#1080#1089#1090#1080#1090#1100'+ '#1087#1088#1086#1095#1080#1090#1072#1090#1100
        TabOrder = 2
        OnClick = FlashTestEraseReadButtonClick
      end
      object FlashTestWriteReadButton: TButton
        Left = 4
        Top = 88
        Width = 173
        Height = 25
        Caption = #1058#1077#1089#1090': '#1079#1072#1087#1080#1089#1072#1090#1100'+ '#1087#1088#1086#1095#1080#1090#1072#1090#1100
        TabOrder = 3
        OnClick = FlashTestWriteReadButtonClick
      end
      object GetCheckSumButton: TButton
        Left = 4
        Top = 116
        Width = 119
        Height = 25
        Caption = #1050#1086#1085#1090#1088#1086#1083#1100#1085#1072#1103' '#1089#1091#1084#1084#1072
        TabOrder = 4
        OnClick = GetCheckSumButtonClick
      end
      object ReadAddressButton: TButton
        Left = 204
        Top = 4
        Width = 77
        Height = 25
        Caption = #1055#1088#1086#1095#1080#1090#1072#1090#1100':'
        TabOrder = 5
        OnClick = ReadAddressButtonClick
      end
      object FlashReadAddressEdit: TEdit
        Left = 284
        Top = 8
        Width = 101
        Height = 21
        TabOrder = 6
        Text = '0'
      end
    end
  end
  object BotRxLog: TMemo
    Left = 0
    Top = 220
    Width = 477
    Height = 145
    Anchors = [akLeft, akRight, akBottom]
    ScrollBars = ssVertical
    TabOrder = 4
  end
  object Button3: TButton
    Left = 396
    Top = 192
    Width = 81
    Height = 25
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100
    TabOrder = 5
    OnClick = Button3Click
  end
  object OpenDialog: TOpenDialog
    Left = 80
    Top = 416
  end
  object com_port: TComPort
    BaudRate = br9600
    Port = 'COM1'
    Parity.Bits = prNone
    StopBits = sbOneStopBit
    DataBits = dbEight
    Events = [evRxChar, evTxEmpty, evRxFlag, evRing, evBreak, evCTS, evDSR, evError, evRLSD, evRx80Full]
    FlowControl.OutCTSFlow = False
    FlowControl.OutDSRFlow = False
    FlowControl.ControlDTR = dtrDisable
    FlowControl.ControlRTS = rtsDisable
    FlowControl.XonXoffOut = False
    FlowControl.XonXoffIn = False
    StoredProps = [spBasic]
    TriggersOnRxChar = True
    OnRxChar = com_portRxChar
    Left = 4
    Top = 368
  end
end
