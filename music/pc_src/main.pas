unit main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, FileCtrl, Vcl.ComCtrls,
  CPortCtl, CPort, Vcl.ExtCtrls, Vcl.Buttons;

type
  TForm1 = class(TForm)
    m_log: TMemo;
    OpenDialog: TOpenDialog;
    com_port: TComPort;
    ComComboBox1: TComComboBox;
    ComLed1: TComLed;
    btn_open_port: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    AudioDirEdit: TEdit;
    Button1: TButton;
    Label1: TLabel;
    Label2: TLabel;
    ProjectDirEdit: TEdit;
    VoiceDirEdit: TEdit;
    Label3: TLabel;
    SelectAudioDirButton: TButton;
    SelectProjectDirButton: TButton;
    SelectVoiceDirButton: TButton;
    VoiceFileName: TEdit;
    Button2: TButton;
    btn_load_voice_file: TButton;
    ProgressBar1: TProgressBar;
    TabSheet3: TTabSheet;
    FlashGetStatusRegButton: TButton;
    FlashEraseButton: TButton;
    FlashTestEraseReadButton: TButton;
    FlashTestWriteReadButton: TButton;
    Label4: TLabel;
    BotRxLog: TMemo;
    Button3: TButton;
    GetCheckSumButton: TButton;
    ReadAddressButton: TButton;
    FlashReadAddressEdit: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure btn_load_voice_fileClick(Sender: TObject);
    procedure btn_open_portClick(Sender: TObject);
    procedure ComComboBox1DropDown(Sender: TObject);
    procedure com_portRxChar(Sender: TObject; Count: Integer);
    procedure FlashGetStatusRegButtonClick(Sender: TObject);
    procedure FlashEraseButtonClick(Sender: TObject);
    procedure FlashTestEraseReadButtonClick(Sender: TObject);
    procedure FlashTestWriteReadButtonClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure SelectAudioDirButtonClick(Sender: TObject);
    procedure SelectProjectDirButtonClick(Sender: TObject);
    procedure SelectVoiceDirButtonClick(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure GetCheckSumButtonClick(Sender: TObject);
    procedure ReadAddressButtonClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

type packet_t = packed record
  code: byte;
  data: array [0..7] of byte;
  crc: byte;
end;

const FIFO_RX_PACKET_BUF_SIZE = 10;

type fifo_rx_packet_t = packed record
  buf: array [0..FIFO_RX_PACKET_BUF_SIZE-1] of packet_t;
  tail: integer;
  head: integer;
  idx: integer;
  idx_inside_packet: integer;
end;

var
  Form1: TForm1;
  fifo_rx_packet: fifo_rx_packet_t;

const
  CMD__FLASH_STATUS               = $01;
  CMD__FLASH_ERASE                = $02;
  CMD__FLASH_TEST_ERASE_READ      = $03;
  CMD__FLASH_TEST_WRITE_READ      = $04;
  CMD__FLASH_READ                 = $06;
  CMD__GET_CHECK_SUM              = $0F;
  CMD__START_PRG_FLASH            = $10;
  CMD__PRG_BUF                    = $11;
  CMD__STOP_PRG_FLASH             = $12;
  CMD__ERROR                      = $FF;

  ERR_OK = $00;
  ERR_BUSY = $01;

  FLASH_PAGE_SIZE = 512;
  FLASH_PAGE_AMOUNT = 4096;
  FLASH_SIZE = FLASH_PAGE_AMOUNT * FLASH_PAGE_SIZE;

implementation

{$R *.dfm}

procedure fifo_rx_packet_purge;
begin
  fifo_rx_packet.tail := 0;
  fifo_rx_packet.head := 0;
  fifo_rx_packet.idx := 0;
  fifo_rx_packet.idx_inside_packet := 0;
end;

function fifo_rx_packet_get_one(var packet: packet_t): boolean;
begin
  if (fifo_rx_packet.idx = 0) then begin
    Result := false;
    exit;
  end;

  packet := fifo_rx_packet.buf[fifo_rx_packet.tail];

  inc(fifo_rx_packet.tail);
  if (fifo_rx_packet.tail >= FIFO_RX_PACKET_BUF_SIZE) then begin
    fifo_rx_packet.tail := 0;
  end;
  dec(fifo_rx_packet.idx);
  Result := true;
end;

procedure get_crc(var packet: packet_t);
var
  i: integer;
begin
  packet.crc := packet.code;
  for i := 0 to sizeof(packet.data) - 1 do begin
    packet.crc := packet.crc xor packet.data[i];
  end;
end;

function start_prg_flash: boolean;
var
  tx_packet, rx_packet: packet_t;
  i: integer;
begin

  tx_packet.code := CMD__START_PRG_FLASH;
  for i := 0 to sizeof(tx_packet.data) - 1 do begin
    tx_packet.data[i] := $ff;
  end;
  get_crc(tx_packet);

  fifo_rx_packet_purge;
  form1.com_port.Write(tx_packet,sizeof(tx_packet));
  for i := 0 to 30000 do
  begin
    Application.ProcessMessages;
    if (fifo_rx_packet_get_one(rx_packet)) then
    begin
      if (rx_packet.code = CMD__START_PRG_FLASH) then
      begin
        if (rx_packet.data[0] = ERR_BUSY) then
        begin
          Form1.m_log.Lines.Add(TimeToStr(Now) + ': �������� ������ �������.');
        end
        else if (rx_packet.data[0] = ERR_OK) then
        begin
          Result := True;
          exit;
        end;
      end;
    end;
    sleep(1);
  end;

  Result := False;
end;

function stop_prg_flash: boolean;
var
  tx_packet, rx_packet: packet_t;
  i: integer;
begin

  tx_packet.code := CMD__STOP_PRG_FLASH;
  for i := 0 to sizeof(tx_packet.data) - 1 do begin
    tx_packet.data[i] := $ff;
  end;
  get_crc(tx_packet);

  fifo_rx_packet_purge;
  form1.com_port.Write(tx_packet,sizeof(tx_packet));
  for i := 0 to 1000 do begin
    Application.ProcessMessages;
    if (fifo_rx_packet_get_one(rx_packet)) then
    begin
      if (rx_packet.code = CMD__STOP_PRG_FLASH) then
      begin
        if (rx_packet.data[0] = ERR_OK) then begin
          Result := True;
          exit;
        end;
      end;
    end;
    sleep(1);
  end;
  Result := true;
end;

function UpperCase(s: string): string;
var
  i: integer;
begin
  result := s;
  for i := 1 to length(result) do
    if (result[i] in ['a'..'z', '�'..'�']) then
      result[i] := chr(ord(result[i]) - 32);
end;

procedure TForm1.btn_load_voice_fileClick(Sender: TObject);
var
  VoiceFile: File of byte;
  tx_packet: array[0..100] of packet_t;
  i, j, idx: integer;
  rx_packet: packet_t;
begin
  if (not FileExists(VoiceFileName.Text)) then begin
    m_log.Lines.Add('����� � ��������� ������ �� ����������!!!');
    exit;
  end;

  if (not com_port.Connected) then begin
    m_log.Lines.Add('���� �� ������!');
    exit;
  end;

  ProgressBar1.Position := 0;

  btn_load_voice_file.Enabled := False;
  if (not start_prg_flash()) then
  begin
    btn_load_voice_file.Enabled := True;
    m_log.Lines.Add('�� ������� ������ ��������!');
    exit;
  end;
  m_log.Lines.Add(TimeToStr(Now) + ': ������ ������� �������.');

  btn_load_voice_file.Enabled := True;

  AssignFile(VoiceFile, VoiceFileName.Text);
  Reset(VoiceFile);
  ProgressBar1.Max := FileSize(VoiceFile);

  while true do
  begin
    idx := 0;
    for j := 0 to 3 do
    begin
      tx_packet[j].code := CMD__PRG_BUF;
      for i := 0 to sizeof(tx_packet[j].data) - 1 do
      begin
        if EOF(VoiceFile) then
        begin
          tx_packet[j].data[i] := $ff;
        end
        else
        begin
          read(VoiceFile, tx_packet[j].data[i]);
        end;
      end;
      get_crc(tx_packet[j]);
      inc(idx);
      if (EOF(VoiceFile)) then
      begin
        break;
      end;
    end;

    ProgressBar1.Position :=
        ProgressBar1.Position + sizeof(tx_packet[0].data) * idx;
    com_port.Write(tx_packet, sizeof(packet_t) * idx);
    for i := 0 to 1 do begin
      Application.ProcessMessages;
      if (fifo_rx_packet_get_one(rx_packet)) then
      begin
        if (rx_packet.code = CMD__ERROR) then
        begin
          m_log.Lines.Add('������ � ���� ��������!');
          exit;
        end;
      end;
      sleep(1);
    end;

    if (EOF(VoiceFile)) then break;
  end;
  CloseFile(VoiceFile);

  if (not stop_prg_flash) then begin
    m_log.Lines.Add('�� ������� ��������� ��������!');
    exit;
  end;
  m_log.Lines.Add(TimeToStr(Now) + ': �������� ��������� �������.');
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  Dir: string;
  fs: TSearchRec;
  AudioFileList: TStringList;
  AudioFileSumSize: LongWord;
  AudioFileSize: array [0..1000] of LongWord;
  PhonemeAmount: integer;
  PhonemeAddress: array [0..1000] of longword;
  voice: array [0..FLASH_SIZE - 1] of byte;
  VoiceFileSize: LongWord;
  CheckSum: byte;
  HeaderFile: TextFile;
  i, j: LongWord;
  AudioFile: file of byte;
  str: string;
begin
  dir := AudioDirEdit.Text + '\';
  if (not DirectoryExists(dir)) then
  begin
    m_log.Lines.Add('����� � ����� ������� �� �������!');
    exit;
  end;

  AudioFileList := TStringList.Create;

  // �������� ������ �����
  if (FindFirst(dir + '*.pcm', faAnyFile, fs) = 0) then
  begin
    i := 0;
    AudioFileSumSize := 0;
    repeat
      m_log.Lines.Add(fs.Name + ' - ' + inttostr(fs.Size) + ' ����');
      AudioFileList.Add(fs.Name);
      AudioFileSize[i] := fs.Size;
      AudioFileSumSize := AudioFileSumSize + fs.Size;
      inc(i);
    until FindNext(fs)<>0;
    m_log.Lines.Add('-----------------------------');
    m_log.Lines.Add('��������� ������: ' + inttostr(AudioFileSumSize) + ' ����' +
                    ' (' + FloatToStrF(AudioFileSumSize / 1024 / 1024, ffFixed, 6, 3) + ' �����)');
  end;
  FindClose(fs);

  if (AudioFileSumSize >= sizeof(voice)) then begin
    m_log.Lines.Add('������� ������� ������ �����!');
    exit;
  end;

  PhonemeAmount := AudioFileList.Count;
  VoiceFileSize := 0;

  // �������� ���������� �����
  voice[VoiceFileSize] := PhonemeAmount;
  inc(VoiceFileSize);
  voice[VoiceFileSize] := PhonemeAmount shr 8;
  inc(VoiceFileSize);

  // �������� ������� �� ���������� ����� (����� ������ + �����)
  PhonemeAddress[0] := VoiceFileSize + PhonemeAmount * 8;
  for i := 0 to PhonemeAmount - 1 do begin
    voice[VoiceFileSize + 0] := PhonemeAddress[i];
    voice[VoiceFileSize + 1] := PhonemeAddress[i] shr 8;
    voice[VoiceFileSize + 2] := PhonemeAddress[i] shr 16;
    voice[VoiceFileSize + 3] := PhonemeAddress[i] shr 24;

    voice[VoiceFileSize + 4] := AudioFileSize[i];
    voice[VoiceFileSize + 5] := AudioFileSize[i] shr 8;
    voice[VoiceFileSize + 6] := AudioFileSize[i] shr 16;
    voice[VoiceFileSize + 7] := AudioFileSize[i] shr 24;

    VoiceFileSize := VoiceFileSize + 8;

    PhonemeAddress[i + 1] := PhonemeAddress[i] + AudioFileSize[i];
  end;

  // �������� ���� ����� �����
  for i := 0 to PhonemeAmount - 1 do
  begin
    AssignFile(AudioFile, dir + '\' + AudioFileList.Strings[i]);
    Reset(AudioFile);
    for j := 0 to AudioFileSize[i] - 1 do
    begin
      Read(AudioFile, voice[VoiceFileSize]);
      inc(VoiceFileSize);
    end;
    CloseFile(AudioFile);
  end;

  while ((VoiceFileSize mod FLASH_PAGE_SIZE) <> 0) do
  begin
    voice[VoiceFileSize] := $FF;
    inc(VoiceFileSize);
  end;

  m_log.Lines.Add('������ ���������� �����: ' + inttostr(VoiceFileSize) + ' ����' +
                  ' (' + FloatToStrF(VoiceFileSize / 1024 / 1024, ffFixed, 6, 3) + ' �����)');

  CheckSum := 0;
  for i := 0 to VoiceFileSize - 1 do
  begin
    CheckSum := CheckSum xor voice[i];
  end;
  m_log.Lines.Add('����������� �����: 0x' + IntToHex(CheckSum, 2));

  // ��������� ��������� ����
  AssignFile(AudioFile, AudioDirEdit.Text + '\voice.bin');
  Rewrite(AudioFile);
  BlockWrite(AudioFile, voice, VoiceFileSize);
  CloseFile(AudioFile);

  // ������������ header-����
  AssignFile(HeaderFile, ProjectDirEdit.Text + '\phoneme_id.h');
  ReWrite(HeaderFile);
  WriteLn(HeaderFile, '#ifndef PHONEME_ID_H_');
  WriteLn(HeaderFile, '#define PHONEME_ID_H_');
  WriteLn(HeaderFile, '');
  for i := 0 to PhonemeAmount - 1 do
  begin
    str := AudioFileList.Strings[i];
    str := UpperCase(copy(str, 0, Length(str) - 4));
    str := '#define PHONEME_ID__' + str + ' ';
    WriteLn(HeaderFile, str, inttostr(i):(79 - length(str)));
  end;
  WriteLn(HeaderFile, '');
  WriteLn(HeaderFile, '#endif /* PHONEME_ID_H_ */');
  CloseFile(HeaderFile);

  AudioFileList.Destroy;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  OpenDialog.CleanupInstance;
  OpenDialog.Title := '������� bin-����';
  OpenDialog.InitialDir := VoiceFileName.Text;
  OpenDialog.Filter := 'bin-����|*.bin';
  OpenDialog.Execute;
  if (CompareStr(OpenDialog.FileName, '') <> 0) then begin
    VoiceFileName.Text := OpenDialog.FileName;
  end;
end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  BotRxLog.Lines.Clear;
  m_log.Lines.Clear;
end;

procedure TForm1.ComComboBox1DropDown(Sender: TObject);
begin
  EnumComPorts(ComComboBox1.Items);
  if ComComboBox1.ItemIndex > -1 then begin
    com_port.Port := ComComboBox1.Items[ComComboBox1.ItemIndex];
  end;
end;

procedure TForm1.com_portRxChar(Sender: TObject; Count: Integer);
var
  i: integer;
  rx_buf: array[0..4095] of byte;
  pointer: PByte;
begin
  com_port.Read(rx_buf, count);
  for i := 0 to count - 1 do begin
    pointer := @fifo_rx_packet.buf[fifo_rx_packet.head];
    pointer := pointer + fifo_rx_packet.idx_inside_packet;
    pointer^ := rx_buf[i];
    inc(fifo_rx_packet.idx_inside_packet);
    if (fifo_rx_packet.idx_inside_packet >= sizeof(packet_t)) then begin
      fifo_rx_packet.idx_inside_packet := 0;
      inc(fifo_rx_packet.head);
      if (fifo_rx_packet.head >= FIFO_RX_PACKET_BUF_SIZE) then begin
        fifo_rx_packet.head := 0;
      end;
      inc(fifo_rx_packet.idx);
    end;
  end;

  for i := 0 to count - 1 do
  begin
    BotRxLog.Text := BotRxLog.Text + '0x' + inttohex(rx_buf[i], 2) + ' ';
  end;
end;

procedure TForm1.btn_open_portClick(Sender: TObject);
begin
  if (com_port.Connected) then begin
    com_port.Close;
    if (not com_port.Connected) then begin
      btn_open_port.Caption := '�������';
      ComLed1.State := lsOff;
    end;
  end else begin
    com_port.Port := ComComboBox1.Text;
    com_port.BaudRate := brCustom;
    com_port.CustomBaudRate := 1000000;
    try
      com_port.Open;
    except
      Application.MessageBox(
          '�� ������� ������� ����.', '������', MB_OK + MB_ICONERROR);
    end;

    if (com_port.Connected) then begin
      btn_open_port.Caption := '�������';
      ComLed1.State := lsOn;
    end;
  end;
end;

procedure TForm1.FlashGetStatusRegButtonClick(Sender: TObject);
var
  tx_packet: packet_t;
begin
  tx_packet.code := CMD__FLASH_STATUS;
  get_crc(tx_packet);
  com_port.Write(tx_packet, sizeof(packet_t));
end;

procedure TForm1.FlashEraseButtonClick(Sender: TObject);
var
  tx_packet: packet_t;
begin
  tx_packet.code := CMD__FLASH_ERASE;
  get_crc(tx_packet);
  com_port.Write(tx_packet, sizeof(packet_t));
end;

procedure TForm1.FlashTestEraseReadButtonClick(Sender: TObject);
var
  tx_packet: packet_t;
begin
  tx_packet.code := CMD__FLASH_TEST_ERASE_READ;
  get_crc(tx_packet);
  com_port.Write(tx_packet, sizeof(packet_t));
end;


procedure TForm1.FlashTestWriteReadButtonClick(Sender: TObject);
var
  tx_packet: packet_t;
begin
  tx_packet.code := CMD__FLASH_TEST_WRITE_READ;
  get_crc(tx_packet);
  com_port.Write(tx_packet, sizeof(packet_t));
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  fifo_rx_packet_purge;
  AudioDirEdit.Text := 'd:\Projects\baumanec\music\audio test';//GetCurrentDir;
  ProjectDirEdit.Text := 'd:\Projects\baumanec\music\mcu_src';
  VoiceDirEdit.Text := 'd:\Projects\baumanec\music\audio test';
  VoiceFileName.Text := VoiceDirEdit.Text + '\voice.bin';

  ComComboBox1DropDown(self);
  ComComboBox1.ItemIndex := 0;
end;


procedure TForm1.GetCheckSumButtonClick(Sender: TObject);
var
  tx_packet: packet_t;
begin
  tx_packet.code := CMD__GET_CHECK_SUM;
  get_crc(tx_packet);
  com_port.Write(tx_packet, sizeof(packet_t));
end;

procedure TForm1.ReadAddressButtonClick(Sender: TObject);
var
  tx_packet: packet_t;
  address: LongWord;
begin
  tx_packet.code := CMD__FLASH_READ;
  address := StrToInt('$' + FlashReadAddressEdit.Text);
  tx_packet.data[0] := address;
  tx_packet.data[1] := address shr 8;
  tx_packet.data[2] := address shr 16;
  tx_packet.data[3] := address shr 24;
  get_crc(tx_packet);
  com_port.Write(tx_packet, sizeof(packet_t));
end;

procedure TForm1.SelectAudioDirButtonClick(Sender: TObject);
var
  Dir: String;
begin
  Dir := AudioDirEdit.Text;
  SelectDirectory('�������� �����', '', Dir, [sdNewUI]);
  if (dir <> '') then
  begin
    AudioDirEdit.Text := Dir;
  end;
end;

procedure TForm1.SelectProjectDirButtonClick(Sender: TObject);
var
  Dir: String;
begin
  Dir := ProjectDirEdit.Text;
  SelectDirectory('�������� �����', '', Dir, [sdNewUI]);
  if (dir <> '') then
  begin
    ProjectDirEdit.Text := Dir;
  end;
end;

procedure TForm1.SelectVoiceDirButtonClick(Sender: TObject);
var
  Dir: String;
begin
  Dir := VoiceDirEdit.Text;
  SelectDirectory('�������� �����', '', Dir, [sdNewUI]);
  if (dir <> '') then
  begin
    VoiceDirEdit.Text := Dir;
  end;
end;

end.
