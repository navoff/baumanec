/**
 * @file melodies.c
 * @brief
 * @author ������� �.�.
 * @date 03.06.2012
 * @addtogroup
 * @{
*/

#include <stdint.h>

#include "melodies.h"
#include "phoneme_id.h"


//================================= Definition ================================



//================================ Declaration ================================

uint16_t melody_void_intro[] = {0xffff};
uint16_t melody_void_main[] = {0xffff};

//uint16_t melody_test_main[] = {PHONEME_ID__TONE_100HZ_1100HZ_10SEC};
uint16_t melody_imperial_march_intro[] = {PHONEME_ID__MARCH_INTRO, 0xffff};
uint16_t melody_imperial_march_main[] = {PHONEME_ID__MARCH_MAIN, 0xffff};
uint16_t melody_pink_panther_main[] = {PHONEME_ID__PINK_PANTHER, 0xffff};
uint16_t melody_bumer_main[] = {PHONEME_ID__BUMER, 0xffff};
//uint16_t melody_morning_main[] = {PHONEME_ID__MORNING, 0xffff};
uint16_t melody_lion_sleeps_tonight_main[] = {PHONEME_ID__THE_LION_SLEEPS_TONIGHT, 0xffff};
//uint16_t melody_seven_nation_army_main[] = {PHONEME_ID__SEVEN_NATION_ARMY, 0xffff};
uint16_t melody_piraty_main[] = {PHONEME_ID__PIRATY, 0xffff};
//uint16_t melody_whistle_main[] = {PHONEME_ID__WHISTLE, 0xffff};
//uint16_t melody_intro[] = {PHONEME_ID__INTRO, 0xffff};
//uint16_t melody_outro[] = {PHONEME_ID__OUTRO, 0xffff};
//uint16_t melody_whistle[] = {PHONEME_ID__WHISTLE, 0xffff};

//melodies_t melodies = {
//    2,
//    {
//        {melody_intro, melody_outro},
//        {melody_void_intro, melody_whistle},
//        {melody_void_intro, melody_void_main},
//        {melody_void_intro, melody_test_main},
//        {melody_void_intro, melody_piraty_main},
//        {melody_void_intro, melody_lion_sleeps_tonight_main},
//        {melody_void_intro, melody_seven_nation_army_main},
//        {melody_void_intro, melody_whistle_main},
//        {melody_imperial_march_intro, melody_imperial_march_main},
//        {melody_void_intro, melody_pink_panther_main},
//        {melody_void_intro, melody_bumer_main},
//        {melody_void_intro, melody_lion_sleeps_tonight_main},
//        {melody_morning_intro, melody_morning_main},
//    }
//};

melodies_t melodies = {
    5,
    {
        {melody_void_intro, melody_void_main},
        {melody_imperial_march_intro, melody_imperial_march_main},
        {melody_void_intro, melody_pink_panther_main},
        {melody_void_intro, melody_bumer_main},
        {melody_void_intro, melody_lion_sleeps_tonight_main},
        {melody_void_intro, melody_piraty_main},
    }
};

// ============================== Implementation ==============================



/** @} */
