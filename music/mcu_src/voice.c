/**
 * @file voice.c
 * @brief
 * @author ������� �.�.
 * @date 22.05.2012
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/interrupt.h>

#include "errors_type.h"
#include "voice.h"
#include "flash.h"
#include "main.h"

//================================= Definition ================================

#define FIFO_PHONEME_BUF_SIZE           20

/// ������� ������������� ���������� �������.
#define F_DISCRET                       6000

/// ������� PWM.
#define F_PWM                           24000

/// ������� ������ �������� PWM � ������.
#define N_PWM_MIDDLE                    180

/// ������������ ������ ������ �������� PWM � ������.
#define N_PWM_MAX_DEV                   128

#define N_PWM_REPITITION                (F_PWM / F_DISCRET)

#define N_PWM                           (F_CPU + F_PWM - 1) / F_PWM

#if (N_PWM_MIDDLE - N_PWM_MAX_DEV <= 0)
#error N_PWM_MAX_DEV too small
#endif

#if (N_PWM_MIDDLE + N_PWM_MAX_DEV >= N_PWM)
#error N_PWM_MAX_DEV too large
#endif

#define FIFO_PRG_BUF_SIZE   64

#define FIFO_PWM_BUF_SIZE   10

typedef struct{
  uint32_t addr;
  uint32_t size;
}phoneme_header_t;

typedef struct{
  enum{
    VOICE_ST__WAIT,
    VOICE_ST__ERASE_IN_PROG,
    VOICE_ST__RX_WRITE_DATA,
    VOICE_ST__WRITE_PAGE,
    VOICE_ST__WAIT_WRITE_STOP,

    VOICE_ST__READ_PHONEME_HEADER,
    VOICE_ST__READ_PHONEME,
    VOICE_ST__ERROR,
  }state;
  uint8_t error;
  /// ���������� ����� � ��������� �����.
  /// @note ����������� ��� ������� ���������������.
  uint16_t phoneme_amount;
  /// ���������� ������������� ���� ������.
  uint32_t unread_len;
  /// ����� ��������.
  uint8_t req;
  /// ����� ��������, ������� ������ ���������.
  uint8_t erase_sector_idx;
  /// ����� �����, ������� �������� � ����� ��� ������ ��������.
  uint16_t prog_byte_idx;
  uint32_t prog_address;
}voice_t;

/// ���� ����� ������, ������������ �� flash.
typedef struct{
  uint8_t buf[FIFO_PRG_BUF_SIZE];
  uint8_t tail;
  uint8_t head;
  uint8_t idx;
}fifo_voice_prg_t;

/// ���� ����� ��������������� �����.
typedef struct{
  uint16_t buf[FIFO_PHONEME_BUF_SIZE];
  uint8_t tail;
  uint8_t head;
  uint8_t idx;
}fifo_phoneme_t;

typedef struct{
  uint16_t buf[FIFO_PWM_BUF_SIZE];
  uint8_t tail;
  uint8_t head;
  uint8_t idx;
  uint8_t repetition;
}fifo_pwm_t;

#define PWM_START()         TIMSK |= (1 << OCIE1A)
#define PWM_STOP()          TIMSK &= ~(1 << OCIE1A)
#define PWM_IS_STOP()       ((TIMSK & (1 << OCIE1A)) == 0)

//================================ Declaration ================================

void fifo_pwm_put_buf(uint8_t buf[], uint8_t size);

fifo_phoneme_t fifo_phoneme;
fifo_voice_prg_t fifo_voice_prg;
fifo_pwm_t fifo_pwm;
voice_t voice;

// ============================== Implementation ==============================

/**
 * @brief ��������� ���������� ��������.
 */
void voice_init(void)
{
  fifo_phoneme.head = fifo_phoneme.idx = fifo_phoneme.tail = 0;
  fifo_voice_prg.head = fifo_voice_prg.idx = fifo_voice_prg.tail = 0;
  fifo_pwm.head = fifo_pwm.idx = fifo_pwm.tail = fifo_pwm.repetition = 0;

  voice.error = 0;
  voice.req = 0;
  voice.phoneme_amount = 0;
  voice.unread_len = 0;

  DDRB |= (1 << PB1);
  PORTB &= ~(1 << PB1);

  ICR1 = N_PWM;
  OCR1A = N_PWM_MIDDLE;
  TIFR = (1 << OCF1A);

  TCCR1A = (1 << COM1A1) | (1 << WGM11);
  TCCR1B = (1 << WGM13) | (1 << WGM12) | (1 << CS10);
}

void voice_restart(void)
{
  flash_release();
  voice.state = VOICE_ST__WAIT;
}

/**
 * @brief ������� ���������� ��������� � ���������������� ������.
 */
void voice_process(void)
{
  uint8_t amount;

voice_sm:

  switch(voice.state)
  {
    case VOICE_ST__ERROR:
      voice.req = 0;
      voice.state = VOICE_ST__WAIT;
    break;

    case VOICE_ST__WAIT:
      if (voice.req & VOICE_REQ_STOP_PRG)
      {
        voice.req &= ~(VOICE_REQ_STOP_PRG);
      }

      if (voice.req & VOICE_REQ_START_PRG)
      {
        voice.req &= ~(VOICE_REQ_START_PRG);
        voice.erase_sector_idx = 0;
        cmd_send_status(CMD__START_PRG_FLASH, ERR_BUSY);
        voice.state = VOICE_ST__ERASE_IN_PROG;
        goto voice_sm;
      }

      if (voice.req & VOICE_REQ_STOP_SPEECH)
      {
        voice.req &= ~(VOICE_REQ_STOP_SPEECH);
      }

      if (voice.req & VOICE_REQ_START_SPEECH)
      {
        voice.req &= ~(VOICE_REQ_START_SPEECH);

        // ��������� ���������� ����� � ��������� �����
        flash_read_buf(
            0,
            (uint8_t *)&voice.phoneme_amount,
            sizeof(voice.phoneme_amount));
        voice.state = VOICE_ST__READ_PHONEME_HEADER;
        break;
      }
    break;

    case VOICE_ST__READ_PHONEME_HEADER:

      if (voice.req & VOICE_REQ_STOP_SPEECH)
      {
        voice.req &= ~(VOICE_REQ_STOP_SPEECH);
        voice_speech_break();
        voice.state = VOICE_ST__WAIT;
        break;
      }

      if (fifo_phoneme.idx)
      {
        uint16_t phoneme = fifo_phoneme.buf[fifo_phoneme.tail++];
        if (fifo_phoneme.tail >= FIFO_PHONEME_BUF_SIZE)
        {
          fifo_phoneme.tail = 0;
        }
        fifo_phoneme.idx--;

        if (phoneme < voice.phoneme_amount)
        {
          phoneme_header_t phoneme_header;
          flash_read_buf(
              sizeof(voice.phoneme_amount) + phoneme * sizeof(phoneme_header_t),
              (uint8_t *)&phoneme_header,
              sizeof(phoneme_header_t));
          voice.unread_len = phoneme_header.size;
          flash_continuous_read_start(phoneme_header.addr);
          voice.state = VOICE_ST__READ_PHONEME;
          break;
        }
      }
    break;

    case VOICE_ST__READ_PHONEME:
      if (voice.req & VOICE_REQ_STOP_SPEECH)
      {
        voice.req &= ~(VOICE_REQ_STOP_SPEECH);
        voice_speech_break();
        voice.state = VOICE_ST__WAIT;
        break;
      }

      amount = FIFO_PWM_BUF_SIZE - fifo_pwm.idx;
      // ���� ���� ����� � ������ PWM
      if (amount)
      {
        // ���� ����, ��� ������
        if (voice.unread_len)
        {
          if (voice.unread_len < amount)
          {
            amount = voice.unread_len;
          }
          uint8_t buf[FIFO_PWM_BUF_SIZE];
          flash_continuous_read_buf(buf, amount);
          fifo_pwm_put_buf(buf, amount);
          voice.unread_len -= amount;
        }
        else
        {
          flash_continuous_read_stop();
          voice.state = VOICE_ST__READ_PHONEME_HEADER;
          break;
        }
      }

      // ���� PWM ��� �� ������� (������ ������ �������������� �����)
      if (PWM_IS_STOP())
      {
        if ((fifo_pwm.idx == FIFO_PWM_BUF_SIZE) ||
            (voice.unread_len == 0))
        {
          PWM_START();
        }
      }
    break;

    case VOICE_ST__ERASE_IN_PROG:
      if (flash_is_not_busy() == ERR_OK)
      {
        if (voice.erase_sector_idx >= FLASH_SECTOR_AMOUNT)
        {
          cmd_send_status(CMD__START_PRG_FLASH, ERR_OK);
          flash_continuous_write_to_buf_start();
          voice.prog_byte_idx = 0;
          voice.prog_address = 0;
          voice.state = VOICE_ST__RX_WRITE_DATA;
        }
        else
        {
          flash_erase_sector_req(voice.erase_sector_idx);
          voice.erase_sector_idx++;
        }
      }
    break;

    case VOICE_ST__RX_WRITE_DATA:
      while (fifo_voice_prg.idx)
      {
        flash_continuous_write_to_buf_byte(fifo_voice_prg.buf[fifo_voice_prg.tail++]);
        if (fifo_voice_prg.tail >= FIFO_PRG_BUF_SIZE)
        {
          fifo_voice_prg.tail = 0;
        }
        fifo_voice_prg.idx--;
        voice.prog_byte_idx++;
        if (voice.prog_byte_idx >= FLASH_PAGE_SIZE)
        {
          flash_continuous_write_stop(voice.prog_address);
          voice.state = VOICE_ST__WRITE_PAGE;
          goto voice_sm;
        }
      }

      if (voice.req & VOICE_REQ_START_PRG)
      {
        flash_release();
        voice.state = VOICE_ST__WAIT;
        break;
      }

      if (voice.req & VOICE_REQ_STOP_PRG)
      {
        voice.req &= ~(VOICE_REQ_STOP_PRG);
        flash_release();
        voice.state = VOICE_ST__WAIT_WRITE_STOP;
      }
    break;

    case VOICE_ST__WRITE_PAGE:
      if (flash_is_not_busy() == ERR_OK)
      {
        flash_continuous_write_to_buf_start();
        voice.prog_byte_idx = 0;
        voice.prog_address += FLASH_PAGE_SIZE;
        voice.state = VOICE_ST__RX_WRITE_DATA;

        //cmd_send_status(CMD__START_PRG_FLASH, ERR_OK);
        goto voice_sm;
      }
    break;

    case VOICE_ST__WAIT_WRITE_STOP:
      if (flash_is_not_busy() == ERR_OK)
      {
        cmd_send_status(CMD__STOP_PRG_FLASH, ERR_OK);
        voice.state = VOICE_ST__WAIT;
      }
    break;

  }
}

/**
 * @brief ���������� ���������� �� ������������ �������� ��������� OCR1A.
 * @param SIG_OUTPUT_COMPARE1A
 */
ISR(SIG_OUTPUT_COMPARE1A)
{
  if (fifo_pwm.idx)
  {
    if (fifo_pwm.repetition == 0)
    {
      OCR1A = fifo_pwm.buf[fifo_pwm.tail++];
      if (fifo_pwm.tail >= FIFO_PWM_BUF_SIZE)
      {
        fifo_pwm.tail = 0;
      }
      fifo_pwm.idx--;
    }

    fifo_pwm.repetition++;
    if (fifo_pwm.repetition >= N_PWM_REPITITION)
    {
      fifo_pwm.repetition = 0;
    }
  }
  else
  {
    OCR1A = N_PWM_MIDDLE;
    PWM_STOP();
  }
}

/**
 * @brief �������� ������ �������� ������ � ����� PWM ��� ���������������.
 * @param buf - ������ ��������.
 * @param size - ������ �������.
 *
 * ������������ �������������� �� 8-��������� ����� � 16-�������� ������
 * ��������� PWM.
 */
void fifo_pwm_put_buf(uint8_t buf[], uint8_t size)
{
  for (uint8_t i = 0; i < size; i++)
  {
    fifo_pwm.buf[fifo_pwm.head++] =
        (((uint16_t)buf[i]) * N_PWM_MAX_DEV +
        ((N_PWM_MIDDLE - N_PWM_MAX_DEV) << 7)) >> 7;;
    if (fifo_pwm.head >= FIFO_PWM_BUF_SIZE)
    {
      fifo_pwm.head = 0;
    }
  }

  cli();
  fifo_pwm.idx += size;
  sei();
}

/**
 * @brief �������� ������ �� ��������� �������.
 * @param req_msk - ����� ��������.
 */
void voice_req(uint8_t req_msk)
{
  voice.req |= req_msk;
}

/**
 * @brief ��������� ������ ������ � ���� ����� ��� �������� �� flash.
 * @param buf - ������ ������.
 * @param size - ������ �������.
 * @return #ERR_OK - ������ ������� �������.
 */
uint8_t fifo_voice_prg_put_buf(uint8_t buf[], uint8_t size)
{
  if ((FIFO_PRG_BUF_SIZE - fifo_voice_prg.idx) < size)
  {
    return ERR_BOF;
  }

  for (uint8_t i = 0; i < size; i++)
  {
    fifo_voice_prg.buf[fifo_voice_prg.head++] = *buf++;
    if (fifo_voice_prg.head >= FIFO_PRG_BUF_SIZE)
    {
      fifo_voice_prg.head = 0;
    }
  }

  fifo_voice_prg.idx += size;

  return ERR_OK;
}

/**
 * @brief �������� ���������������.
 */
void voice_speech_break(void)
{
  fifo_phoneme.tail = fifo_phoneme.head;
  fifo_phoneme.idx = 0;

  voice.unread_len = 0;

  PWM_STOP();

  cli();
  OCR1A = N_PWM_MIDDLE;
  fifo_pwm.tail = fifo_pwm.head;
  fifo_pwm.idx = 0;
  sei();
}

/**
 * @brief ��������, ������������ �� ��������������� �����.
 * @return #ERR_OK - ��������������� �����������.
 */
uint8_t voice_speech_is_over(void)
{
  if (
      (fifo_phoneme.idx == 0) &&
      (fifo_pwm.idx == 0) &&
      (voice.unread_len == 0)
      )
  {
    return ERR_OK;
  }
  else
  {
    return ERR_BUSY;
  }
}

/**
 * @brief �������� ����� � ����� �� ���������������.
 * @param phrase - �����.
 * @return #ERR_OK - ����� ������� ���������.
 *
 * ����� - ������ ��������������� �����.
 * ����� ������ ������������ ������������� �������� 0xFFFF !!!!
 * ����� ����� ����� ��������� � �����, �� ��������� ��������� ���������������
 * ����������.
 */
uint8_t voice_speech_phrase(uint16_t phrase[])
{
  uint8_t i = 0;

  while (phrase[i] != 0xffff)
  {
    if (fifo_phoneme.idx >= FIFO_PHONEME_BUF_SIZE)
    {
      return ERR_BOF;
    }

    fifo_phoneme.buf[fifo_phoneme.head++] = phrase[i++];
    if (fifo_phoneme.head >= FIFO_PHONEME_BUF_SIZE)
    {
      fifo_phoneme.head = 0;
    }
    fifo_phoneme.idx++;
  }

  return ERR_OK;
}

/** @} */
