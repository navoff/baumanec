/**
 * @file tick_timer.c
 * @brief
 * @author ������� �.�.
 * @date 23.05.2012
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr\io.h>

#include "tick_timer.h"

//================================= Definition ================================

//================================ Declaration ================================

// ============================== Implementation ==============================

void tick_init(void)
{
  TCNT0 = 0;
  TIFR = (1 << TOV0);
#if defined(TICK_PERIOD_100_US)
  TCCR0 = (1 << CS02) | (0 << CS01) | (1 << CS00);
#else
#error undefined TICK_PERIOD
#endif
}

uint8_t tick_cnt(void)
{
  return TCNT0;
}

void tick_cnt_dec(void)
{
  uint8_t tccr0 = TCCR0;
  TCCR0 = 0;
  TCNT0--;
  TCCR0 = tccr0;
}

/** @} */
