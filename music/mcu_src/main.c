/**
 * @file main.c
 * @brief
 * @author ������� �.�.
 * @date 22.05.2012
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "main.h"
#include "uart.h"
#include "voice.h"
#include "flash.h"
#include "tick_timer.h"
#include "errors_type.h"
#include "phoneme_id.h"
#include "melodies.h"

//================================= Definition ================================

//================================ Declaration ================================

inline void rx_buf_process(void);
inline void tx_buf_process(void);

inline uint8_t rx_packet_process(packet_t *packet);

inline uint8_t check_crc(packet_t *packet);

struct{
  uint8_t melody_idx;
  uint8_t timer;
}music;

// ============================== Implementation ==============================

uint8_t flag_btn_press = 1;

int main()
{
  LED_INIT();

  // ��� ��� ������
  PORTD |= (1 << PD4);
  uint8_t buf_pin = 0;

  music.melody_idx = 0;
  music.timer = 0;

  voice_init();
  tick_init();
  uart_init(BAUDRATE__1000000);
  flash_init();
  voice_req(VOICE_REQ_START_SPEECH);

  sei();

  while(1)
  {
    uint8_t is_tick = 0;

    if (tick_cnt())
    {
      tick_cnt_dec();
      is_tick = 1;
    }

    rx_buf_process();
    tx_buf_process();

    if (is_tick)
    {
      voice_process();

      cli();
      if (fifo_uart_rx.idx)
      {
        fifo_uart_rx.wait_timer++;
        if (fifo_uart_rx.wait_timer >= 100)
        {
          fifo_uart_rx.tail = fifo_uart_rx.head;
          fifo_uart_rx.idx = 0;
          fifo_uart_rx.wait_timer = 0;
        }
      }
      sei();

      music.timer++;
      if (music.timer >= 10)
      {
        music.timer = 0;
        if (voice_speech_is_over() == ERR_OK)
        {
          voice_speech_phrase(melodies.melody[music.melody_idx].main);
        }
      }
      if (flag_btn_press)
      {
        flag_btn_press = 0;
        LED_SWITCH();
        music.melody_idx++;
        if (music.melody_idx >= melodies.amount)
        {
          music.melody_idx = 0;
        }

        voice_speech_break();
        voice_speech_phrase(melodies.melody[music.melody_idx].intro);
      }
    }

    if (fifo_uart_rx.bof)
    {
      fifo_uart_rx.bof = 0;
      cmd_send_status(CMD__ERROR, ERR_BOF);
    }

    if (bit_is_set(PIND, PD4))
    {
      buf_pin = 1;
    }

    if (bit_is_clear(PIND,PD4) && (buf_pin))
    {
      buf_pin = 0;
      flag_btn_press = 1;
    }
  }
}

inline void tx_buf_process(void)
{
  if (fifo_uart_tx.idx)
  {
    if (UCSRA & (1 << UDRE))
    {
      UDR = fifo_uart_tx.buf[fifo_uart_tx.tail++];
      if (fifo_uart_tx.tail >= FIFO_UART_TX_BUF_SIZE)
      {
        fifo_uart_tx.tail = 0;
      }
      fifo_uart_tx.idx--;
    }
  }
}

struct{
  enum{
    RX_PROCESS_ST__WAIT,
    RX_PROCESS_ST__BUSY,
  } state;
  packet_t buf_packet;
}rx_process = {
    RX_PROCESS_ST__WAIT
};

inline void rx_buf_process(void)
{
rx_buf_sm:
  switch(rx_process.state)
  {
    case RX_PROCESS_ST__WAIT:
      // � �������� ������ ��������� ������ �� ����� �����
      if (fifo_uart_rx.idx >= sizeof(packet_t))
      {
        fifo_uart_rx_get_packet(&rx_process.buf_packet);
        rx_process.state = RX_PROCESS_ST__BUSY;
        goto rx_buf_sm;
      }
    break;

    case RX_PROCESS_ST__BUSY:
      if (rx_packet_process(&rx_process.buf_packet) != ERR_BUSY)
      {
        rx_process.state = RX_PROCESS_ST__WAIT;
      }
    break;
  }
}

inline uint8_t rx_packet_process(packet_t *packet)
{
  uint8_t res = ERR_OK;

  if (0)// xxx check_crc(packet) != ERR_OK)
  {
    cmd_send_status(CMD__ERROR, ERR_CRC);
    return ERR_CRC;
  }

  uint8_t buf[8];
  uint32_t address;
  switch (packet->code)
  {
    case CMD__FLASH_STATUS:
      voice_restart();
      cmd_send_status(CMD__FLASH_STATUS, flash_read_status());
    break;

    case CMD__FLASH_ERASE:
      voice_restart();
      flash_erase();
      cmd_send_status(CMD__FLASH_ERASE, ERR_OK);
    break;

    case CMD__FLASH_TEST_ERASE_READ:
    {
      voice_restart();
      uint8_t test_result[8];
      if (flash_test_erase_read(test_result) == ERR_OK)
      {
        cmd_send_status(CMD__FLASH_TEST_ERASE_READ, ERR_OK);
      }
      else
      {
        cmd_send_buf(CMD__ERROR, test_result);
      }
    }
    break;

    case CMD__FLASH_TEST_WRITE_READ:
    {
      voice_restart();
      uint8_t test_result[8];
      if (flash_test_write_read(test_result) == ERR_OK)
      {
        cmd_send_status(CMD__FLASH_TEST_WRITE_READ, ERR_OK);
      }
      else
      {
        cmd_send_buf(CMD__ERROR, test_result);
      }
    }
    break;

    case CMD__GET_CHECK_SUM:
      voice_restart();
      cmd_send_status(CMD__GET_CHECK_SUM, flash_get_check_sum());
    break;

    case 0x05:
      voice_restart();
      address =
          (((uint32_t)packet->data[0]) << 17) +
          (((uint32_t)packet->data[1]) << 12) +
          (((uint32_t)packet->data[2]) << 9) +
          (((uint32_t)packet->data[3]) << 3);
      flash_read_buf(address, buf, 8);
      uart_tx_buf((uint8_t *)&address, 4);
      uart_tx_buf(buf, 8);
    break;
    case 0x06:
      voice_restart();
      address =
          (((uint32_t)packet->data[0]) << 0) +
          (((uint32_t)packet->data[1]) << 8) +
          (((uint32_t)packet->data[2]) << 16) +
          (((uint32_t)packet->data[3]) << 24);
      flash_read_buf(address, buf, 8);
      uart_tx_buf((uint8_t *)&address, 4);
      uart_tx_buf(buf, 8);
    break;

    case CMD__START_PRG_FLASH:
      music.melody_idx = 0;
      voice_speech_break();
      voice_req(VOICE_REQ_STOP_SPEECH | VOICE_REQ_START_PRG);
    break;

    case CMD__PRG_BUF:
      if (fifo_voice_prg_put_buf(packet->data, sizeof(packet->data)) == ERR_BOF)
      {
        res = ERR_BUSY;
      }
    break;

    case CMD__STOP_PRG_FLASH:
      voice_req(VOICE_REQ_STOP_PRG | VOICE_REQ_START_SPEECH);
    break;

    default:
      cmd_send_status(CMD__ERROR, ERR_RESULT);
  }

  return res;
}

void cmd_send_status(uint8_t code, uint8_t status)
{
  packet_t packet;

  packet.code = code;
  packet.data[0] = status;
  for (uint8_t i = 1; i < sizeof(packet.data); i++)
  {
    packet.data[i] = 0x00;
  }
  packet.crc = 0xFF; // xxx

  uart_tx_buf((uint8_t *)&packet, sizeof(packet_t));
}

void cmd_send_buf(uint8_t code, uint8_t buf[])
{
  packet_t packet;

  packet.code = code;
  for (uint8_t i = 0; i < sizeof(packet.data); i++)
  {
    packet.data[i] = buf[i];
  }
  packet.crc = 0xFF; // xxx

  uart_tx_buf((uint8_t *)&packet, sizeof(packet_t));
}

inline uint8_t check_crc(packet_t *packet)
{
  uint8_t crc = 0;
  for (uint8_t i = 0; i < sizeof(packet_t); i++)
  {
    crc ^= ((uint8_t *)packet)[i];
  }
  if (crc == 0)
  {
    return ERR_OK;
  }
  else
  {
    return ERR_CRC;
  }
}

/** @} */

