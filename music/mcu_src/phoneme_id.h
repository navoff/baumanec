#ifndef PHONEME_ID_H_
#define PHONEME_ID_H_

#define PHONEME_ID__BUMER                                                     0
#define PHONEME_ID__MARCH_INTRO                                               1
#define PHONEME_ID__MARCH_MAIN                                                2
#define PHONEME_ID__PINK_PANTHER                                              3
#define PHONEME_ID__PIRATY                                                    4
#define PHONEME_ID__THE_LION_SLEEPS_TONIGHT                                   5

#endif /* PHONEME_ID_H_ */
