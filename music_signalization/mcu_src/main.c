/**
 * @file main.c
 * @brief
 * @author ������� �.�.
 * @date 30.05.2011
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "tick_timer.h"
#include "melodies.h"
#include "music.h"
#include "ir.h"
#include "leds.h"

//================================= Definition ================================

#define IR_IS_ACTIVE()      bit_is_clear(PIND, PD3)
#define IR_IS_PASSIVE()     bit_is_set(PIND, PD3)

#define BTN_IS_ACTIVE()     bit_is_clear(PINB, PB0)
#define BTN_IS_PASSIVE()    bit_is_set(PINB, PB0)

typedef struct{
  enum{
    SIGNALIZATION_ST__WAIT_START_ACTIVE,
    SIGNALIZATION_ST__WAIT_START_PAUSE,
    SIGNALIZATION_ST__ACTIVE,
    SIGNALIZATION_ST__ALARM,
    SIGNALIZATION_ST__STOP,
  } state;
  uint16_t timestamp;
} signalization_t;

//================================ Declaration ================================

uint16_t music_timestamp;
uint16_t signalization_timestamp;
signalization_t signalization;

// ============================== Implementation ==============================

void signalization_init(void)
{
  // ������
  PORTB |= (1 << PB0);
}

void signalization_process(void)
{
  if (BTN_IS_ACTIVE())
  {
    if (signalization.state != SIGNALIZATION_ST__STOP)
    {
      LED_BLUE_OFF();
      LED_GREEN_OFF();
      music_stop();
      signalization.state = SIGNALIZATION_ST__STOP;
    }
  }

  switch (signalization.state)
  {
    case SIGNALIZATION_ST__STOP:
      if (BTN_IS_PASSIVE())
      {
        signalization.state = SIGNALIZATION_ST__WAIT_START_ACTIVE;
      }
    break;

    case SIGNALIZATION_ST__WAIT_START_ACTIVE:
      LED_BLUE_OFF();
      LED_GREEN_ON();

      if (IR_IS_ACTIVE())
      {
        START_TIMER(signalization.timestamp);
        signalization.state = SIGNALIZATION_ST__WAIT_START_PAUSE;
      }
    break;

    case SIGNALIZATION_ST__WAIT_START_PAUSE:
      if (IR_IS_PASSIVE())
      {
        signalization.state = SIGNALIZATION_ST__WAIT_START_ACTIVE;
        break;
      }

      if (CHECK_TIMER(signalization.timestamp, 5000))
      {
        LED_BLUE_ON();
        LED_GREEN_OFF();

        signalization.state = SIGNALIZATION_ST__ACTIVE;
      }
    break;

    case SIGNALIZATION_ST__ACTIVE:
      if (IR_IS_PASSIVE())
      {
        music_restart();
        signalization.state = SIGNALIZATION_ST__ALARM;
      }
    break;

    case SIGNALIZATION_ST__ALARM:

    break;
  }
}

int main (void)
{
  signalization_init();
	tick_init();
	music_init();
	ir_tx_init();
	leds_init();

	START_TIMER(music_timestamp);
	START_TIMER(signalization_timestamp);

	sei();

	while(1)
	{
    if (tick_cnt())
    {
      tick_cnt_dec();
    }

    if (CHECK_TIMER(music_timestamp, 1))
    {
      START_TIMER(music_timestamp);

      music_process();
    }

    if (CHECK_TIMER(signalization_timestamp, 1))
    {
      START_TIMER(signalization_timestamp);

      signalization_process();
    }

    if (IR_IS_ACTIVE())
    {
      LED_RED_ON();
    }
    else
    {
      LED_RED_OFF();
    }
	}
}

/** @} */
