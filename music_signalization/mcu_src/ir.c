/**
 * @file ir.c
 * @brief
 * @author ������� �.�.
 * @date 11.06.2016
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "ir.h"

//================================= Definition ================================

//================================ Declaration ================================

// ============================== Implementation ==============================

void ir_tx_init(void)
{
  // ���������� �����������
  DDRD |= (1 << PD4);

  TIFR = (1 << OCF2);
  TIMSK |= (1 << OCIE2);

  TCNT2 = 0;
  OCR2 = 111;
  TCCR2 = (1 << WGM21) | (0 << WGM20) | (0 << CS22) | (0 << CS21) | (1 << CS20);
}

//ISR(TIMER2_COMP_vect)
ISR(_VECTOR(2))
{
  PORTD ^= (1 << PD4);
}

/** @} */
