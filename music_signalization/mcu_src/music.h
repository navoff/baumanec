/**
 * @file music.h
 * @brief
 * @author ������� �.�.
 * @date 14.06.2016
 * @addtogroup
 * @{
*/

#ifndef MUSIC_H_
#define MUSIC_H_

//================================= Definition ================================

//================================ Declaration ================================

void music_init(void);
void music_process(void);
void music_restart(void);
void music_stop(void);

#endif /* MUSIC_H_ */

/** @} */
