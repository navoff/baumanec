/**
 * @file leds.h
 * @brief
 * @author ������� �.�.
 * @date 11.06.2016
 * @addtogroup
 * @{
*/

#ifndef LEDS_H_
#define LEDS_H_

#include <avr/io.h>

//================================= Definition ================================

#define LED_BLUE_PORT     PORTC
#define LED_BLUE_DDR      DDRC
#define LED_BLUE_PIN      PC5

#define LED_GREEN_PORT    PORTC
#define LED_GREEN_DDR     DDRC
#define LED_GREEN_PIN     PC4

#define LED_RED_PORT      PORTC
#define LED_RED_DDR       DDRC
#define LED_RED_PIN       PC3


#define LED_BLUE_ON()     LED_BLUE_PORT |= (1 << LED_BLUE_PIN)
#define LED_BLUE_OFF()    LED_BLUE_PORT &= ~(1 << LED_BLUE_PIN)

#define LED_GREEN_ON()    LED_GREEN_PORT |= (1 << LED_GREEN_PIN)
#define LED_GREEN_OFF()   LED_GREEN_PORT &= ~(1 << LED_GREEN_PIN)

#define LED_RED_ON()      LED_RED_PORT |= (1 << LED_RED_PIN)
#define LED_RED_OFF()     LED_RED_PORT &= ~(1 << LED_RED_PIN)

//================================ Declaration ================================

void leds_init(void);

#endif /* LEDS_H_ */

/** @} */
