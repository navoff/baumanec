/**
 * @file melodies.c
 * @brief
 * @author ������� �.�.
 * @date 30.05.2011
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/io.h>
#include <avr/pgmspace.h>

#include "melodies.h"

//================================= Definition ================================

//================================ Declaration ================================

// ============================== Implementation ==============================

PROGMEM Nota_Type Melody_March[] = {
	{ G1, SEM },
	{ Silence, SQ },
	{ G1, SEM },
	{ Silence, SQ },
	{ G1, SEM },
	{ Silence, SQ },
	{ Ddi1, QV },
	{ Ddi1, Cr },
	{ Silence, SQ },
	{ Adi1, Cr },

	{ Silence, SQ },
	{ G1, SEM },
	{ Silence, SQ },
	{ Ddi1, QV },
	{ Ddi1, Cr },
	{ Silence, SQ },
	{ Adi1, Cr },
	{ Silence, SQ },
	{ G1, CEL },
	{ Silence, SQ },

	{ D2, SEM },
	{ Silence, SQ },
	{ D2, SEM },
	{ Silence, SQ },
	{ D2, SEM },
	{ Silence, SQ },
	{ Ddi2, QV },
	{ Ddi2, Cr },
	{ Silence, SQ },
	{ Adi1, Cr },

	{ Silence, SQ },
	{ Fdi1, SEM },
	{ Silence, SQ },
	{ Ddi1, QV },
	{ Ddi1, Cr },
	{ Silence, SQ },
	{ Adi1, Cr },
	{ Silence, SQ },
	{ G1, CEL },
	{ Silence, SQ },

	{ G2, SEM },
	{ Silence, SQ },
	{ G1, QV },
	{ G1, Cr },
	{ Silence, SQ },
	{ G1, Cr },
	{ Silence, SQ },
	{ G2, SEM },
	{ Silence, SQ },
	{ Fdi2, QV },

	{ Fdi2, Cr },
	{ Silence, SQ },
	{ F2, Cr },
	{ Silence, SQ },
	{ E2, Cr },
	{ Silence, SQ },
	{ Ddi2, Cr },
	{ Silence, SQ },
	{ E2, QV },
	{ Silence, QV },

	{ Gdi1, QV },
	{ Silence, SQ },
	{ Cdi2, SEM },
	{ Silence, SQ },
	{ C2, QV },
	{ C2, Cr },
	{ Silence, SQ },
	{ H1, Cr },
	{ Silence, SQ },
	{ Adi1, Cr },

	{ Silence, SQ },
	{ A1, Cr },
	{ Silence, SQ },
	{ Adi1, QV },
	{ Silence, QV },
	{ Ddi1, QV },
	{ Silence, SQ },
	{ Fdi1, SEM },
	{ Silence, SQ },
	{ Ddi1, QV },

	{ Ddi1, Cr },
	{ Silence, SQ },
	{ A1, QV },
	{ Silence, SQ },
	{ G1, SEM },
	{ Silence, SQ },
	{ Ddi1, QV },
	{ Ddi1, Cr },
	{ Silence, SQ },
	{ A1, QV },

	{ Silence, SQ },
	{ G1, CEL },
	{ Silence, SEM },
	{ 0xffff, 0xff},
};

PROGMEM Nota_Type Melody_Pantera[] ={
	{ Ddi1, Cr },
	{ E1, SEM },
	{ Silence, SEM },
	{ Fdi1, Cr },
	{ G1, SEM },
	{ Silence, SEM },
	{ Ddi1, Cr },
	{ E1, QV },
	{ Fdi1, Cr },
	{ G1, QV }, // 10
	{ C2, Cr },
	{ H1, QV },
	{ E1, Cr },
	{ G1, QV },
	{ H1, Cr },
	{ Bdi1, SEM },
	{ Silence, QV },
	{ B1, Cr },
	{ A1, Cr },
	{ E1, Cr }, // 20
	{ D1, Cr },
	{ E1, Cr },
	{ E1, SEM },
	{ Silence, SEM },
	{ 0xffff, 0xff},
};

PROGMEM Nota_Type Melody_Bumer[] ={
	{ E2, Cr },
	{ G2, SEM },
	{ Silence, SEM },
	{ G2, Cr },
	{ E2, SEM },
	{ Silence, SEM },
	{ A2, Cr },
	{ G2, Cr },
	{ A2, Cr },
	{ G2, Cr },
	{ A2, Cr },
	{ G2, Cr },
	{ A2, Cr },
	{ G2, Cr },
	{ A2, Cr },
	{ H2, QV },
	{ Silence, SEM },
	{ E2, Cr },
	{ G2, QV },
	{ Silence, SEM },
	{ G2, Cr },
	{ E2, QV },
	{ Silence, SEM },
	{ A2, Cr },
	{ G2, Cr },
	{ A2, Cr },
	{ G2, Cr },
	{ A2, Cr },
	{ G2, Cr },
	{ A2, Cr },
	{ G2, Cr },
	{ A2, Cr },
	{ H2, QV },
	{ Silence, SEM },
	{ 0xffff, 0xff},
};

PROGMEM Nota_Type Melody_Dzeltentem[] ={
	{ Silence, SQ },
	{ E2, Cr },
	{ F2, Cr },
	{ E2, Cr },
	{ Ddi2, Cr },
	{ E2, Cr },
	{ Silence, SQ },
	{ C2, QV },
	{ Silence, SEM },

	{ E2, Cr },
	{ F2, Cr },
	{ E2, Cr },
	{ Ddi2, Cr },
	{ E2, Cr },
	{ Silence, SQ},
	{ H1, QV},
	{ Silence, SEM },

	{ E2, Cr },
	{ C3, Cr },
	{ H2, Cr },
	{ A2, Cr },

	{ E2, Cr },
	{ G2, Cr },
	{ Silence, Qu },
	{ F2, Qu },
	{ E2, Cr },
	{ F2, Cr },
	{ E2, Qu },
	{ F2, Qu },
	{ E2, Qu },
	{ D2, SEM },
	{ Silence, SEM },
	{ 0xffff, 0xff},
};

Nota_Type* Melodies[Melody_Amount]={
	Melody_March,
	Melody_Bumer,
	Melody_Dzeltentem,
	Melody_Pantera,
};

/** @} */
