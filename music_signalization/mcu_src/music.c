/**
 * @file music.c
 * @brief
 * @author ������� �.�.
 * @date 14.06.2016
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/io.h>
#include <avr/pgmspace.h>

#include "melodies.h"
#include "music.h"

//================================= Definition ================================

typedef struct{
  uint8_t melody_idx;
  uint16_t nota_idx;
  Nota_Type *current_nota;
  uint16_t nota_timer;
  uint8_t is_started;
} music_t;

//================================ Declaration ================================

music_t music;

// ============================== Implementation ==============================

void music_init(void)
{
  // ��� ����������� ���������
  DDRB |= (1<<PB1);

  /*
   * ��������� �������.
   * ��� �������. ����� ��� ����������. ������� ����� ��� ����������.
   */
  OCR1A = 0;
  TCCR1A = (0<<COM1A1)|(1<<COM1A0)|(0<<WGM11)|(0<<WGM10);
  TCCR1B = (0<<WGM13)|(1<<WGM12)|(0<<CS22)|(0<<CS21)|(1<<CS20);

  music.is_started = 0;
}

void music_restart(void)
{
  music.melody_idx = 0;
  music.nota_idx = 0;
  music.current_nota = (Nota_Type *)Melodies[music.melody_idx];
  music.nota_timer = 0;
  music.is_started = 1;
}

void music_stop(void)
{
  OCR1A = 0;
  music.is_started = 0;
}

void music_process(void)
{
  if (music.is_started == 0)
  {
    return;
  }

  if (music.nota_timer == 0)
  {
    // ������� ���� �� flash
    uint16_t freq = pgm_read_word(&music.current_nota[music.nota_idx].freq);
    uint8_t duration = pgm_read_byte(&music.current_nota[music.nota_idx].paus);

    // ���� ������� �����������
    if ((freq == 0xffff) && (duration == 0xff))
    {
      music.nota_idx = 0;

      music.melody_idx++;
      if (music.melody_idx >= Melody_Amount)
      {
        music.melody_idx = 0;
      }
      music.current_nota = (Nota_Type *)Melodies[music.melody_idx];
    }
    else
    {
      // ������ ����
      OCR1A = freq;
      music.nota_timer = ((uint16_t)duration) * 15;
      music.nota_idx++;
    }
  }
  else
  {
    music.nota_timer--;
  }
}

/** @} */
