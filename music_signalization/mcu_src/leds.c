/**
 * @file leds.c
 * @brief
 * @author ������� �.�.
 * @date 11.06.2016
 * @addtogroup
 * @{
*/

#include <stdint.h>

#include "leds.h"

//================================= Definition ================================

//================================ Declaration ================================

// ============================== Implementation ==============================

void leds_init(void)
{
  LED_BLUE_DDR |= (1 << LED_BLUE_PIN);
  LED_GREEN_DDR |= (1 << LED_GREEN_PIN);
  LED_RED_DDR |= (1 << LED_RED_PIN);
}

/** @} */
