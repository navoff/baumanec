/**
 * @file main.c
 * @brief
 * @author ������� �.�.
 * @date 17.06.2014
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <string.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "errors_type.h"
#include "tick_timer.h"
#include "milestag.h"

//================================= Definition ================================

#define LED_INIT()            DDRB |= (1 << PB1)
#define LED_ON()              PORTB |= (1 << PB1)
#define LED_OFF()             PORTB &= ~(1 << PB1)
#define LED_SWITCH()          PORTB ^= (1 << PB1)

//#define ADC_SET_CH_LEFT_RIGHT()       ADMUX &= ~(0xF << MUX0); ADMUX |= (4 << MUX0)
//#define ADC_SET_CH_UP_DOWN()          ADMUX &= ~(0xF << MUX0); ADMUX |= (5 << MUX0)
#define ADC_SET_CH_UP_DOWN()          ADMUX &= ~(0xF << MUX0); ADMUX |= (4 << MUX0)
#define ADC_SET_CH_LEFT_RIGHT()       ADMUX &= ~(0xF << MUX0); ADMUX |= (5 << MUX0)
#define ADC_START_CONVERSION()        ADCSRA |= (1 << ADSC)


#define ADC_BUF_SIZE              4
typedef struct {
  uint8_t buf[ADC_BUF_SIZE];
  uint8_t idx;
  uint16_t mean;
} adc_ch_t;

typedef enum{
  CH__UP_DOWN,
  CH__LEFT_RIGHT,
} channel_t;

typedef struct{
  adc_ch_t ch_up_down;
  adc_ch_t ch_left_right;
  channel_t ch;
} adc_t;

//================================ Declaration ================================

void adc_init(adc_ch_t *adc);
void adc_process(adc_ch_t *adc, uint8_t data);

adc_t adc;
channel_t tx_ch;

// ============================== Implementation ==============================

int main(void)
{
  DDRC &= ~((1 << PC5) | (1 << PC4));
  ADMUX = (0 << REFS1) | (0 << REFS1) | (1 << ADLAR);

  ADCSRA = (1 << ADEN) | (1 << ADSC) | (0 << ADFR) |
      (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
  ADC_SET_CH_UP_DOWN();
  adc.ch = CH__UP_DOWN;
  tx_ch = CH__UP_DOWN;

  LED_INIT();
  tick_init();
  milestag_init();
  adc_init(&adc.ch_left_right);
  adc_init(&adc.ch_up_down);

  uint8_t timer_1ms = 0;
  uint8_t timer_10ms = 0;
  uint16_t timer_100ms = 0;

  uint16_t tx_timestamp;
  START_TIMER(tx_timestamp);

  sei();

  while (1)
  {
    if (is_tick())
    {
      timer_1ms++;
      timer_10ms++;
      timer_100ms++;
    }

    if (ADCSRA & (1 << ADIF))
    {
      ADCSRA |= (1 << ADIF);
      if (adc.ch == CH__UP_DOWN)
      {
        adc_process(&adc.ch_up_down, ADCH);
        ADC_SET_CH_LEFT_RIGHT();
        ADC_START_CONVERSION();
        adc.ch = CH__LEFT_RIGHT;
      }
      else
      {
        adc_process(&adc.ch_left_right, ADCH);
        ADC_SET_CH_UP_DOWN();
        ADC_START_CONVERSION();
        adc.ch = CH__UP_DOWN;
      }
    }

    if (timer_1ms >= MS_TO_TICKS(1))
    {
      timer_1ms = 0;
    }

    if (timer_10ms >= MS_TO_TICKS(10))
    {
      timer_10ms = 0;
    }

    if (CHECK_TIMER(tx_timestamp, 20))
    {
      milestag_packet_t packet;
      if (tx_ch == CH__LEFT_RIGHT)
      {
        packet.id = MILESTAG_ID__CH_LEFT_RIGHT;
        packet.data = adc.ch_left_right.mean / ADC_BUF_SIZE;
        tx_ch = CH__UP_DOWN;
      }
      else
      {
        packet.id = MILESTAG_ID__CH_UP_DOWN;
        packet.data = adc.ch_up_down.mean / ADC_BUF_SIZE;
        tx_ch = CH__LEFT_RIGHT;
      }

      if (milestag_tx_packet(&packet) == ERR_OK)
      {
        START_TIMER(tx_timestamp);
        LED_SWITCH();
      }
    }
  }
}

void adc_init(adc_ch_t *adc)
{
  memset(adc->buf, 0, ADC_BUF_SIZE);
  adc->idx = 0;
  adc->mean = 0;
}

void adc_process(adc_ch_t *adc, uint8_t data)
{
  adc->mean -= adc->buf[adc->idx];
  adc->buf[adc->idx] = data;
  adc->mean += data;
  adc->idx++;
  if (adc->idx >= ADC_BUF_SIZE)
  {
    adc->idx = 0;
  }
}

/** @} */
