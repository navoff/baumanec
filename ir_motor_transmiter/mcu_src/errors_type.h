/**
 * @file errors_type.h
 * @brief
 * @author ������� �.�.
 * @date 22.05.2012
 * @addtogroup
 * @{
*/

#ifndef ERRORS_TYPE_H_
#define ERRORS_TYPE_H_

//================================= Definition ================================

#define ERR_OK          0
#define ERR_BUSY        1
#define ERR_BOF         2
#define ERR_CRC         3
#define ERR_RESULT      4
#define ERR_BUF         5

//================================ Declaration ================================


#endif /* ERRORS_TYPE_H_ */

/** @} */
