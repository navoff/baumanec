/**
 * @file milestag.c
 * @brief
 * @author ������� �.�.
 * @date 07.05.2014
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <string.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "errors_type.h"
#include "main.h"
#include "tick_timer.h"
#include "milestag.h"

//================================= Definition ================================

#define IR_RX_PORT                PORTD
#define IR_RX_P                   PD3
#define IR_RX_DDR                 (*(&IR_RX_PORT - 1))
#define IR_RX_PIN                 (*(&IR_RX_PORT - 2))

#define IR_TX_PORT                PORTB
#define IR_TX_P                   PB3
#define IR_TX_DDR                 (*(&IR_TX_PORT - 1))
#define IR_TX_PIN                 (*(&IR_TX_PORT - 2))

/// ���������� ������ ��� ������������ 600 ���.
#define TX_BIT_DURATION           3000
#define TX_HEADER_DURATION        (TX_BIT_DURATION * 4)
#define TX_ONE_DURATION           (TX_BIT_DURATION * 2)
#define TX_ZERO_DURATION          (TX_BIT_DURATION * 1)
#define TX_PAUSE_DURATION         (TX_BIT_DURATION * 1)

#define RX_BIT_DURATION           (US_TO_TICKS(375))
#define RX_BIT_DEVIATION          (US_TO_TICKS(125))
#define RX_HEADER_DURATION        (RX_BIT_DURATION * 4)
#define RX_ONE_DURATION           (RX_BIT_DURATION * 2)
#define RX_ZERO_DURATION          (RX_BIT_DURATION * 1)
#define RX_PAUSE_DURATION         (RX_BIT_DURATION * 1)
#define RX_IS(cnt,UNIT)           ((cnt >= (RX_##UNIT##_DURATION - RX_BIT_DEVIATION)) && \
                                   (cnt <= (RX_##UNIT##_DURATION + RX_BIT_DEVIATION)))

#define IR_TX_OSC_STOP()          TCCR2 = 0; IR_TX_PORT &= ~(1 << IR_TX_P)
#define IR_TX_OSC_START()         TCCR2 = ((1 << WGM21) | (0 << WGM20) | \
                                           (0 << COM21) | (1 << COM20) | \
                                           (0 << CS22) | (0 << CS21) | (1 << CS20))

#define MILESTAG_FIFO_RX_PACKET_BUF_SIZE        4
#define MILESTAG_RX_BUF_SIZE                    8
#define MILESTAG_TX_BUF_SIZE                    8
/// @note ������ ���� ������ 2.
#define MILESTAG_RX_TICK_FIFO_BUF_SIZE          8

/*
 * ������� ���������� ��� - LSB.
 * ������� ���������� ���� - little-endian.
 */

typedef struct {
  struct{
    enum{
        MILESTAG_TX_ST__NONE,
        MILESTAG_TX_ST__IN_PROG,
      } state;
    uint8_t buf[MILESTAG_TX_BUF_SIZE];
    uint8_t buf_idx;
    uint8_t buf_msk;
    uint8_t bit_remain;
  } tx;
  struct{
    struct{
      uint8_t buf[MILESTAG_RX_TICK_FIFO_BUF_SIZE];
      uint8_t tail;
      uint8_t head;
      uint8_t idx;
    } tick_fifo;
    uint8_t tick_buf;
    uint8_t buf[MILESTAG_RX_BUF_SIZE];
    uint8_t buf_idx;
    uint8_t bit_idx;
    enum{
      MILESTAG_RX_ST__HEADER,
      MILESTAG_RX_ST__PAUSE,
      MILESTAG_RX_ST__DATA,
    } state;
  } rx;
  struct{
    milestag_packet_t buf[MILESTAG_FIFO_RX_PACKET_BUF_SIZE];
    uint8_t tail;
    uint8_t head;
    uint8_t idx;
  } fifo_rx_packet;
} milestag_t;

//================================ Declaration ================================

uint8_t milestag_tx_buf(uint8_t buf[], uint8_t size);
uint8_t milestag_fifo_rx_packet_put(milestag_packet_t *packet);

volatile milestag_t milestag;

// ============================== Implementation ==============================

void milestag_init(void)
{
  IR_RX_DDR &= ~(1 << IR_RX_P);
  IR_RX_PORT |= (1 << IR_RX_P);

  IR_TX_DDR |= (1 << IR_TX_P);
  IR_TX_PORT &= ~(1 << IR_TX_P);

  OCR2 = 110; // ��� ������������ ������� 36 ���
  TCCR2 = 0;

  OCR1B = TX_PAUSE_DURATION;
  TCCR1A = (0 << WGM11) | (0 << WGM10);
  TCCR1B = 0;

  // ����� �����
  MCUCR |= (0 << ISC11) | (1 << ISC10);
  GIFR = (1 << INTF1);
  GICR |= (1 << INT1);

  milestag.fifo_rx_packet.tail = 0;
  milestag.fifo_rx_packet.head = 0;
  milestag.fifo_rx_packet.idx = 0;

  milestag.rx.tick_fifo.idx = 0;
  milestag.rx.tick_fifo.head = 0;
  milestag.rx.tick_fifo.tail = 0;
  milestag.rx.buf_idx = 0;
  milestag.rx.state = MILESTAG_RX_ST__HEADER;

  milestag.tx.state = MILESTAG_TX_ST__NONE;
}

uint8_t milestag_tx_packet(milestag_packet_t *packet)
{
  return milestag_tx_buf((uint8_t *)packet, sizeof(milestag_packet_t));
}

void milestag_rx_process(void)
{
  while (milestag.rx.tick_fifo.idx)
  {
    cli();
    uint8_t cnt = milestag.rx.tick_fifo.buf[milestag.rx.tick_fifo.tail++];
    milestag.rx.tick_fifo.tail &= (MILESTAG_RX_TICK_FIFO_BUF_SIZE - 1);
    milestag.rx.tick_fifo.idx--;
    sei();

    uint8_t res = 0xFF;
    switch (milestag.rx.state)
    {
      case MILESTAG_RX_ST__HEADER:
        if (RX_IS(cnt, HEADER))
        {
          milestag.rx.buf_idx = 0;
          milestag.rx.buf[0] = 0;
          milestag.rx.bit_idx = 0;
          milestag.rx.state = MILESTAG_RX_ST__PAUSE;
        }
      break;
      case MILESTAG_RX_ST__PAUSE:
        if (RX_IS(cnt, PAUSE))
        {
          milestag.rx.state = MILESTAG_RX_ST__DATA;
        }
        else
        {
          milestag.rx.state = MILESTAG_RX_ST__HEADER;
        }
      break;
      case MILESTAG_RX_ST__DATA:
        if (RX_IS(cnt, ONE))
        {
          milestag.rx.state = MILESTAG_RX_ST__PAUSE;
          res = 1;
        }
        else if (RX_IS(cnt, ZERO))
        {
          milestag.rx.state = MILESTAG_RX_ST__PAUSE;
          res = 0;
        }
        else if (RX_IS(cnt, HEADER))
        {
          milestag.rx.state = MILESTAG_RX_ST__PAUSE;
        }
        else
        {
          milestag.rx.state = MILESTAG_RX_ST__HEADER;
        }
        if (res != 0xFF)
        {
          milestag.rx.buf[milestag.rx.buf_idx] |= (res << milestag.rx.bit_idx);
          milestag.rx.bit_idx++;
          if (milestag.rx.bit_idx >= 8)
          {
            milestag.rx.bit_idx = 0;
            milestag.rx.buf_idx++;
            milestag.rx.buf[milestag.rx.buf_idx] = 0;
            if (milestag.rx.buf_idx >= sizeof(milestag_packet_t))
            {
              milestag_fifo_rx_packet_put((milestag_packet_t *)milestag.rx.buf);
              milestag.rx.state = MILESTAG_RX_ST__HEADER;
            }
          }
        }
      break;
    }
  }
}

uint8_t milestag_tx_buf(uint8_t buf[], uint8_t size)
{
  uint8_t res = ERR_RESULT;

  if ((milestag.tx.state == MILESTAG_TX_ST__NONE) &&
      (size < MILESTAG_TX_BUF_SIZE))
  {
    milestag.tx.state = MILESTAG_TX_ST__IN_PROG;
    for (uint8_t i = 0; i < size; i++)
    {
      milestag.tx.buf[i] = buf[i];
    }
    milestag.tx.bit_remain = size * 8;
    milestag.tx.buf_idx = 0;
    milestag.tx.buf_msk = 0x01;

    IR_TX_OSC_START();
    OCR1B = TX_PAUSE_DURATION;
    OCR1A = TX_PAUSE_DURATION + TX_HEADER_DURATION;
    TCNT1 = TX_PAUSE_DURATION + 1;
    TIMSK |= (1 << OCIE1A) | (1 << OCIE1B);
    TIFR = (1 << OCF1A) | (1 << OCF1B);
    TCCR1B = (0 << WGM13) | (1 << WGM12) | (0 << CS12) | (0 << CS11) | (1 << CS10);

    res = ERR_OK;
  }

  return res;
}

uint8_t milestag_fifo_rx_packet_put(milestag_packet_t *packet)
{
  if (milestag.fifo_rx_packet.idx >= MILESTAG_FIFO_RX_PACKET_BUF_SIZE)
  {
    return ERR_BOF;
  }

  memcpy((void *)&milestag.fifo_rx_packet.buf[milestag.fifo_rx_packet.head++],
         packet,
         sizeof(milestag_packet_t));
  if (milestag.fifo_rx_packet.head >= MILESTAG_FIFO_RX_PACKET_BUF_SIZE)
  {
    milestag.fifo_rx_packet.head = 0;
  }
  milestag.fifo_rx_packet.idx++;

  return ERR_OK;
}

uint8_t milestag_fifo_rx_packet_get(milestag_packet_t *packet)
{
  if (milestag.fifo_rx_packet.idx == 0)
  {
    return ERR_BUF;
  }

  memcpy(packet,
         (void *)&milestag.fifo_rx_packet.buf[milestag.fifo_rx_packet.tail++],
         sizeof(milestag_packet_t));
  if (milestag.fifo_rx_packet.tail >= MILESTAG_FIFO_RX_PACKET_BUF_SIZE)
  {
    milestag.fifo_rx_packet.tail = 0;
  }
  milestag.fifo_rx_packet.idx--;

  return ERR_OK;
}

ISR(SIG_OUTPUT_COMPARE1B)
{
  IR_TX_OSC_START();
}

ISR(SIG_OUTPUT_COMPARE1A)
{
  IR_TX_OSC_STOP();
  if (milestag.tx.bit_remain)
  {
    if (milestag.tx.buf[milestag.tx.buf_idx] & milestag.tx.buf_msk)
    {
      OCR1A = TX_PAUSE_DURATION + TX_ONE_DURATION;
    }
    else
    {
      OCR1A = TX_PAUSE_DURATION + TX_ZERO_DURATION;
    }
    milestag.tx.buf_msk <<= 1;
    if (milestag.tx.buf_msk == 0)
    {
      milestag.tx.buf_msk = 0x01;
      milestag.tx.buf_idx++;
    }
    milestag.tx.bit_remain--;
  }
  else
  {
    TCCR1B = 0;
    milestag.tx.state = MILESTAG_TX_ST__NONE;
  }
}

ISR(INT1_vect)
{
  uint8_t cnt = TICK_COUNTER_L();
  if (milestag.rx.tick_fifo.idx < MILESTAG_RX_TICK_FIFO_BUF_SIZE)
  {
    milestag.rx.tick_fifo.buf[milestag.rx.tick_fifo.head++] = cnt - milestag.rx.tick_buf;
    milestag.rx.tick_fifo.head &= (MILESTAG_RX_TICK_FIFO_BUF_SIZE - 1);
    milestag.rx.tick_fifo.idx++;
  }
  milestag.rx.tick_buf = cnt;
}

/** @} */
