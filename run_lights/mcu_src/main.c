/**
 * @file main.c
 * @brief
 * @author ������� �.�.
 * @date 30.05.2011
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>

#include "effects.h"

//================================= Definition ================================

//================================ Declaration ================================

// ============================== Implementation ==============================

int main (void)
{
	uint8_t unit_idx = 0;
	uint8_t takt_idx = 0;

	uint8_t flag_btn_press = 1;
	uint8_t buf_pin = 1;

	PORTB |= (1<<PB1);

	// ��������� �����
	DDRD = 0xff;
	PORTD = 0xff;

	// ��������� �������
	OCR1A = 1000; // 10 = 1.3��
	TCCR1A = 0;
	TCCR1B = (1<<WGM12)|(1<<CS12)|(0<<CS11)|(1<<CS10); // clk/1024

	while(1)
	{
	  // ���� ������ ������
		if (flag_btn_press)
		{
			flag_btn_press = 0;

			// ������� �� ������ ������
			unit_idx++;
			if (unit_idx >= effects.amount)
			{
				unit_idx = 0;
			}
			takt_idx = 0;
		}
#if defined(__AVR_ATmega48__)
    if (bit_is_set(TIFR1, OCF1A))
    {
      TIFR1 = (1<<OCF1A);
#elif defined(__AVR_ATmega8__)
		if (bit_is_set(TIFR,OCF1A))
		{
			TIFR = (1<<OCF1A);
#endif
			takt_idx++;
			if (takt_idx >= effects.unit[unit_idx]->len)
			{
				takt_idx = 0;
			}
			PORTD = effects.unit[unit_idx]->tack[takt_idx];
		}

		if (bit_is_clear(PINB,PB1))
		{
			buf_pin = 0;
		}
		if (bit_is_set(PINB,PB1) && (buf_pin == 0))
		{
			buf_pin = 1;
			flag_btn_press = 1;
			_delay_ms(1);
		}
	} // end while
} // end main

/** @} */
