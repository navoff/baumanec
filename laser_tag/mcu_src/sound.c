/**
 * @file sound.c
 * @brief
 * @author ������� �.�.
 * @date 11.05.2014
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "tick_timer.h"
#include "main.h"
#include "sound.h"

//================================= Definition ================================

#define SOUND_HAND_PORT                     PORTC
#define SOUND_HAND_P                        PC5
#define SOUND_HAND_DDR                      (*(&SOUND_HAND_PORT - 1))



#define SOUND_PERIOD_SHOT_START             15
#define SOUND_PERIOD_SHOT_STOP              5
#define SOUND_SHOT_CHANGE_PERIOD            10

#define SOUND_PERIOD_GAME_BEGIN_START       15
#define SOUND_PERIOD_GAME_BEGIN_STOP        5

#define SOUND_PERIOD_WOUND_BEGIN            15
#define SOUND_DURATION_WOUND_BEGIN          500

#define SOUND_PERIOD_WOUND_END              5
#define SOUND_DURATION_WOUND_END            100

#define SOUND_PERIOD_KILL                   15
#define SOUND_DURATION_KILL                 2000

//================================ Declaration ================================

// ============================== Implementation ==============================

void sound_init(void)
{
  SOUND_HAND_DDR |= (1 << SOUND_HAND_P);
  SOUND_HAND_PORT &= ~(1 << SOUND_HAND_P);

  sound.req.msk = 0;
  sound.state = SOUND_ST__WAIT;
}

void sound_pwm_process(void)
{
  if (sound.period)
  {
    sound.cnt++;
    if (sound.cnt >= sound.period)
    {
      SOUND_HAND_PORT ^= (1 << SOUND_HAND_P);
      sound.cnt = 0;
    }
  }
}

void sound_process(void)
{
  switch(sound.state)
  {
    case SOUND_ST__WAIT:
      if (sound.req.wound_begin || sound.req.wound_end || sound.req.kill)
      {
        if (sound.req.wound_begin)
        {
          sound.period = SOUND_PERIOD_WOUND_BEGIN;
          sound.state = SOUND_ST__WOUND_BEGIN;
        }
        else if (sound.req.wound_end)
        {
          sound.period = SOUND_PERIOD_WOUND_END;
          sound.state = SOUND_ST__WOUND_END;
        }
        else if (sound.req.kill)
        {
          sound.period = SOUND_PERIOD_KILL;
          sound.state = SOUND_ST__KILL;
        }
        sound.req.wound_begin = 0;
        sound.req.wound_end = 0;
        sound.req.kill = 0;
        START_TIMER(sound.timestamp);
        break;
      }

      if (sound.req.shot)
      {
        sound.req.shot = 0;
        sound.period = SOUND_PERIOD_SHOT_START;
        START_TIMER(sound.timestamp);
        sound.state = SOUND_ST__SHOT;
        break;
      }

      if (sound.req.game_begin)
      {
        sound.req.game_begin = 0;
        sound.period = SOUND_PERIOD_GAME_BEGIN_START;
        sound.idx = 0;
        START_TIMER(sound.timestamp);
        sound.state = SOUND_ST__GAME_BEGIN;
        break;
      }
    break;

    case SOUND_ST__GAME_BEGIN:
      if ((sound.idx == 0) || (sound.idx == 2))
      {
        if (CHECK_TIMER(sound.timestamp, 200))
        {
          SOUND_HAND_PORT &= ~(1 << SOUND_HAND_P);
          sound.period = 0;
          START_TIMER(sound.timestamp);
          sound.idx++;
        }
      }
      else if ((sound.idx == 1))
      {
        if (CHECK_TIMER(sound.timestamp, 200))
        {
          sound.period = SOUND_PERIOD_GAME_BEGIN_START;
          START_TIMER(sound.timestamp);
          sound.idx++;
        }
      }
      else if (sound.idx == 3)
      {
        if (CHECK_TIMER(sound.timestamp, 200))
        {
          START_TIMER(sound.timestamp);
          sound.period = SOUND_PERIOD_GAME_BEGIN_STOP;
          sound.idx++;
        }
      }
      else
      {
        if (CHECK_TIMER(sound.timestamp, 500))
        {
          SOUND_HAND_PORT &= ~(1 << SOUND_HAND_P);
          sound.period = 0;
          sound.state = SOUND_ST__WAIT;
        }
      }
    break;

    case SOUND_ST__SHOT:
      if (CHECK_TIMER(sound.timestamp, SOUND_SHOT_CHANGE_PERIOD))
      {
        START_TIMER(sound.timestamp);
        sound.period--;
        if (sound.period < SOUND_PERIOD_SHOT_STOP)
        {
          SOUND_HAND_PORT &= ~(1 << SOUND_HAND_P);
          sound.period = 0;
          sound.state = SOUND_ST__WAIT;
        }
      }
    break;

    case SOUND_ST__WOUND_BEGIN:
      if (CHECK_TIMER(sound.timestamp, SOUND_DURATION_WOUND_BEGIN))
      {
        SOUND_HAND_PORT &= ~(1 << SOUND_HAND_P);
        sound.period = 0;
        sound.state = SOUND_ST__WAIT;
      }
    break;
    case SOUND_ST__WOUND_END:
      if (CHECK_TIMER(sound.timestamp, SOUND_DURATION_WOUND_END))
      {
        SOUND_HAND_PORT &= ~(1 << SOUND_HAND_P);
        sound.period = 0;
        sound.state = SOUND_ST__WAIT;
      }
    break;
    case SOUND_ST__KILL:
      if (CHECK_TIMER(sound.timestamp, SOUND_DURATION_KILL))
      {
        SOUND_HAND_PORT &= ~(1 << SOUND_HAND_P);
        sound.period = 0;
        sound.state = SOUND_ST__WAIT;
      }
    break;
  }
}

/** @} */
