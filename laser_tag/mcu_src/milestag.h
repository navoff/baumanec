/**
 * @file milestag.h
 * @brief
 * @author ������� �.�.
 * @date 07.05.2014
 * @addtogroup
 * @{
*/

#ifndef MILESTAG_H_
#define MILESTAG_H_

//================================= Definition ================================

#define MILESTAG_ID__SHOOT_TEAM_1          0x01
#define MILESTAG_ID__SHOOT_TEAM_2          0x02

typedef struct{
  uint8_t id;
  uint8_t data;
} milestag_packet_t;

//================================ Declaration ================================

void    milestag_init(void);
uint8_t milestag_tx_packet(milestag_packet_t *packet);
void    milestag_rx_process(void);
uint8_t milestag_fifo_rx_packet_get(milestag_packet_t *packet);

#endif /* MILESTAG_H_ */

/** @} */
