/**
 * @file main.h
 * @brief
 * @author ������� �.�.
 * @date 06.05.2014
 * @addtogroup
 * @{
*/

#ifndef MAIN_H_
#define MAIN_H_

//================================= Definition ================================

#define LED_INIT()            DDRD |= (1 << PD4)
#define LED_ON()              PORTD |= (1 << PD4)
#define LED_OFF()             PORTD &= ~(1 << PD4)
#define LED_SWITCH()          PORTD ^= (1 << PD4)

//================================ Declaration ================================


#endif /* MAIN_H_ */

/** @} */
