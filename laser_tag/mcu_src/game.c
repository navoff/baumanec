/**
 * @file game.c
 * @brief
 * @author ������� �.�.
 * @date 11.05.2014
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/io.h>

#include "errors_type.h"
#include "milestag.h"
#include "sound.h"
#include "tick_timer.h"
#include "main.h"

#include "game.h"

//================================= Definition ================================

// ����� �������
#define TEAM1
//#define TEAM2

#define WOUND_DURATION                3000
#define LIFE_AMOUNT                   3

#define LED_LIFE_1_INIT()             DDRB |= (1 << PB0)
#define LED_LIFE_1_ON()               PORTB |= (1 << PB0)
#define LED_LIFE_1_OFF()              PORTB &= ~(1 << PB0)
#define LED_LIFE_2_INIT()             DDRD |= (1 << PD7)
#define LED_LIFE_2_ON()               PORTD |= (1 << PD7)
#define LED_LIFE_2_OFF()              PORTD &= ~(1 << PD7)
#define LED_LIFE_3_INIT()             DDRD |= (1 << PD6)
#define LED_LIFE_3_ON()               PORTD |= (1 << PD6)
#define LED_LIFE_3_OFF()              PORTD &= ~(1 << PD6)

//================================ Declaration ================================

void game_life_amount_indication(uint8_t life_amount);

game_t game;

// ============================== Implementation ==============================

void game_init(void)
{
  LED_LIFE_1_INIT();
  LED_LIFE_2_INIT();
  LED_LIFE_3_INIT();
  game.state = GAME_ST__NONE;
}

void game_process(void)
{
  game_life_amount_indication(game.life_amount);

  milestag_packet_t packet;
  uint8_t res = milestag_fifo_rx_packet_get(&packet);

  switch(game.state)
  {
    case GAME_ST__NONE:
      sound.req.game_begin = 1;
      game.state = GAME_ST__GAME_BEGIN_INDICATION;
    break;

    case GAME_ST__GAME_BEGIN_INDICATION:
      if (sound.state == SOUND_ST__WAIT)
      {
        LED_ON();
        game.life_amount = LIFE_AMOUNT;
        game.req_shot = 0;
        game.state = GAME_ST__BATTLE;
      }
    break;

    case GAME_ST__BATTLE:
      if (res == ERR_OK)
      {
        #if defined(TEAM1)
        if (packet.id == MILESTAG_ID__SHOOT_TEAM_2)
        #elif defined(TEAM2)
        if (packet.id == MILESTAG_ID__SHOOT_TEAM_1)
        #endif
        {
          game.life_amount--;
          if (game.life_amount == 0)
          {
            sound.req.kill = 1;
            game.state = GAME_ST__KILL;
          }
          else
          {
            sound.req.wound_begin = 1;
            START_TIMER(game.timestamp);
            game.state = GAME_ST__WOUND;
          }
          LED_OFF();
          break;
        }
      }

      if (game.req_shot)
      {
        game.req_shot = 0;
        sound.req.shot = 1;
        milestag_packet_t packet;
        #if defined(TEAM1)
        packet.id = MILESTAG_ID__SHOOT_TEAM_1;
        #elif defined(TEAM2)
        packet.id = MILESTAG_ID__SHOOT_TEAM_2;
        #endif
        packet.data = 1;
        milestag_tx_packet(&packet);
      }
    break;

    case GAME_ST__WOUND:
      if (CHECK_TIMER(game.timestamp, WOUND_DURATION))
      {
        LED_ON();
        sound.req.wound_end = 1;
        game.req_shot = 0;
        game.state = GAME_ST__BATTLE;
        break;
      }
    break;

    case GAME_ST__KILL:
    break;
  }
}

void game_life_amount_indication(uint8_t life_amount)
{
  if (life_amount >= 1)
  {
    LED_LIFE_1_ON();
  }
  else
  {
    LED_LIFE_1_OFF();
  }
  if (life_amount >= 2)
  {
    LED_LIFE_2_ON();
  }
  else
  {
    LED_LIFE_2_OFF();
  }
  if (life_amount >= 3)
  {
    LED_LIFE_3_ON();
  }
  else
  {
    LED_LIFE_3_OFF();
  }

}

/** @} */
