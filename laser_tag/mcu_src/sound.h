/**
 * @file sound.h
 * @brief
 * @author ������� �.�.
 * @date 11.05.2014
 * @addtogroup
 * @{
*/

#ifndef SOUND_H_
#define SOUND_H_

//================================= Definition ================================

typedef struct{
  enum{
    SOUND_ST__WAIT,
    SOUND_ST__SHOT,
    SOUND_ST__GAME_BEGIN,
    SOUND_ST__WOUND_BEGIN,
    SOUND_ST__WOUND_END,
    SOUND_ST__KILL,
  } state;
  uint8_t period;
  uint8_t cnt;
  uint8_t idx;
  union{
    struct{
      uint8_t shot         :1;
      uint8_t game_begin    :1;
      uint8_t wound_begin   :1;
      uint8_t wound_end     :1;
      uint8_t kill          :1;
    };
    uint8_t msk;
  } req;
  uint16_t timestamp;
} sound_t;


//================================ Declaration ================================

void sound_init(void);
void sound_pwm_process(void);
void sound_process(void);

sound_t sound;

#endif /* SOUND_H_ */

/** @} */
