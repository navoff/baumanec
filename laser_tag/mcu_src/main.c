/**
 * @file main.c
 * @brief
 * @author ������� �.�.
 * @date 06.05.2014
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "milestag.h"
#include "tick_timer.h"
#include "uart.h"
#include "errors_type.h"
#include "sound.h"
#include "game.h"
#include "main.h"

#define BTN_PORT              PORTC
#define BTN_P                 PC4
#define BTN_DDR               (*(&BTN_PORT - 1))
#define BTN_PIN               (*(&BTN_PORT - 2))

//================================= Definition ================================

//================================ Declaration ================================

void game_process(void);

// ============================== Implementation ==============================

int main(void)
{
  LED_INIT();

  BTN_PORT |= (1 << BTN_P);
  uint8_t buf_pin = 0;

  uint8_t timer_1ms = 0;
  uint8_t timer_10ms = 0;

  sound_init();
  milestag_init();
  tick_init();
  uart_init(BAUDRATE__1000000);
  game_init();

  sei();

  while(1)
  {
    if (bit_is_set(BTN_PIN, BTN_P))
    {
      buf_pin = 1;
    }

    if (bit_is_clear(BTN_PIN, BTN_P) && (buf_pin))
    {
      buf_pin = 0;
      game.req_shot = 1;
    }

    uart_tx_process();

    if (is_tick())
    {
      timer_1ms++;
      timer_10ms++;

      sound_pwm_process();
    }

    if (timer_1ms >= MS_TO_TICKS(1))
    {
      timer_1ms = 0;

      sound_process();
      milestag_rx_process();
    }

    if (timer_10ms >= MS_TO_TICKS(10))
    {
      timer_10ms = 0;

      game_process();
    }
  }
}


/** @} */
