/**
 * @file game.h
 * @brief
 * @author ������� �.�.
 * @date 11.05.2014
 * @addtogroup
 * @{
*/

#ifndef GAME_H_
#define GAME_H_

//================================= Definition ================================

typedef struct{
  enum{
    GAME_ST__NONE,
    GAME_ST__GAME_BEGIN_INDICATION,
    GAME_ST__BATTLE,
    GAME_ST__WOUND,
    GAME_ST__KILL,
  } state;
  uint16_t timestamp;
  uint8_t life_amount;
  uint8_t req_shot;
} game_t;

//================================ Declaration ================================

void game_init(void);
void game_process(void);

extern game_t game;

#endif /* GAME_H_ */

/** @} */
