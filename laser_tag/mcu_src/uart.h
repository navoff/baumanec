/**
 * @file uart.h
 * @brief
 * @author ������� �.�.
 * @date 22.05.2012
 * @addtogroup
 * @{
*/

#ifndef UART_H_
#define UART_H_

//================================= Definition ================================

#define FIFO_UART_RX_BUF_SIZE       150
#define FIFO_UART_TX_BUF_SIZE       20

#define BAUDRATE__9600              1
#define BAUDRATE__1000000           2

typedef struct{
  uint8_t code;
  uint8_t data[8];
  uint8_t crc;
}packet_t;

typedef struct{
  uint8_t buf[FIFO_UART_RX_BUF_SIZE];
  uint8_t idx;
  uint8_t head;
  uint8_t tail;
  uint8_t wait_timer;
  uint8_t bof;
}fifo_uart_rx_t;

typedef struct{
  uint8_t buf[FIFO_UART_TX_BUF_SIZE];
  uint8_t idx;
  uint8_t head;
  uint8_t tail;
}fifo_uart_tx_t;

//================================ Declaration ================================

void uart_init(uint8_t baudrate);
void uart_tx_process(void);
void uart_set_baudrate(uint8_t baudrate);
void fifo_uart_rx_get_packet(packet_t *packet);

uint8_t uart_tx_buf(uint8_t buf[], uint8_t size);

extern volatile fifo_uart_rx_t fifo_uart_rx;
extern volatile fifo_uart_tx_t fifo_uart_tx;

#endif /* UART_H_ */

/** @} */
