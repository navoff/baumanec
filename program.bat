echo off

:: ������ ��� �������������
SET /p prog="Programmer type: ft232 (1) or dragon (2)?"
IF /i "%prog%" == "1" (
	SET options_prog=-P ft0 -c ft232_bb
	SET avrdude_path=%PROGRAMFILES%\avrdude_5.03
)
IF /i "%prog%" == "2" (
	SET options_prog=-P usb -c dragon_isp
	SET avrdude_path=%PROGRAMFILES%\avrdude_5.11
)

:: ������ ��� ������ (��������)
SET /p memory="Memory type: fuse (1) or flash (2)?"
IF /i "%memory%" == "1" GOTO  set_osc
GOTO  set_project

:: ������ �������� ��������� �������
:set_osc
SET /p osc="Osc type: IntRc (1) or XTAL (2)?"
IF /i "%osc%" == "1" SET options_mem=-B4800 -U hfuse:w:0xD1:m -U lfuse:w:0xE4:m
IF /i "%osc%" == "2" SET options_mem=-B4800 -U hfuse:w:0xC1:m -U lfuse:w:0xCF:m 
GOTO start_prog

:: ������ ��� �������
:set_project
SET /p project="Project name:"
SET options_mem=-U flash:w:"%project%\mcu_src\Release\%project%.hex":a 
GOTO start_prog

:start_prog
@echo "%avrdude_path%\avrdude.exe" -C "%avrdude_path%\avrdude.conf" -p m8 %options_prog% %options_mem% -u
"%avrdude_path%\avrdude.exe" -C "%avrdude_path%\avrdude.conf" -p m8 %options_prog% %options_mem% -u
goto print_prog_result

:print_prog_result
:: ���� � ���� ���������� ��������� ������ ��� ���� ������� �� ���� �������
if ERRORLEVEL 1 goto lError
@echo =====================================================
@echo =                   SUCCESSFULLY                    =
@echo =====================================================
goto lExit

:: ������������ ���� �������� ��� ������ �������
:lError
@echo =====================================================
@echo =                      ERROR                        =
@echo =====================================================
goto lExit

:lExit
PAUSE