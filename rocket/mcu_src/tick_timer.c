/**
 * @file tick_timer.c
 * @brief
 * @author ������� �.�.
 * @date 23.05.2012
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr\io.h>

#include "tick_timer.h"

//================================= Definition ================================

//================================ Declaration ================================

tick_counter_t tick_counter;

// ============================== Implementation ==============================

void tick_init(void)
{
  tick_counter.W = 0;
  TCNT0 = 0;
  TIFR = (1 << TOV0);
#if defined(TICK_PERIOD_125_US)
  TCCR0 = (1 << CS02) | (0 << CS01) | (1 << CS00);
#else
#error undefined TICK_PERIOD
#endif
}

uint8_t is_tick(void)
{
  uint8_t TCNT_buf = TCNT0;
  uint8_t res = TCNT_buf - tick_counter.L;
  if (res) tick_counter.W++;
  return res;
}

/** @} */
