/**
 * @file main.c
 * @brief
 * @author ������� �.�.
 * @date 16.06.2014
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "tick_timer.h"

//================================= Definition ================================

#define LED_BTN_INIT()              DDRC |= (1 << PC5)
#define LED_BTN_ON()                PORTC |= (1 << PC5)
#define LED_BTN_OFF()               PORTC &= ~(1 << PC5)
#define LED_BTN_SWITCH()            PORTC ^= (1 << PC5)

#define LED_FUSE_INIT()             DDRB |= (1 << PB1)
#define LED_FUSE_ON()               PORTB |= (1 << PB1)
#define LED_FUSE_OFF()              PORTB &= ~(1 << PB1)
#define LED_FUSE_SWITCH()           PORTB ^= (1 << PB1)

#define CMD_AMOUNT                  3

typedef enum{
  ROCKET_BTN__OFF,
  ROCKET_BTN__ON,
  ROCKET_BTN__UNDEF,
} rocket_button_t;

typedef struct{
  volatile uint8_t *port;
  uint8_t pin;
} out_t;

typedef struct{
  uint16_t timestamp;
  uint8_t start_req;
  uint8_t in_prog;
  out_t led_out;
  out_t relay_out;
} cmd_t;

typedef struct{
  enum{
    ROCKET_ST__UNPLAG,
    ROCKET_ST__WAIT,
  } state;
  uint16_t rising_edge_stamp;
  uint16_t btn_timestamp;
  uint16_t cmd_timestamp;
  uint16_t cmd_idx;
  rocket_button_t btn;
  rocket_button_t btn_wait_state;
  enum{
    ROCKET_CMD_ST__NONE,
    ROCKET_CMD_ST__START,
    ROCKET_CMD_ST__WAIT_OFF,
    ROCKET_CMD_ST__DELAY,
  } cmd_state;
  uint8_t fused;
} rocket_t;

//================================ Declaration ================================

void btn_process(void);
void cmd_init(cmd_t *cmd,
              volatile uint8_t *relay_port, uint8_t relay_pin,
              volatile uint8_t *led_port, uint8_t led_pin);
void cmd_process(cmd_t *cmd);
void cmd_start(uint8_t cmd_idx);

volatile rocket_t rocket = {
    .state = ROCKET_ST__UNPLAG,
    .btn = ROCKET_BTN__UNDEF,
    .btn_wait_state = ROCKET_BTN__OFF,
    .fused = 1,
    .cmd_state = ROCKET_CMD_ST__NONE,
};

cmd_t cmd[CMD_AMOUNT];

// ============================== Implementation ==============================

int main(void)
{
  LED_BTN_INIT();
  LED_BTN_OFF();

  LED_FUSE_INIT();
  LED_FUSE_OFF();

  tick_init();

  cmd_init(&cmd[0], &PORTD, (1 << PD7), &PORTD, (1 << PD7));
  cmd_init(&cmd[1], &PORTD, (1 << PD7), &PORTD, (1 << PD5));
  cmd_init(&cmd[2], &PORTB, (1 << PB6), &PORTB, (1 << PB6));

  DDRD &= ~((1 << PD2) | (1 << PD3));
  PORTD |= (1 << PD2) | (1 << PD3);

  TCCR1A = 0;
  TCCR1B = (0 << CS12) | (1 << CS11) | (0 << CS10); // clk / 8
//  TIFR = (1 << TOV1);
//  TIMSK |= (1 << TOIE1);

  // INT0 - �������� �����, INT1 - ������ �����
  MCUCR |= (1 << ISC11) | (0 << ISC10) | (1 << ISC01) | (1 << ISC00);
  GIFR = (1 << INTF1) | (1 << INTF0);
  GICR = (1 << INT1) | (1 << INT0);

  uint8_t timer_10ms = 0;
  sei();

  while(1)
  {
    if (is_tick())
    {
      timer_10ms++;
    }

    if (timer_10ms >= MS_TO_TICKS(10))
    {
      timer_10ms = 0;

      btn_process();
      cmd_process(&cmd[0]);
      cmd_process(&cmd[1]);
      cmd_process(&cmd[2]);

      if (rocket.fused)
      {
        LED_FUSE_ON();
      }
      else
      {
        LED_FUSE_OFF();
      }
    }
  }
}

void btn_process(void)
{
  cli();
  rocket_button_t btn = rocket.btn;
  sei();

  if (rocket.fused == 0)
  {
    switch (rocket.cmd_state)
    {
      case ROCKET_CMD_ST__NONE:
        if (btn == ROCKET_BTN__OFF)
        {
          rocket.cmd_state = ROCKET_CMD_ST__START;
        }
      break;
      case ROCKET_CMD_ST__START:
        if (btn == ROCKET_BTN__ON)
        {
          rocket.cmd_idx = 0;
          rocket.cmd_state = ROCKET_CMD_ST__WAIT_OFF;
        }
      break;
      case ROCKET_CMD_ST__WAIT_OFF:
        if (btn == ROCKET_BTN__OFF)
        {
          rocket.cmd_idx++;
          START_TIMER(rocket.cmd_timestamp);
          rocket.cmd_state = ROCKET_CMD_ST__DELAY;
        }
        else if (btn == ROCKET_BTN__UNDEF)
        {
          rocket.cmd_state = ROCKET_CMD_ST__START;
        }
      break;
      case ROCKET_CMD_ST__DELAY:
        if (btn == ROCKET_BTN__OFF)
        {
          if (CHECK_TIMER(rocket.cmd_timestamp, 2000))
          {
            cmd_start(rocket.cmd_idx);
            rocket.cmd_state = ROCKET_CMD_ST__START;
          }
        }
        else if (btn == ROCKET_BTN__ON)
        {
          rocket.cmd_state = ROCKET_CMD_ST__WAIT_OFF;
        }
        else if (btn == ROCKET_BTN__UNDEF)
        {
          rocket.cmd_state = ROCKET_CMD_ST__START;
        }
      break;
    }
  }
  else
  {
    rocket.cmd_state = ROCKET_CMD_ST__NONE;
  }

  if (btn == ROCKET_BTN__ON)
  {
    LED_BTN_ON();

    if (rocket.btn_wait_state == ROCKET_BTN__ON)
    {
      if (CHECK_TIMER(rocket.btn_timestamp, 2000))
      {
        rocket.fused = !rocket.fused;
        rocket.btn_wait_state = ROCKET_BTN__OFF;
      }
    }
  }
  else if (btn == ROCKET_BTN__OFF)
  {
    LED_BTN_OFF();

    if (rocket.btn_wait_state == ROCKET_BTN__OFF)
    {
      rocket.btn_wait_state = ROCKET_BTN__ON;
    }

    START_TIMER(rocket.btn_timestamp);
  }
  else
  {
    LED_BTN_OFF();
    rocket.btn_wait_state = ROCKET_BTN__OFF;
  }
}

void cmd_init(cmd_t *cmd,
              volatile uint8_t *relay_port, uint8_t relay_pin,
              volatile uint8_t *led_port, uint8_t led_pin)
{
  cmd->led_out.port = led_port;
  cmd->led_out.pin = led_pin;
  cmd->in_prog = 0;
  cmd->start_req = 0;

  (*(cmd->led_out.port - 1)) |= cmd->led_out.pin;

//  (*(cmd->relay_out.port - 1)) |= cmd->relay_out.pin;
}

#define CMD_DURATION                4000
void cmd_process(cmd_t *cmd)
{
  if (cmd->in_prog)
  {
    if (CHECK_TIMER(cmd->timestamp, CMD_DURATION))
    {
      *cmd->led_out.port &= ~cmd->led_out.pin;
      cmd->in_prog = 0;
      cmd->start_req = 0;
    }
  }
  else
  {
    if (cmd->start_req)
    {
      cmd->start_req = 0;
      cmd->in_prog = 1;
      *cmd->led_out.port |= cmd->led_out.pin;
      START_TIMER(cmd->timestamp);
    }
  }
}

void cmd_start(uint8_t cmd_idx)
{
  if ((cmd_idx > CMD_AMOUNT) || (cmd_idx == 0))
  {
    return;
  }

  cmd[cmd_idx - 1].start_req = 1;
}

//ISR(SIG_OVERFLOW1)
//{
//}

ISR(SIG_INTERRUPT0)
{
  rocket.rising_edge_stamp = TCNT1;
}

#define US_TO_CAPTURE_TICKS(us)       us//(((uint16_t )us) * (F_CPU / 8 / 1000000))

#define ZERO_WIDTH_MIN        US_TO_CAPTURE_TICKS(990)
#define ZERO_WIDTH_MAX        US_TO_CAPTURE_TICKS(1210)
#define ONE_WIDTH_MIN         US_TO_CAPTURE_TICKS(1710)
#define ONE_WIDTH_MAX         US_TO_CAPTURE_TICKS(2090)

ISR(SIG_INTERRUPT1)
{
  uint16_t falling_edge_stamp = TCNT1;
  uint16_t pulse_width = falling_edge_stamp - rocket.rising_edge_stamp;
  if ((pulse_width >= ZERO_WIDTH_MIN) && (pulse_width <= ZERO_WIDTH_MAX))
  {
    rocket.btn = ROCKET_BTN__OFF;
  }
  else if ((pulse_width >= ONE_WIDTH_MIN) && (pulse_width <= ONE_WIDTH_MAX))
  {
    rocket.btn = ROCKET_BTN__ON;
  }
  else
  {
    rocket.btn = ROCKET_BTN__UNDEF;
  }
}

/** @} */
