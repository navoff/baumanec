echo off

:: ������ ��� �������������
SET options_prog=-P ft0 -c ft232_bb
SET avrdude_path=%PROGRAMFILES%\avrdude_5.03

:: ������ ��� �������
:set_project
SET project=music
SET options_mem=-U flash:w:"%project%\mcu_src\Release\%project%.hex":a 
GOTO start_prog

:start_prog
@echo "%avrdude_path%\avrdude.exe" -C "%avrdude_path%\avrdude.conf" -p m8 %options_prog% %options_mem% -u
"%avrdude_path%\avrdude.exe" -C "%avrdude_path%\avrdude.conf" -p m8 %options_prog% %options_mem% -u
goto print_prog_result

:print_prog_result
:: ���� � ���� ���������� ��������� ������ ��� ���� ������� �� ���� �������
if ERRORLEVEL 1 goto lError
@echo =====================================================
@echo =                   SUCCESSFULLY                    =
@echo =====================================================
goto lExit

:: ������������ ���� �������� ��� ������ �������
:lError
@echo =====================================================
@echo =                      ERROR                        =
@echo =====================================================
goto lExit

:lExit
PAUSE