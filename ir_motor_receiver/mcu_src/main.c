/**
 * @file main.c
 * @brief
 * @author ������� �.�.
 * @date 17.06.2014
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "errors_type.h"
#include "tick_timer.h"
#include "milestag.h"
#include "uart.h"
#include "main.h"

//================================= Definition ================================

#define MOTOR_LEFT_1_DDR        DDRD
#define MOTOR_LEFT_1_PORT       PORTD
#define MOTOR_LEFT_1_P          (1 << PD5)
#define MOTOR_LEFT_2_DDR        DDRD
#define MOTOR_LEFT_2_PORT       PORTD
#define MOTOR_LEFT_2_P          (1 << PD6)

#define MOTOR_RIGHT_1_DDR       DDRD
#define MOTOR_RIGHT_1_PORT      PORTD
#define MOTOR_RIGHT_1_P         (1 << PD7)
#define MOTOR_RIGHT_2_DDR       DDRB
#define MOTOR_RIGHT_2_PORT      PORTB
#define MOTOR_RIGHT_2_P         (1 << PB0)

#define BTN_PORT                PORTC
#define BTN_P                   (1 << PC5)
//#define BTN_DDR                 (*(&BTN_PORT - 1))
//#define BTN_PIN                 (*(&BTN_PORT - 2))
#define BTN_DDR                 DDRC
#define BTN_PIN                 PINC

#define PWM_MAX                 32

#define MOTOR_IDX_LEFT          0
#define MOTOR_IDX_RIGHT         1

#define SPEED_CH                0
#define ANGLE_CH                1

typedef struct{
  volatile uint8_t *port;
  uint8_t pin;
} out_t;

typedef struct{
  enum {
    PWM_ST__OFF,
    PWM_ST__ON,
  } state;
  uint8_t cnt;
  uint8_t cmp;
  uint8_t direction;
  uint16_t max;
} pwm_t;

//================================ Declaration ================================

inline void tx_buf_process(void);

void motor_init(void);
void motor_set_state(uint8_t channel, uint8_t data);

void pwm_init(pwm_t *pwm);
void pwm_process(pwm_t *pwm, out_t *out_pos, out_t *out_neg);

struct{
  uint16_t speed, angle;
  pwm_t pwm_left, pwm_right;
  out_t out_left_pos, out_left_neg, out_right_pos, out_right_neg;
  enum{
    TEST_ST__UP,
    TEST_ST__DOWN,
  } test_state;
} motor = {
    .test_state = TEST_ST__UP,
    .out_left_pos = {&MOTOR_LEFT_1_PORT, MOTOR_LEFT_1_P},
    .out_left_neg = {&MOTOR_LEFT_2_PORT, MOTOR_LEFT_2_P},
    .out_right_pos = {&MOTOR_RIGHT_1_PORT, MOTOR_RIGHT_1_P},
    .out_right_neg = {&MOTOR_RIGHT_2_PORT, MOTOR_RIGHT_2_P},
};

// ============================== Implementation ==============================

int main(void)
{
  BTN_DDR &= ~BTN_P;
  BTN_PORT |= BTN_P;
  uint8_t buf_pin = 0;

  LED_1_INIT();
  LED_2_INIT();
  LED_1_OFF();
  LED_2_OFF();

  tick_init();
  milestag_init();
  uart_init(BAUDRATE__1000000);
  motor_init();

  pwm_init(&motor.pwm_left);
  pwm_init(&motor.pwm_right);

  LED_1_ON();
  _delay_ms(1000);
  LED_1_OFF();

  uint8_t timer_10ms = 0;

  uint16_t milestag_timestamp;
  START_TIMER(milestag_timestamp);

  uint16_t test_timestamp;
  START_TIMER(test_timestamp);
  uint16_t test_speed = 0;
  sei();

  while(1)
  {
    uint8_t speed_process = 0;
    if (BTN_PIN & BTN_P)
    {
      buf_pin = 1;
    }

    if (((BTN_PIN & BTN_P) == 0) && (buf_pin))
    {
      buf_pin = 0;
      speed_process = 1;
    }

//    if (speed_process)
    if (CHECK_TIMER(test_timestamp, 1000))
    {
      START_TIMER(test_timestamp);
      switch(motor.test_state)
      {
        case TEST_ST__DOWN:
          if (test_speed < PWM_MAX)
          {
            test_speed = 0;
            motor.test_state = TEST_ST__UP;
          }
          else
          {
            test_speed -= PWM_MAX;
          }
        break;
        case TEST_ST__UP:
          if (test_speed >= (256 - PWM_MAX))
          {
            test_speed = 255;
            motor.test_state = TEST_ST__DOWN;
          }
          else
          {
            test_speed += PWM_MAX;
          }
        break;
      }
//      uart_tx_byte(test_speed);
//      motor_set_state(SPEED_CH, test_speed);
    }

    tx_buf_process();

    if (is_tick())
    {
      timer_10ms++;
      pwm_process(&motor.pwm_left, &motor.out_left_pos, &motor.out_left_neg);
      pwm_process(&motor.pwm_right, &motor.out_right_pos, &motor.out_right_neg);
    }

    if (CHECK_TIMER(milestag_timestamp, 3))
    {
      START_TIMER(milestag_timestamp);
      milestag_rx_process();
      LED_1_SWITCH();
    }

    if (timer_10ms >= MS_TO_TICKS(10))
    {
      timer_10ms = 0;

      milestag_packet_t packet;
      uint8_t res = milestag_fifo_rx_packet_get(&packet);

      if (res == ERR_OK)
      {
        if (packet.id == MILESTAG_ID__CH_UP_DOWN)
        {
          motor_set_state(SPEED_CH, packet.data);
        }

        if (packet.id == MILESTAG_ID__CH_LEFT_RIGHT)
        {
          motor_set_state(ANGLE_CH, packet.data);
          uart_tx_byte(packet.data);
        }
      }
    }
  }
}

void motor_init(void)
{
  MOTOR_LEFT_1_DDR |= MOTOR_LEFT_1_P;
  MOTOR_LEFT_2_DDR |= MOTOR_LEFT_2_P;
  MOTOR_RIGHT_1_DDR |= MOTOR_RIGHT_1_P;
  MOTOR_RIGHT_2_DDR |= MOTOR_RIGHT_2_P;

  MOTOR_LEFT_1_PORT &= ~(MOTOR_LEFT_1_P);
  MOTOR_LEFT_2_PORT &= ~(MOTOR_LEFT_2_P);
  MOTOR_RIGHT_1_PORT &= ~(MOTOR_RIGHT_1_P);
  MOTOR_RIGHT_2_PORT &= ~(MOTOR_RIGHT_2_P);

  motor.speed = 0xFFFF;
  motor.angle = 0xFFFF;
}

inline void tx_buf_process(void)
{
  if (fifo_uart_tx.idx)
  {
    if (UCSRA & (1 << UDRE))
    {
      UDR = fifo_uart_tx.buf[fifo_uart_tx.tail++];
      if (fifo_uart_tx.tail >= FIFO_UART_TX_BUF_SIZE)
      {
        fifo_uart_tx.tail = 0;
      }
      fifo_uart_tx.idx--;
    }
  }
}

void motor_set_state(uint8_t channel, uint8_t data)
{
  uint8_t need_process = 0;
  if (channel == SPEED_CH)
  {
    if (motor.speed != data)
    {
      motor.speed = data;
      need_process = 1;
    }
  }
  if (channel == ANGLE_CH)
  {
    if (motor.angle != data)
    {
      motor.angle = data;
      need_process = 1;
    }
  }

  if (need_process)
  {
    uint8_t abs_speed;
    if (motor.speed >= 128)
    {
      motor.pwm_left.direction = 1;
      motor.pwm_right.direction = 1;
      abs_speed = motor.speed - 128;
    }
    else
    {
      motor.pwm_left.direction = 0;
      motor.pwm_right.direction = 0;
      abs_speed = 128 - motor.speed;
    }

    if (abs_speed < 32)
    {
      abs_speed = 0;
    }

    motor.pwm_left.cmp = (((uint16_t)abs_speed) * PWM_MAX + 64) / 128;
    motor.pwm_right.cmp = (((uint16_t)abs_speed) * PWM_MAX + 64) / 128;
  }
}

void pwm_init(pwm_t *pwm)
{
  pwm->cmp = 0;
  pwm->state = PWM_ST__OFF;
}

void pwm_process(pwm_t *pwm, out_t *out_pos, out_t *out_neg)
{
  switch(pwm->state)
  {
    case PWM_ST__OFF:
      if (pwm->cmp != 0)
      {
        if (pwm->direction)
        {
          *out_pos->port |= out_pos->pin;
          *out_neg->port &= ~out_neg->pin;
        }
        else
        {
          *out_pos->port &= ~out_pos->pin;
          *out_neg->port |= out_neg->pin;
        }
        pwm->cnt = 0;
        pwm->max = PWM_MAX;
        pwm->state = PWM_ST__ON;
      }
    break;
    case PWM_ST__ON:
      if (pwm->cmp == 0)
      {
        *out_pos->port &= ~out_pos->pin;
        *out_neg->port &= ~out_neg->pin;
        pwm->state = PWM_ST__OFF;
        break;
      }

      pwm->cnt++;
      if (pwm->cnt >= pwm->max)
      {
        pwm->cnt = 0;
        if (pwm->direction)
        {
          *out_pos->port |= out_pos->pin;
          *out_neg->port &= ~out_neg->pin;
        }
        else
        {
          *out_pos->port &= ~out_pos->pin;
          *out_neg->port |= out_neg->pin;
        }
      }
      else if (pwm->cnt == pwm->cmp)
      {
        *out_pos->port &= ~out_pos->pin;
        *out_neg->port &= ~out_neg->pin;
      }
    break;
  }
}
/** @} */
