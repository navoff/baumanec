/**
 * @file main.h
 * @brief
 * @author ������� �.�.
 * @date 17.06.2014
 * @addtogroup
 * @{
*/

#ifndef MAIN_H_
#define MAIN_H_

//================================= Definition ================================

#define LED_1_INIT()            DDRB |= (1 << PB1)
#define LED_1_ON()              PORTB |= (1 << PB1)
#define LED_1_OFF()             PORTB &= ~(1 << PB1)
#define LED_1_SWITCH()          PORTB ^= (1 << PB1)

#define LED_2_INIT()            DDRB |= (1 << PB2)
#define LED_2_ON()              PORTB |= (1 << PB2)
#define LED_2_OFF()             PORTB &= ~(1 << PB2)
#define LED_2_SWITCH()          PORTB ^= (1 << PB2)

//================================ Declaration ================================


#endif /* MAIN_H_ */

/** @} */
