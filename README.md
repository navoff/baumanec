Необходимое ПО:
1. Eclipse
2. Плагин AVR для Eclipse
  ссылка: https://sourceforge.net/projects/avr-eclipse/
  локально: _tools\avreclipse.p2repository-2.4.2.zip
3. WinAVR
  ссылка: https://sourceforge.net/projects/winavr/
  локально: tools\WinAVR-20100110-install.exe
4. Схемы сделаны в P-CAD 2006