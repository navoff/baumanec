/**
 * @file main.c
 * @brief
 * @author ������� �.�.
 * @date 14.06.2014
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>

#include "effects.h"
#include "tick_timer.h"

#include "main.h"

//================================= Definition ================================

#define LED_AMOUNT        16

typedef struct{
  volatile uint8_t *port;
  uint8_t pin;
} out_t;

typedef struct{
  uint8_t start_req;
  enum {
    PWM_ST__WAIT,
    PWM_ST__ON,
    PWM_ST__OFF,
  } state;
  uint16_t cnt;
  uint16_t cmp;
  uint16_t max;
  out_t out;
} pwm_t;

//================================ Declaration ================================

void led_init(void);
void led_set(uint32_t state);

void pwm_init(pwm_t *pwm);
void pwm_process(pwm_t *pwm, out_t *out);
void pwm_all_init(void);
void pwm_all_process(void);

out_t out[LED_AMOUNT] = {
    {&PORTD, (1 << PD0)},
    {&PORTD, (1 << PD1)},
    {&PORTD, (1 << PD2)},
    {&PORTD, (1 << PD3)},
    {&PORTD, (1 << PD4)},
    {&PORTB, (1 << PB6)},
    {&PORTB, (1 << PB7)},
    {&PORTD, (1 << PD5)},
    {&PORTD, (1 << PD6)},
    {&PORTD, (1 << PD7)},
    {&PORTB, (1 << PB0)},
    {&PORTB, (1 << PB1)},
    {&PORTB, (1 << PB2)},
    {&PORTC, (1 << PC3)},
    {&PORTC, (1 << PC4)},
    {&PORTC, (1 << PC5)},
};

pwm_t pwm[LED_AMOUNT];

// ============================== Implementation ==============================

int main(void)
{
  led_init();
  tick_init();
  pwm_all_init();

  effects.idx = 0;
  unit_t *unit = effects.unit[effects.idx];
  uint8_t led_pos = 0;
  uint8_t effect_cnt = 0;
  led_set(unit->tack[led_pos]);

  uint16_t effect_timestamp = 0;
  START_TIMER(effect_timestamp);

  while(1)
  {
    pwm_all_process();

    if (is_tick())
    {
    }

    if (CHECK_TIMER(effect_timestamp, 200))
    {
      START_TIMER(effect_timestamp);

      led_pos++;
      if (led_pos >= unit->len)
      {
        led_pos = 0;
        effect_cnt++;
        if (effect_cnt >= 3)
        {
          effect_cnt = 0;
          effects.idx++;
          if (effects.idx >= effects.amount)
          {
            effects.idx = 0;
          }
          unit = effects.unit[effects.idx];
        }
      }
      led_set(unit->tack[led_pos]);
    }
  }
}

void pwm_init(pwm_t *pwm)
{
  pwm->state = PWM_ST__WAIT;
}

#define PWM_SLOW_INC          1
#define PWM_FAST_INC          1
#define PWM_DEC               1
#define PWM_SLOW_FAST_CMP     40
#define PWM_MAX               80

void pwm_process(pwm_t *pwm, out_t *out)
{
  if (pwm->start_req)
  {
    pwm->start_req = 0;
    pwm->cnt = 0;
    pwm->cmp = PWM_SLOW_INC;
    pwm->max = PWM_MAX;
    pwm->state = PWM_ST__ON;

    *out->port |= out->pin;
  }

  switch(pwm->state)
  {
    case PWM_ST__WAIT:
    break;
    case PWM_ST__ON:
      pwm->cnt++;
      if (pwm->cnt == pwm->cmp)
      {
        *out->port &= ~out->pin;
      }
      if (pwm->cnt >= pwm->max)
      {
        pwm->cnt = 0;
        *out->port |= out->pin;
        if (pwm->cmp < PWM_SLOW_FAST_CMP)
        {
          pwm->cmp += PWM_SLOW_INC;
        }
        else
        {
          pwm->cmp += PWM_FAST_INC;
        }
        if (pwm->cmp >= pwm->max)
        {
          pwm->cmp = pwm->max;
          pwm->state = PWM_ST__OFF;
        }
      }
    break;
    case PWM_ST__OFF:
      pwm->cnt++;
      if (pwm->cnt == pwm->cmp)
      {
        *out->port &= ~out->pin;
      }
      if (pwm->cnt >= pwm->max)
      {
        pwm->cnt = 0;
        *out->port |= out->pin;
        pwm->cmp -= PWM_DEC;
        if (pwm->cmp <= PWM_DEC)
        {
          *out->port &= ~out->pin;
          pwm->state = PWM_ST__WAIT;
        }
      }
    break;
  }
}

void pwm_all_init(void)
{
  for (uint8_t i = 0; i < LED_AMOUNT; i++)
  {
    pwm_init(&pwm[i]);
  }
}

void pwm_all_process(void)
{
  for (uint8_t i = 0; i < LED_AMOUNT; i++)
  {
    pwm_process(&pwm[i], &out[i]);
  }
}

void led_init(void)
{
  for (uint8_t i = 0; i < LED_AMOUNT; i++)
  {
    (*(out[i].port - 1)) |= out[i].pin;
  }
}

void led_set(uint32_t state)
{
  for (uint8_t i = 0; i < LED_AMOUNT; i++)
  {
    if (state & 0x01)
    {
      pwm[i].start_req = 1;
    }

    state >>= 1;
  }
}

/** @} */
