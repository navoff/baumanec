/**
 * @file uart.c
 * @brief
 * @author ������� �.�.
 * @date 22.05.2012
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/interrupt.h>

#include "errors_type.h"
#include "uart.h"

//================================= Definition ================================

//================================ Declaration ================================

volatile fifo_uart_rx_t fifo_uart_rx;
volatile fifo_uart_tx_t fifo_uart_tx;

// ============================== Implementation ==============================

void uart_init(uint8_t baudrate)
{
  fifo_uart_tx.head = fifo_uart_tx.idx = fifo_uart_tx.tail = 0;
  fifo_uart_rx.idx = 0;
  fifo_uart_rx.wait_timer = 0;
  fifo_uart_rx.bof = 0;

  // tx
  DDRD |= (1 << PD1);
  PORTD |= (1 << PD1);

  // rx
  DDRD &= ~(1 << PD0);
  PORTD |= (1 << PD0);

  uart_set_baudrate(baudrate);

  UCSRB = (1 << RXCIE) | (1 << RXEN) | (1 << TXEN);
  UCSRC = (1 << URSEL) | (1 << UCSZ1) | (1 << UCSZ0);

  UCSRB |=(1 << RXEN) | (1 << TXEN);
}

/**
 * @brief ���������� �������� UART-a.
 * @param baudrate - ����� �� ��������� ��������:
 *  @arg BAUDRATE__9600
 *  @arg BAUDRATE__1000000
 */
void uart_set_baudrate(uint8_t baudrate)
{
  switch(baudrate)
  {
    case BAUDRATE__9600:
      UBRRH = 0;
      UBRRL = 103;
    break;

    case BAUDRATE__1000000:
    default:
      UBRRH = 0;
      UBRRL = 0;
    break;
  }
  UCSRA = (1 << U2X);
}

void fifo_uart_rx_get_packet(packet_t *packet)
{
  uint8_t *pointer = (uint8_t *)packet;
  for (uint8_t i = 0; i < sizeof(packet_t); i++)
  {
    pointer[i] = fifo_uart_rx.buf[fifo_uart_rx.tail++];
    if (fifo_uart_rx.tail >= FIFO_UART_RX_BUF_SIZE)
    {
      fifo_uart_rx.tail = 0;
    }
  }
  cli();
  fifo_uart_rx.idx -= sizeof(packet_t);
  sei();
  fifo_uart_rx.wait_timer = 0;
}

uint8_t uart_tx_buf(uint8_t buf[], uint8_t size)
{
  if (fifo_uart_tx.idx >= FIFO_UART_TX_BUF_SIZE - 1)
  {
    return ERR_BOF;
  }

  for (uint8_t i = 0; i < size; i++)
  {
    fifo_uart_tx.buf[fifo_uart_tx.head++] = buf[i];
    if (fifo_uart_tx.head >= FIFO_UART_TX_BUF_SIZE)
    {
      fifo_uart_tx.head = 0;
    }
    fifo_uart_tx.idx++;
  }

  return ERR_OK;
}

ISR(SIG_UART_RECV)
{
  uint8_t data = UDR;

  fifo_uart_rx.wait_timer = 0;

  if (fifo_uart_rx.idx >= (FIFO_UART_RX_BUF_SIZE - 1))
  {
    fifo_uart_rx.bof = 1;
    return;
  }

  fifo_uart_rx.buf[fifo_uart_rx.head++] = data;
  if (fifo_uart_rx.head >= FIFO_UART_RX_BUF_SIZE)
  {
    fifo_uart_rx.head = 0;
  }
  fifo_uart_rx.idx++;
}

/** @} */
