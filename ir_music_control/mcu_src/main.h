/**
 * @file main.h
 * @brief
 * @author ������� �.�.
 * @date 23.05.2012
 * @addtogroup
 * @{
*/

#ifndef MAIN_H_
#define MAIN_H_

//================================= Definition ================================

//#define LED_INIT()                            DDRB |= (1 << PB0)
//#define LED_ON()                              PORTB |= (1 << PB0)
//#define LED_OFF()                             PORTB &= ~(1 << PB0)
//#define LED_SWITCH()                          PORTB ^= (1 << PB0)
#define LED_INIT()                            DDRD |= (1 << PD7)
#define LED_ON()                              PORTD |= (1 << PD7)
#define LED_OFF()                             PORTD &= ~(1 << PD7)
#define LED_SWITCH()                          PORTD ^= (1 << PD7)

#define CMD__FLASH_STATUS                     0x01
#define CMD__FLASH_ERASE                      0x02
#define CMD__FLASH_TEST_ERASE_READ            0x03
#define CMD__FLASH_TEST_WRITE_READ            0x04

#define CMD__GET_CHECK_SUM                    0x0F
#define CMD__START_PRG_FLASH                  0x10
#define CMD__PRG_BUF                          0x11
#define CMD__STOP_PRG_FLASH                   0x12
#define CMD__START_SPEECH                     0x13
#define CMD__STOP_SPEECH                      0x14

#define CMD__ERROR                0xFF

//================================ Declaration ================================

void cmd_send_status(uint8_t code, uint8_t status);
void cmd_send_buf(uint8_t code, uint8_t buf[]);

#endif /* MAIN_H_ */

/** @} */
