/**
 * @file flash.c
 * @brief
 * @author ������� �.�.
 * @date 23.05.2012
 * @addtogroup
 * @{
*/

#include <stdio.h>
#include <string.h>
#include <avr\io.h>
#include <util\delay.h>

#include "main.h"
#include "flash.h"
#include "errors_type.h"

//================================= Definition ================================

#define FLASH_CMD__RDSR                     0xD7
#define FLASH_CMD__BLOCK_ERASE              0x50
#define FLASH_CMD__SECTOR_ERASE             0x7C
#define FLASH_CMD__READ                     0x03
#define FLASH_CMD__WRITE_BUF                0x84
#define FLASH_CMD__PRG_BUF_WITHOUT_ERASE    0x88


#define FLASH_CMD__EWSR             0x50
#define FLASH_CMD__WRSR             0x01

#define FLASH_CMD__WREN             0x06
#define FLASH_CMD__WRDI             0x04

#define FLASH_CMD__AAI              0xAD


#define FLASH_SR__PAGE_SIZE_512     0x01
#define FLASH_SR__BUSY              0x80


#define FLASH_SR__BP0             2
#define FLASH_SR__BP1             3
#define FLASH_SR__BP2             4
#define FLASH_SR__BP3             5
#define FLASH_SR__BPL             7

#define FLASH_PORT      PORTC
#define FLASH_CE        PC1 // todo �������
#define FLASH_CS        PC1
#define FLASH_RST       PC2
#define FLASH_SCK       PC3
#define FLASH_SI        PC4
#define FLASH_SO        PC5

#define FLASH_DDR       (*(&FLASH_PORT - 1))
#define FLASH_PIN       (*(&FLASH_PORT - 2))

//================================ Declaration ================================

void    SPI_write_byte(uint8_t byte);
uint8_t SPI_read_byte(void);
uint8_t flash_read_status(void);
void    flash_set_512_page_size(void);

// ============================== Implementation ==============================

void flash_init(void)
{
  FLASH_PORT |= (1 << FLASH_RST);
  FLASH_DDR |= (1 << FLASH_RST);

  FLASH_PORT |= (1 << FLASH_CS);
  FLASH_DDR |= (1 << FLASH_CS);

  FLASH_PORT &= ~(1 << FLASH_SCK);
  FLASH_DDR |= (1 << FLASH_SCK);

  FLASH_PORT &= ~(1 << FLASH_SI);
  FLASH_DDR |= (1 << FLASH_SI);

  FLASH_PORT |= (1 << FLASH_SO);
  FLASH_DDR &= ~(1 << FLASH_SO);

  if ((flash_read_status() & FLASH_SR__PAGE_SIZE_512) == 0)
  {
    flash_set_512_page_size();
    while(1)
    {
      LED_SWITCH();
      _delay_ms(200);
    }
  }
}

void flash_set_512_page_size(void)
{
  FLASH_PORT &= ~(1 << FLASH_CS);

  SPI_write_byte(0x3D);
  SPI_write_byte(0x2A);
  SPI_write_byte(0x80);
  SPI_write_byte(0xA6);

  FLASH_PORT |= (1 << FLASH_CS);
}

uint8_t flash_test_erase_read(uint8_t result_params[])
{
  flash_erase();

  uint8_t buf[4];
  for (uint32_t address = 0; address < (((uint32_t)FLASH_PAGE_AMOUNT) * FLASH_PAGE_SIZE); address += 4)
  {
    LED_SWITCH();
    flash_read_buf(address, buf, 4);
    for (uint8_t i = 0; i < 4; i++)
    {
      if (buf[i] != 0xFF)
      {
        memcpy(&result_params[0], &address, 4);
        memcpy(&result_params[4], buf, 4);
        return ERR_RESULT;
      }
    }
  }

  return ERR_OK;
}

uint8_t flash_test_write_read(uint8_t result_params[])
{
  // ��������� �������� ������
  flash_erase();

  uint32_t address = 0;
  for (uint16_t i = 0; i < FLASH_PAGE_AMOUNT; i++)
  {
    flash_continuous_write_to_buf_start();
    for (uint16_t k = 0; k < FLASH_PAGE_SIZE; k += 4)
    {
      flash_continuous_write_to_buf_byte((address + k));
      flash_continuous_write_to_buf_byte((address + k) >> 8);
      flash_continuous_write_to_buf_byte((address + k) >> 16);
      flash_continuous_write_to_buf_byte((address + k) >> 24);
    }
    flash_continuous_write_stop(address);
    address += FLASH_PAGE_SIZE;
    while (flash_is_not_busy());
    if ((i & 0xFF) == 0)
    {
      UDR = i >> 8;
    }
  }

  address = 0;
  flash_continuous_read_start(0);
  for (uint16_t i = 0; i < FLASH_PAGE_AMOUNT; i++)
  {
    for (uint16_t k = 0; k < FLASH_PAGE_SIZE; k += 4, address += 4)
    {
      uint32_t read;
      flash_continuous_read_buf((uint8_t *)&read, 4);
      if (read != address)
      {
        flash_continuous_read_stop();
        memcpy(&result_params[0], &address, 4);
        memcpy(&result_params[4], &read, 4);
        return ERR_RESULT;
      }
    }
    if ((i & 0xFF) == 0)
    {
      UDR = i >> 8;
    }
  }
  flash_continuous_read_stop();

  return ERR_OK;
}

uint8_t flash_get_check_sum(void)
{
  uint8_t check_sum = 0;

  flash_continuous_read_start(0);
  for (uint16_t i = 0; i < FLASH_PAGE_AMOUNT; i++)
  {
    for (uint16_t k = 0; k < FLASH_PAGE_SIZE; k += 8)
    {
      uint8_t read[8];
      flash_continuous_read_buf(read, 8);
      for (uint8_t m = 0; m < 8; m++)
      {
        check_sum ^= read[m];
      }
    }
    if ((i & 0xFF) == 0)
    {
      UDR = i >> 8;
    }
  }
  flash_continuous_read_stop();

  return check_sum;
}

void SPI_write_byte(uint8_t byte)
{
  uint8_t msk = 0x80;
  while (msk)
  {
    if (byte & msk)
    {
      FLASH_PORT |= (1 << FLASH_SI);
    }
    else
    {
      FLASH_PORT &= ~(1 << FLASH_SI);
    }
    msk >>= 1;
    FLASH_PORT |= (1 << FLASH_SCK);
    FLASH_PORT &= ~(1 << FLASH_SCK);
  }
}

uint8_t SPI_read_byte(void)
{
  uint8_t data = 0;

  for (uint8_t i = 0; i < 8; i++)
  {
    data <<= 1;

    FLASH_PORT |= (1 << FLASH_SCK);
    if (FLASH_PIN & (1 << FLASH_SO))
    {
      data |= 1;
    }
    FLASH_PORT &= ~(1 << FLASH_SCK);
  }

  return data;
}

uint8_t flash_read_status(void)
{
  uint8_t res;

  FLASH_PORT &= ~(1 << FLASH_CS);

  SPI_write_byte(FLASH_CMD__RDSR);
  res = SPI_read_byte();

  FLASH_PORT |= (1 << FLASH_CS);

  return res;
}

uint8_t flash_is_not_busy(void)
{
  uint8_t res;

  FLASH_PORT &= ~(1 << FLASH_CS);

  SPI_write_byte(FLASH_CMD__RDSR);

  FLASH_PORT |= (1 << FLASH_SCK);
  if (FLASH_PIN & (1 << FLASH_SO))
  {
    res = ERR_OK;
  }
  else
  {
    res = ERR_RESULT;
  }
  FLASH_PORT &= ~(1 << FLASH_SCK);
  FLASH_PORT |= (1 << FLASH_CS);

  return res;
}

void flash_erase_sector_req(uint8_t sector_idx)
{
  if (sector_idx >= FLASH_SECTOR_AMOUNT)
    return;

  uint32_t address;

  if ((sector_idx == 0) || (sector_idx == 1))
  {
    address = sector_idx * FLASH_FIRST_SECTOR_SIZE;
  }
  else
  {
    address = (sector_idx - 1) * FLASH_SECTOR_SIZE;
  }

  FLASH_PORT &= ~(1 << FLASH_CS);
  SPI_write_byte(FLASH_CMD__SECTOR_ERASE);
  SPI_write_byte(address >> 16);
  SPI_write_byte(address >> 8);
  SPI_write_byte(address);
  FLASH_PORT |= (1 << FLASH_CS);
}

void flash_erase(void)
{
  for (uint8_t i = 0; i < FLASH_SECTOR_AMOUNT; i++)
  {
    flash_erase_sector_req(i);
    while (flash_is_not_busy());
  }
}

void flash_read_buf(uint32_t address, uint8_t buf[], uint8_t size)
{
  FLASH_PORT &= ~(1 << FLASH_CS);
  SPI_write_byte(FLASH_CMD__READ);
  SPI_write_byte(address >> 16);
  SPI_write_byte(address >> 8);
  SPI_write_byte(address);
  for (uint8_t i = 0; i < size; i++)
  {
    buf[i] = SPI_read_byte();
  }
  FLASH_PORT |= (1 << FLASH_CS);
}

void flash_continuous_read_start(uint32_t address)
{
  FLASH_PORT &= ~(1 << FLASH_CS);
  SPI_write_byte(FLASH_CMD__READ);
  SPI_write_byte(address >> 16);
  SPI_write_byte(address >> 8);
  SPI_write_byte(address);
}

void flash_continuous_read_buf(uint8_t buf[], uint8_t size)
{
  for (uint8_t i = 0; i < size; i++)
  {
    buf[i] = SPI_read_byte();
  }
}

void flash_continuous_read_stop(void)
{
  FLASH_PORT |= (1 << FLASH_CS);
}


void flash_continuous_write_to_buf_start(void)
{
  FLASH_PORT &= ~(1 << FLASH_CS);
  SPI_write_byte(FLASH_CMD__WRITE_BUF);
  SPI_write_byte(0);
  SPI_write_byte(0);
  SPI_write_byte(0);
}

void flash_continuous_write_to_buf_byte(uint8_t byte)
{
  SPI_write_byte(byte);
}

void flash_continuous_write_stop(uint32_t address)
{
  FLASH_PORT |= (1 << FLASH_CS);
  FLASH_PORT &= ~(1 << FLASH_CS);

  address &= ~(FLASH_PAGE_SIZE - 1);

  SPI_write_byte(FLASH_CMD__PRG_BUF_WITHOUT_ERASE);
  SPI_write_byte(address >> 16);
  SPI_write_byte(address >> 8);
  SPI_write_byte(0);

  FLASH_PORT |= (1 << FLASH_CS);
}

void flash_release(void)
{
  FLASH_PORT |= (1 << FLASH_CS);
}




void flash_write_AAI_first_data(uint16_t data)
{
  FLASH_PORT &= ~(1 << FLASH_CE);
  SPI_write_byte(FLASH_CMD__AAI);
  SPI_write_byte(0);
  SPI_write_byte(0);
  SPI_write_byte(0);
  SPI_write_byte(data);
  SPI_write_byte(data >> 8);
  FLASH_PORT |= (1 << FLASH_CE);
}

void flash_write_AAI_data(uint16_t data)
{
  FLASH_PORT &= ~(1 << FLASH_CE);
  SPI_write_byte(FLASH_CMD__AAI);
  SPI_write_byte(data);
  SPI_write_byte(data >> 8);
  FLASH_PORT |= (1 << FLASH_CE);
}

uint8_t flash_AAI_not_busy(void)
{
  if (FLASH_PIN & (1 << FLASH_SO))
  {
    return ERR_OK;
  }
  else
  {
    return ERR_BUSY;
  }
}

/** @} */
