/**
 * @file melodies.h
 * @brief
 * @author ������� �.�.
 * @date 03.06.2012
 * @addtogroup
 * @{
*/

#ifndef MELODIES_H_
#define MELODIES_H_

//================================= Definition ================================

typedef struct{
  uint16_t *intro;
  uint16_t *main;
}melody_t;

typedef struct{
  uint8_t amount;
  melody_t melody[];
}melodies_t;

//================================ Declaration ================================

extern melodies_t melodies;

#endif /* MELODIES_H_ */

/** @} */
