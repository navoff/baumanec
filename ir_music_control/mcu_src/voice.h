/**
 * @file voice.h
 * @brief
 * @author ������� �.�.
 * @date 22.05.2012
 * @addtogroup
 * @{
*/

#ifndef VOICE_H_
#define VOICE_H_

//================================= Definition ================================

#define VOICE_REQ_START_PRG         0x01
#define VOICE_REQ_STOP_PRG          0x02
#define VOICE_REQ_START_SPEECH      0x04
#define VOICE_REQ_STOP_SPEECH       0x08

//================================ Declaration ================================

void    voice_init(void);
void    voice_restart(void);
void    voice_process(void);
uint8_t voice_speech_is_over(void);
uint8_t voice_speech_phrase(uint16_t phrase[]);
void    voice_speech_break(void);
void    voice_req(uint8_t req_msk);


uint8_t fifo_voice_prg_put_buf(uint8_t buf[], uint8_t size);

#endif /* VOICE_H_ */

/** @} */
