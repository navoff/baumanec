/**
 * @file ir_detect.c
 * @brief
 * @author ������� �.�.
 * @date 15.06.2016
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/io.h>

#include "tick_timer.h"
#include "ir_detect.h"
#include "main.h"

//================================= Definition ================================

#define IR_IS_ACTIVE()            bit_is_clear(PIND, PD3)
#define IR_DETECT_DURATION_SIZE   10

typedef struct{
  uint16_t duration[IR_DETECT_DURATION_SIZE];
  uint16_t timestamp;
  uint8_t is_active;
  int8_t idx;
} ir_detect_t;

//================================ Declaration ================================

ir_detect_t ir_detect;
uint8_t ir_btn_hi_press;
uint8_t ir_btn_low_press;

// ============================== Implementation ==============================

void ir_detect_init(void)
{
  START_TIMER(ir_detect.timestamp);
  ir_detect.is_active = 0;
  ir_detect.idx = -1;

  ir_btn_low_press = 0;
  ir_btn_hi_press = 0;
}

void ir_detect_process(void)
{
  if (((IR_IS_ACTIVE()) && (ir_detect.is_active == 0)) ||
      ((IR_IS_ACTIVE() == 0) && (ir_detect.is_active)))
  {
    ir_detect.is_active = !ir_detect.is_active;

    if ((ir_detect.idx != -1) && (ir_detect.idx < IR_DETECT_DURATION_SIZE))
    {
      ir_detect.duration[ir_detect.idx] = DELTA_TIMER(ir_detect.timestamp);
    }
    ir_detect.idx++;
    START_TIMER(ir_detect.timestamp);
  }

  if (CHECK_TIMER(ir_detect.timestamp, 100))
  {
    if (ir_detect.idx > 0)
    {
      uint8_t amount = 0;
      uint16_t mean = 0;
      for (uint8_t i = 0; i < ir_detect.idx; i++)
      {
        if ((ir_detect.duration[i] >= 8) && (ir_detect.duration[i] <= 12))
        {
          amount++;
          mean += ir_detect.duration[i];
        }

        if ((ir_detect.duration[i] >= 18) && (ir_detect.duration[i] <= 22))
        {
          amount++;
          mean += ir_detect.duration[i];
        }
      }
      ir_detect.idx = -1;

      if (amount >= 3)
      {
        mean = (mean + (amount >> 1)) / amount;
        if ((mean >= 8) && (mean <= 12))
        {
          ir_btn_hi_press = 1;
        }

        if ((mean >= 18) && (mean <= 22))
        {
          ir_btn_low_press = 1;
        }
      }
    }
  }
}

/** @} */
