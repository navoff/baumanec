/**
 * @file flash.h
 * @brief
 * @author ������� �.�.
 * @date 23.05.2012
 * @addtogroup
 * @{
*/

#ifndef FLASH_H_
#define FLASH_H_

//================================= Definition ================================

#define FLASH_PAGE_SIZE                 512

#define FLASH_PAGE_AMOUNT               4096

#define FLASH_FIRST_SECTOR_SIZE         (((uint32_t)FLASH_PAGE_SIZE) * 8 * 1)
#define FLASH_SECTOR_SIZE               (((uint32_t)FLASH_PAGE_SIZE) * 8 * 32)

/// ���������� ��������.
/// @note ������� ������ ������� �� 2 �����: 0a � 0b.
#define FLASH_SECTOR_AMOUNT             17

//================================ Declaration ================================

void    flash_init(void);
uint8_t flash_test_write_read(uint8_t result_params[]);
uint8_t flash_test_erase_read(uint8_t result_params[]);
uint8_t flash_get_check_sum(void);

uint8_t flash_is_not_busy(void);
uint8_t flash_read_status(void);
void    flash_erase_sector_req(uint8_t sector_idx);
void    flash_erase(void);
void    flash_read_buf(uint32_t address, uint8_t buf[], uint8_t size);
void    flash_continuous_read_start(uint32_t address);
void    flash_continuous_read_buf(uint8_t buf[], uint8_t size);
void    flash_continuous_read_stop(void);

void    flash_continuous_write_to_buf_start(void);
void    flash_continuous_write_to_buf_byte(uint8_t byte);
void    flash_continuous_write_stop(uint32_t address);
void    flash_release(void);


uint8_t flash_AAI_not_busy(void);

void flash_write_AAI_first_data(uint16_t data);
void flash_write_AAI_data(uint16_t data);

#endif /* FLASH_H_ */

/** @} */
