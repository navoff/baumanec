/**
 * @file ir_detect.h
 * @brief
 * @author ������� �.�.
 * @date 15.06.2016
 * @addtogroup
 * @{
*/

#ifndef IR_DETECT_H_
#define IR_DETECT_H_

//================================= Definition ================================

//================================ Declaration ================================

void ir_detect_init(void);
void ir_detect_process(void);

extern uint8_t ir_btn_hi_press;
extern uint8_t ir_btn_low_press;

#endif /* IR_DETECT_H_ */

/** @} */
