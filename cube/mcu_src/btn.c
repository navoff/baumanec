/**
 * @file btn.c
 * @brief
 * @author ������� �.�.
 * @date 23 ���� 2017 �.
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/io.h>

#include "config.h"
#include "tick_timer.h"
#include "btn.h"

// ================================ Definition ================================

#if defined(VER_LUSY)
  #define PIN_BTN     PIND
  #define PORT_BTN    PORTD
  #define P_BTN       PD0
#else
  #define PIN_BTN     PIND
  #define PORT_BTN    PORTD
  #define P_BTN       PD3
#endif

#define BTN_LONG_PRESS_DURATION       1500

// =============================== Declaration ================================

uint16_t btn_timestamp;
uint16_t btn_press_timestamp;
uint8_t btn_pin;
uint8_t btn_was_long_pressed;
uint8_t btn_is_pressed;
uint8_t btn_is_long_pressed;

// ============================== Implementation ==============================


void btn_init(void)
{
  // ��� ��� ������
  PORT_BTN |= (1 << P_BTN);

  START_TIMER(btn_timestamp);
  btn_pin = 1;
  btn_is_pressed = 0;
}

void btn_process(void)
{
  if (CHECK_TIMER(btn_timestamp, 100))
  {
    START_TIMER(btn_timestamp);

    if (btn_pin)
    {
      // ������ ������
      if (bit_is_clear(PIN_BTN, P_BTN))
      {
        btn_pin = 0;

        START_TIMER(btn_press_timestamp);
      }
    }
    else
    {
      // ������ �� ��� ������
      if (bit_is_clear(PIN_BTN, P_BTN))
      {
        if ((btn_was_long_pressed == 0) &&
            (CHECK_TIMER(btn_press_timestamp, BTN_LONG_PRESS_DURATION)))
        {
          btn_is_long_pressed = 1;
          btn_was_long_pressed = 1;
        }
      }
      else
      {
        if (btn_was_long_pressed == 0)
        {
          btn_is_pressed = 1;
        }
        btn_was_long_pressed = 0;
        btn_pin = 1;
      }
    }
  }
}

/** @} */
