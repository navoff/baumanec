/**
 * @file leds.h
 * @brief
 * @author ������� �.�.
 * @date 23 ���� 2017 �.
 * @addtogroup
 * @{
*/

#ifndef LEDS_H_
#define LEDS_H_

//================================= Definition ================================

//================================ Declaration ================================

void pwm_all_init(void);
void pwm_all_process(void);

void led_init(void);
void led_set(uint32_t state);

void led_all_side_off(void);
void led_all_side_on(void);
void led_side_on(uint8_t idx);

extern uint16_t led_timestamp;
extern uint8_t led_pos;

#endif /* LEDS_H_ */

/** @} */
