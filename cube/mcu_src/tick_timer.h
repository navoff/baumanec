/**
 * @file tick_timer.h
 * @brief
 * @author ������� �.�.
 * @date 23.05.2012
 * @addtogroup
 * @{
*/

#ifndef TICK_TIMER_H_
#define TICK_TIMER_H_

//================================= Definition ================================

#define TICK_PERIOD_125_US

#define TICK_COUNTER_L()                TCNT0
#define MS_TO_TICKS(ms)                 (((uint16_t)ms) * 8)
#define US_TO_TICKS(us)                 ((us + 62) / 125)

#define CHECK_TIMER(stamp, period)      ((tick_counter.W - stamp) >= MS_TO_TICKS(period))
#define START_TIMER(stamp)              (stamp = tick_counter.W)

typedef union{
  struct{
    uint8_t L;
    uint8_t H;
  };
  uint16_t W;
} tick_counter_t;

//================================ Declaration ================================

void    tick_init(void);
uint8_t is_tick(void);

extern tick_counter_t tick_counter;

#endif /* TICK_TIMER_H_ */

/** @} */
