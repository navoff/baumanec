/**
 * @file rnd.c
 * @brief
 * @author ������� �.�.
 * @date 23 ���� 2017 �.
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/io.h>

#include "rnd.h"

//================================= Definition ================================

#define RND_BUF_SIZE          8

//================================ Declaration ================================

void rnd_put(uint8_t data);

struct
{
  struct
  {
    uint8_t buf[RND_BUF_SIZE];
    uint8_t idx;
    uint8_t tail;
    uint8_t head;
  } fifo;
  struct
  {
    uint8_t tick_idx;
    uint8_t active_idx[2];
    uint8_t state_idx;
    uint8_t idx;
    uint8_t data;
  } adc;
} rnd;

volatile uint16_t adc_data;
volatile uint8_t adc_data_lo;
volatile uint8_t adc_data_hi;

// ============================== Implementation ==============================

void rnd_init(void)
{
  ADMUX =
      (1 << REFS1) | (1 << REFS0) | // ���������� �������
      (0 << ADLAR) | // ������������ �� ������ �������
      (1 << MUX3) | (1 << MUX2) | (1 << MUX1) | (0 << MUX0); // ����� 0

  ADCSRA =
      (1 << ADEN) | (0 << ADSC) | (1 << ADFR) | (0 << ADIE) |
      (0 << ADPS2) | (0 << ADPS1) | (1 << ADPS0);

  ADCSRA |= (1 << ADSC);

  rnd.fifo.idx = 0;
  rnd.fifo.head = 0;
  rnd.fifo.tail = 0;

  rnd.adc.data = 0;
  rnd.adc.tick_idx = 0;
  rnd.adc.active_idx[0] = 0;
  rnd.adc.active_idx[1] = 0;
  rnd.adc.state_idx = 0;
  rnd.adc.idx = 0;
}

void rnd_process(void)
{
  if (ADCSRA & (1 << ADIF))
  {
    adc_data = ADCL;
    adc_data |= ADCH << 8;

    ADCSRA |= (1 << ADIF);

    if (adc_data & 0x01)
    {
      rnd.adc.active_idx[rnd.adc.state_idx]++;
    }
    rnd.adc.tick_idx++;

    if (rnd.adc.tick_idx >= 4)
    {
      rnd.adc.tick_idx = 0;

      rnd.adc.state_idx++;
      if (rnd.adc.state_idx >= 2)
      {
        rnd.adc.state_idx = 0;

        if (rnd.adc.active_idx[0] >= rnd.adc.active_idx[1])
        {
          rnd.adc.data++;
        }
        rnd.adc.active_idx[0] = 0;
        rnd.adc.active_idx[1] = 0;

        rnd.adc.idx++;

        if (rnd.adc.idx >= 30)
        {
          rnd.adc.idx = 0;

          rnd_put((rnd.adc.data % 6) + 1);

          rnd.adc.data = 0;
        }
      }
    }
  }
}

void rnd_put(uint8_t data)
{
  if (rnd.fifo.idx < RND_BUF_SIZE)
  {
    rnd.fifo.buf[rnd.fifo.head] = data;
    rnd.fifo.head++;
    if (rnd.fifo.head >= RND_BUF_SIZE)
    {
      rnd.fifo.head = 0;
    }
    rnd.fifo.idx++;
  }
}

uint8_t rnd_get(void)
{
  uint8_t res = 0;

  if (rnd.fifo.idx > 0)
  {
    res = rnd.fifo.buf[rnd.fifo.tail];

    rnd.fifo.tail++;
    if (rnd.fifo.tail >= RND_BUF_SIZE)
    {
      rnd.fifo.tail = 0;
    }
    rnd.fifo.idx--;
  }

  return res;
}

/** @} */
