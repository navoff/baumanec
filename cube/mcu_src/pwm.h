/**
 * @file pwm.h
 * @brief
 * @author ������� �.�.
 * @date 23 ���� 2017 �.
 * @addtogroup
 * @{
*/

#ifndef PWM_H_
#define PWM_H_

// ================================ Definition ================================

typedef struct
{
  volatile uint8_t *port;
  uint8_t pin;
} out_t;

typedef struct
{
  uint8_t start_req;
  enum {
    PWM_ST__WAIT,
    PWM_ST__ON,
    PWM_ST__OFF,
  } state;
  uint16_t cnt;
  uint16_t cmp;
  uint16_t max;
  out_t out;
} pwm_t;

//================================ Declaration ================================

void pwm_init(pwm_t *pwm);
void pwm_process(pwm_t *pwm, out_t *out);

#endif /* PWM_H_ */

/** @} */
