/**
 * @file btn.h
 * @brief
 * @author ������� �.�.
 * @date 23 ���� 2017 �.
 * @addtogroup
 * @{
*/

#ifndef BTN_H_
#define BTN_H_

//================================= Definition ================================

//================================ Declaration ================================

void btn_init(void);
void btn_process(void);

extern uint8_t btn_is_pressed;
extern uint8_t btn_is_long_pressed;

#endif /* BTN_H_ */

/** @} */
