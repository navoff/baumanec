/**
 * @file rnd.h
 * @brief
 * @author ������� �.�.
 * @date 23 ���� 2017 �.
 * @addtogroup
 * @{
*/

#ifndef RND_H_
#define RND_H_

//================================= Definition ================================

//================================ Declaration ================================

void rnd_init(void);
void rnd_process(void);
uint8_t rnd_get(void);

#endif /* RND_H_ */

/** @} */
