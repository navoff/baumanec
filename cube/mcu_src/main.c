/**
 * @file main.c
 * @brief
 * @author ������� �.�.
 * @date 21 ���� 2017 �.
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "config.h"
#include "effects.h"
#include "pwm.h"
#include "leds.h"
#include "btn.h"
#include "rnd.h"
#include "tick_timer.h"

//================================= Definition ================================

//================================ Declaration ================================

void game_init(void);
void game_process(void);

struct
{
  enum
  {
    GAME_ST__WAIT,
    GAME_ST__RND,
    GAME_ST__PAUSE,
  } state;
  int side_idx;
  uint16_t timestamp;
} game;

// ============================== Implementation ==============================

int main(void)
{
  led_init();
  tick_init();
  pwm_all_init();
  game_init();
  btn_init();
  rnd_init();

  led_set(effects.unit[0]->tack[0]);

  uint16_t rnd_timestamp = 0;
  START_TIMER(rnd_timestamp);

  uint16_t game_timestamp = 0;
  START_TIMER(game_timestamp);

  while(1)
  {
    if (is_tick())
    {
      if (game.state == GAME_ST__WAIT)
      {
        pwm_all_process();
      }
      btn_process();
    }

    if (CHECK_TIMER(rnd_timestamp, 1))
    {
      START_TIMER(rnd_timestamp);

      rnd_process();
    }

    if (CHECK_TIMER(game_timestamp, 100))
    {
      START_TIMER(game_timestamp);

      game_process();
    }
  }
}

void game_init(void)
{
  game.state = GAME_ST__WAIT;
}

void game_process(void)
{
  switch (game.state)
  {
    case GAME_ST__WAIT:
      if (CHECK_TIMER(led_timestamp, 500))
      {
        START_TIMER(led_timestamp);

        led_pos++;
        if (led_pos >= effects.unit[0]->len)
        {
          led_pos = 0;
        }
        led_set(effects.unit[0]->tack[led_pos]);
      }

      if (btn_is_long_pressed)
      {
        btn_is_long_pressed = 0;

        game.side_idx = 0;
        led_side_on(game.side_idx);
        game.state = GAME_ST__RND;
      }
    break;

    case GAME_ST__RND:
      if (btn_is_pressed)
      {
        btn_is_pressed = 0;

        led_all_side_off();
        START_TIMER(game.timestamp);
        game.state = GAME_ST__PAUSE;
      }

      if (btn_is_long_pressed)
      {
        btn_is_long_pressed = 0;

        led_pos = 0;
        led_set(effects.unit[0]->tack[0]);

        game.state = GAME_ST__WAIT;
      }
    break;

    case GAME_ST__PAUSE:
      if (CHECK_TIMER(game.timestamp, 300))
      {
        game.side_idx = rnd_get();
        led_side_on(game.side_idx);
        game.state = GAME_ST__RND;
      }
    break;
  }
}

/** @} */
