/**
 * @file pwm.c
 * @brief
 * @author ������� �.�.
 * @date 23 ���� 2017 �.
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include "config.h"
#include "pwm.h"

// ================================ Definition ================================

#if defined(VER_LUSY)
  #define PWM_SLOW_INC          1
  #define PWM_FAST_INC          1
  #define PWM_SLOW_DEC          1
  #define PWM_FAST_DEC          1
  #define PWM_SLOW_FAST_CMP     60
  #define PWM_MAX               100
#else
  #define PWM_SLOW_INC          1
  #define PWM_FAST_INC          2
  #define PWM_SLOW_DEC          1
  #define PWM_FAST_DEC          2
  #define PWM_SLOW_FAST_CMP     100
  #define PWM_MAX               160
#endif

// =============================== Declaration ================================

// ============================== Implementation ==============================

void pwm_init(pwm_t *pwm)
{
  pwm->state = PWM_ST__WAIT;
}

void pwm_process(pwm_t *pwm, out_t *out)
{
  if (pwm->start_req)
  {
    pwm->start_req = 0;
    pwm->cnt = 0;
    pwm->cmp = PWM_SLOW_INC;
    pwm->max = PWM_MAX;
    pwm->state = PWM_ST__ON;

    *out->port |= out->pin;
  }

  switch(pwm->state)
  {
    case PWM_ST__WAIT:
    break;
    case PWM_ST__ON:
      pwm->cnt++;
      if (pwm->cnt == pwm->cmp)
      {
        *out->port &= ~out->pin;
      }
      if (pwm->cnt >= pwm->max)
      {
        pwm->cnt = 0;
        *out->port |= out->pin;
        if (pwm->cmp < PWM_SLOW_FAST_CMP)
        {
          pwm->cmp += PWM_SLOW_INC;
        }
        else
        {
          pwm->cmp += PWM_FAST_INC;
        }
        if (pwm->cmp >= pwm->max)
        {
          pwm->cmp = pwm->max;
          pwm->state = PWM_ST__OFF;
        }
      }
    break;
    case PWM_ST__OFF:
      pwm->cnt++;
      if (pwm->cnt == pwm->cmp)
      {
        *out->port &= ~out->pin;
      }
      if (pwm->cnt >= pwm->max)
      {
        pwm->cnt = 0;
        *out->port |= out->pin;

        if (pwm->cmp < PWM_SLOW_FAST_CMP)
        {
          pwm->cmp -= PWM_SLOW_DEC;
        }
        else
        {
          pwm->cmp -= PWM_FAST_DEC;
        }

        if (pwm->cmp <= PWM_SLOW_DEC)
        {
          *out->port &= ~out->pin;
          pwm->state = PWM_ST__WAIT;
        }
      }
    break;
  }
}

/** @} */
