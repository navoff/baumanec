/**
 * @file leds.c
 * @brief
 * @author ������� �.�.
 * @date 23 ���� 2017 �.
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/io.h>

#include "config.h"
#include "pwm.h"
#include "leds.h"

//================================= Definition ================================

#define LED_AMOUNT        9

//================================ Declaration ================================

out_t out[LED_AMOUNT] = {
    #if defined(VER_LUSY)
    {&PORTD, (1 << PD1)},
    {&PORTD, (1 << PD2)},
    {&PORTD, (1 << PD3)},
    {&PORTD, (1 << PD5)},
    {&PORTD, (1 << PD6)},
    {&PORTB, (1 << PB6)},
    {&PORTB, (1 << PB7)},
    {&PORTC, (1 << PC4)},
    {&PORTC, (1 << PC5)},
    #else
    {&PORTD, (1 << PD1)},
    {&PORTD, (1 << PD2)},
    {&PORTD, (1 << PD4)},
    {&PORTD, (1 << PD5)},
    {&PORTD, (1 << PD6)},
    {&PORTD, (1 << PD7)},
    {&PORTB, (1 << PB0)},
    {&PORTB, (1 << PB6)},
    {&PORTB, (1 << PB7)},
    #endif
};

uint16_t led_timestamp = 0;
uint8_t led_pos = 0;

pwm_t pwm[LED_AMOUNT];

// ============================== Implementation ==============================

void pwm_all_init(void)
{
  for (uint8_t i = 0; i < LED_AMOUNT; i++)
  {
    pwm_init(&pwm[i]);
  }
}

void pwm_all_process(void)
{
  for (uint8_t i = 0; i < LED_AMOUNT; i++)
  {
    pwm_process(&pwm[i], &out[i]);
  }
}

void led_init(void)
{
  for (uint8_t i = 0; i < LED_AMOUNT; i++)
  {
    (*(out[i].port - 1)) |= out[i].pin;
  }
}

void led_set(uint32_t state)
{
  for (uint8_t i = 0; i < LED_AMOUNT; i++)
  {
    if (state & 0x01)
    {
      pwm[i].start_req = 1;
    }

    state >>= 1;
  }
}

void led_all_side_off(void)
{
  for (uint8_t i = 0; i < LED_AMOUNT; i++)
  {
    (*(out[i].port)) &= ~out[i].pin;
  }
}

void led_all_side_on(void)
{
  for (uint8_t i = 0; i < LED_AMOUNT; i++)
  {
    (*(out[i].port)) |= out[i].pin;
  }
}

void led_side_on(uint8_t idx)
{
  led_all_side_off();

#if defined(VER_LUSY)
  if (idx == 0)
  {
  }
  else if (idx == 1)
  {
    PORTD |= (1 << PD1);
  }
  else if (idx == 2)
  {
    PORTD |= (1 << PD2);
  }
  else if (idx == 3)
  {
    PORTD |= (1 << PD3);
  }
  else if (idx == 4)
  {
    PORTB |= (1 << PB6);
    PORTB |= (1 << PB7);
  }
  else if (idx == 5)
  {
    PORTD |= (1 << PD5);
    PORTD |= (1 << PD6);
  }
  else if (idx == 6)
  {
    PORTC |= (1 << PC4);
    PORTC |= (1 << PC5);
  }
  else
  {
    led_all_side_on();
  }
#else
  if (idx == 0)
  {
  }
  else if (idx == 1)
  {
    PORTD |= (1 << PD1);
  }
  else if (idx == 2)
  {
    PORTD |= (1 << PD2);
  }
  else if (idx == 3)
  {
    PORTD |= (1 << PD4);
  }
  else if (idx == 4)
  {
    PORTB |= (1 << PB6);
    PORTB |= (1 << PB7);
  }
  else if (idx == 5)
  {
    PORTD |= (1 << PD5);
    PORTD |= (1 << PD6);
  }
  else if (idx == 6)
  {
    PORTD |= (1 << PD7);
    PORTB |= (1 << PB0);
  }
  else
  {
    led_all_side_on();
  }
#endif
}

/** @} */
