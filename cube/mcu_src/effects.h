/**
 * @file effects.h
 * @brief
 * @author ������� �.�.
 * @date 30.05.2011
 * @addtogroup
 * @{
*/

#ifndef EFFECTS_H_
#define EFFECTS_H_

//================================= Definition ================================

typedef struct{
  uint8_t len;
  uint32_t tack[];
}unit_t;

typedef struct{
  uint8_t amount;
  uint8_t idx;
  unit_t *unit[];
}effects_t;

//================================ Declaration ================================

extern effects_t effects;

#endif /* EFFECTS_H_ */

/** @} */
