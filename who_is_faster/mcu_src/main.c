/**
 * @file main.c
 * @brief
 * @author ������� �.�.
 * @date 30.05.2011
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

//================================= Definition ================================

#define LED0_PL0_ON()       PORTB |= (1<<PB0)
#define LED0_PL0_OFF()      PORTB &= ~(1<<PB0)
#define LED1_PL0_ON()       PORTD |= (1<<PD7)
#define LED1_PL0_OFF()      PORTD &= ~(1<<PD7)
#define LED2_PL0_ON()       PORTD |= (1<<PD6)
#define LED2_PL0_OFF()      PORTD &= ~(1<<PD6)
#define LED3_PL0_ON()       PORTD |= (1<<PD5)
#define LED3_PL0_OFF()      PORTD &= ~(1<<PD5)
#define LED4_PL0_ON()       PORTB |= (1<<PB7)
#define LED4_PL0_OFF()      PORTB &= ~(1<<PB7)

#define LED4_PL1_ON()       PORTC |= (1<<PC1)
#define LED4_PL1_OFF()      PORTC &= ~(1<<PC1)
#define LED3_PL1_ON()       PORTC |= (1<<PC2)
#define LED3_PL1_OFF()      PORTC &= ~(1<<PC2)
#define LED2_PL1_ON()       PORTC |= (1<<PC3)
#define LED2_PL1_OFF()      PORTC &= ~(1<<PC3)
#define LED1_PL1_ON()       PORTC |= (1<<PC4)
#define LED1_PL1_OFF()      PORTC &= ~(1<<PC4)
#define LED0_PL1_ON()       PORTC |= (1<<PC5)
#define LED0_PL1_OFF()      PORTC &= ~(1<<PC5)

#define LED_MAIN_ON()       PORTC |= (1<<PC0)
#define LED_MAIN_SWITCH()   PORTC ^= (1<<PC0)
#define LED_MAIN_OFF()      PORTC &= ~(1<<PC0)

#define EXTERN_INT_EN()     GICR |= (1<<INT0)|(1<<INT1)
#define EXTERN_INT_DIS()    GICR &= ~((1<<INT0)|(1<<INT1))

//================================ Declaration ================================

void led_pl0_on(void);
void led_pl0_off(void);
void led_pl1_on(void);
void led_pl1_off(void);

void game_start();
void game_stop();

volatile struct{
  enum{
    GAME_ST__STOP,
    GAME_ST__PREPARE_DELAY,
    GAME_ST__WAIT_DELAY,
    GAME_ST__WAIT_PRESS,
  }state;
  uint16_t wait_led_timer;
  uint8_t player_0_idx;
  uint8_t player_1_idx;
}game;

// ============================== Implementation ==============================

int main (void)
{

  // ��������� ����� �����������
  DDRC |= (1<<PC5)|(1<<PC4)|(1<<PC3)|(1<<PC2)|(1<<PC1)|(1<<PC0);
  DDRB |= (1<<PB2)|(1<<PB1)|(1<<PB0)|(1<<PB7);
  DDRD |= (1<<PD7)|(1<<PD6)|(1<<PD5);

  // ��������� ������� ����������
  PORTD |= (1<<PD3)|(1<<PD2)|(1<<PD4); // ������������� ���������
  MCUCR |= (1<<ISC11)|(0<<ISC10)|(1<<ISC01)|(0<<ISC00); // �� ���������� ������

  /*
   * ��������� �������, ������������� ��� ������������ �������� �����
   * ������� ������.
   */
  OCR2 = 80;
  TCCR2 |= (1<<WGM21)|(1<<CS20);

  game_stop();

  uint8_t buf_pin = 0;

  sei();

  while(1)
  {
    switch(game.state)
    {
      case GAME_ST__PREPARE_DELAY:
        game.wait_led_timer = TCNT2 + 20;
        game.state = GAME_ST__WAIT_DELAY;
      break;

      case GAME_ST__WAIT_DELAY:
        if (game.wait_led_timer == 0)
        {
          game.state = GAME_ST__WAIT_PRESS;
          // �������� �����
          GIFR = (1<<INTF1)|(1<<INTF0);
          // ��������� ������� ����������
          EXTERN_INT_EN();

          LED_MAIN_ON();
        }
        else
        {
          game.wait_led_timer--;
          _delay_ms(50);
        }
      break;

      default:
      break;
    }

    if (bit_is_set(PIND,PD4))
    {
      buf_pin = 1;
      _delay_ms(1);
    }

    if (bit_is_clear(PIND,PD4) && (buf_pin == 1))
    {
      buf_pin = 0;
      if (game.state == GAME_ST__STOP)
      {
        game_start();
      }
      else
      {
        game_stop();
      }
    }
  } // end while
} // end main

void game_stop()
{
  game.state = GAME_ST__STOP;

  led_pl0_on();
  led_pl1_on();
  LED_MAIN_ON();

  EXTERN_INT_DIS();
}

void game_start()
{
  game.state = GAME_ST__PREPARE_DELAY;

  led_pl0_off();
  led_pl1_off();
  LED_MAIN_OFF();

  game.player_0_idx = 0;
  game.player_1_idx = 0;
}

/**
 * @brief ���������� ���������� ��� ������� ������ ������� ������.
 */
ISR(INT0_vect)
{
  game.player_0_idx++;
  game.state = GAME_ST__PREPARE_DELAY;
  LED_MAIN_OFF();

  switch (game.player_0_idx)
  {
    case 1: LED0_PL0_ON(); break;
    case 2: LED1_PL0_ON(); break;
    case 3: LED2_PL0_ON(); break;
    case 4: LED3_PL0_ON(); break;
    case 5:
      for (uint8_t i = 0; i < 4; i++){
        led_pl0_on();
        LED_MAIN_ON();
        _delay_ms(300);

        led_pl0_off();
        LED_MAIN_OFF();
        _delay_ms(300);
      }
      game_stop();
    break;
  }
  EXTERN_INT_DIS();
}

/**
 * @brief ���������� ���������� ��� ������� ������ ������� ������.
 */
ISR(INT1_vect){

  game.player_1_idx++;
  game.state = GAME_ST__PREPARE_DELAY;
  LED_MAIN_OFF();

  switch (game.player_1_idx)
  {
    case 1: LED0_PL1_ON(); break;
    case 2: LED1_PL1_ON(); break;
    case 3: LED2_PL1_ON(); break;
    case 4: LED3_PL1_ON(); break;
    case 5:
      for (uint8_t i = 0; i < 4; i++){
        led_pl1_on();
        LED_MAIN_ON();
        _delay_ms(300);

        led_pl1_off();
        LED_MAIN_OFF();
        _delay_ms(300);
      }
      game_stop();
    break;
  }
  EXTERN_INT_DIS();
}

void led_pl0_off(void)
{
  LED0_PL0_OFF();
  LED1_PL0_OFF();
  LED2_PL0_OFF();
  LED3_PL0_OFF();
  LED4_PL0_OFF();
}

void led_pl0_on (void)
{
  LED0_PL0_ON();
  LED1_PL0_ON();
  LED2_PL0_ON();
  LED3_PL0_ON();
  LED4_PL0_ON();
}

void led_pl1_off(void)
{
  LED0_PL1_OFF();
  LED1_PL1_OFF();
  LED2_PL1_OFF();
  LED3_PL1_OFF();
  LED4_PL1_OFF();
}

void led_pl1_on(void)
{
  LED0_PL1_ON();
  LED1_PL1_ON();
  LED2_PL1_ON();
  LED3_PL1_ON();
  LED4_PL1_ON();
}
/** @} */
