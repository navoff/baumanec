﻿namespace DMX_control
{
  partial class MainForm
  {
    /// <summary>
    /// Требуется переменная конструктора.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Освободить все используемые ресурсы.
    /// </summary>
    /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Код, автоматически созданный конструктором форм Windows

    /// <summary>
    /// Обязательный метод для поддержки конструктора - не изменяйте
    /// содержимое данного метода при помощи редактора кода.
    /// </summary>
    private void InitializeComponent()
    {
      this.portOpenCloseButton = new System.Windows.Forms.Button();
      this.portIsOpenPanel = new System.Windows.Forms.Panel();
      this.portComboBox = new System.Windows.Forms.ComboBox();
      this.button1 = new System.Windows.Forms.Button();
      this.strobeLightGroupBox = new System.Windows.Forms.GroupBox();
      this.strobeLightControl = new DMX_control.StrobeLightControl();
      this.strobeLightGroupBox.SuspendLayout();
      this.SuspendLayout();
      // 
      // portOpenCloseButton
      // 
      this.portOpenCloseButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.portOpenCloseButton.Location = new System.Drawing.Point(129, 5);
      this.portOpenCloseButton.Name = "portOpenCloseButton";
      this.portOpenCloseButton.Size = new System.Drawing.Size(81, 23);
      this.portOpenCloseButton.TabIndex = 17;
      this.portOpenCloseButton.Text = "Открыть";
      this.portOpenCloseButton.UseVisualStyleBackColor = true;
      this.portOpenCloseButton.Click += new System.EventHandler(this.portOpenCloseButton_Click);
      // 
      // portIsOpenPanel
      // 
      this.portIsOpenPanel.BackgroundImage = global::DMX_control.Properties.Resources.Closed;
      this.portIsOpenPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
      this.portIsOpenPanel.Location = new System.Drawing.Point(216, 5);
      this.portIsOpenPanel.Name = "portIsOpenPanel";
      this.portIsOpenPanel.Size = new System.Drawing.Size(25, 23);
      this.portIsOpenPanel.TabIndex = 19;
      // 
      // portComboBox
      // 
      this.portComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.portComboBox.FormattingEnabled = true;
      this.portComboBox.Location = new System.Drawing.Point(5, 5);
      this.portComboBox.Name = "portComboBox";
      this.portComboBox.Size = new System.Drawing.Size(118, 21);
      this.portComboBox.TabIndex = 18;
      this.portComboBox.DropDown += new System.EventHandler(this.portComboBox_DropDown);
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(360, 39);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(75, 23);
      this.button1.TabIndex = 20;
      this.button1.Text = "button1";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // strobeLightGroupBox
      // 
      this.strobeLightGroupBox.Controls.Add(this.strobeLightControl);
      this.strobeLightGroupBox.Location = new System.Drawing.Point(5, 39);
      this.strobeLightGroupBox.Name = "strobeLightGroupBox";
      this.strobeLightGroupBox.Size = new System.Drawing.Size(292, 244);
      this.strobeLightGroupBox.TabIndex = 22;
      this.strobeLightGroupBox.TabStop = false;
      this.strobeLightGroupBox.Text = "Стробоскоп";
      // 
      // strobeLightControl
      // 
      this.strobeLightControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.strobeLightControl.dmxStrobeLight = null;
      this.strobeLightControl.Location = new System.Drawing.Point(6, 19);
      this.strobeLightControl.Name = "strobeLightControl";
      this.strobeLightControl.Padding = new System.Windows.Forms.Padding(2);
      this.strobeLightControl.Size = new System.Drawing.Size(280, 219);
      this.strobeLightControl.TabIndex = 21;
      // 
      // MainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(600, 327);
      this.Controls.Add(this.strobeLightGroupBox);
      this.Controls.Add(this.button1);
      this.Controls.Add(this.portOpenCloseButton);
      this.Controls.Add(this.portIsOpenPanel);
      this.Controls.Add(this.portComboBox);
      this.Name = "MainForm";
      this.Padding = new System.Windows.Forms.Padding(2);
      this.Text = "Пульт управления DMX";
      this.Load += new System.EventHandler(this.MainForm_Load);
      this.strobeLightGroupBox.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button portOpenCloseButton;
    private System.Windows.Forms.Panel portIsOpenPanel;
    private System.Windows.Forms.ComboBox portComboBox;
    private System.Windows.Forms.Button button1;
    private StrobeLightControl strobeLightControl;
    private System.Windows.Forms.GroupBox strobeLightGroupBox;
  }
}

