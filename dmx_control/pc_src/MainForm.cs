﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Newtonsoft.Json;

namespace DMX_control
{
  public partial class MainForm : Form
  {
    private DmxController dmxController;

    public MainForm()
    {
      InitializeComponent();
    }

    private void MainForm_Load(object sender, EventArgs e)
    {
      strobeLightControl.dmxStrobeLight = new DmxStrobeLight(1);

      dmxController = new DmxController();
      dmxController.slaveDevices.Add(strobeLightControl.dmxStrobeLight);
      dmxController.slaveDevices.Add(new DmxSlaveDevice(21, DmxSlaveDevice.Type.TestDevice));

      string output = JsonConvert.SerializeObject(
        dmxController, Formatting.Indented);
      File.WriteAllText("dmx.json", output);
    }

    private void portOpenCloseButton_Click(object sender, EventArgs e)
    {
      if (dmxController.port.IsOpen)
      {
        try
        {
          dmxController.port.Close();
        }
        catch (IOException)
        {
          MessageBox.Show("Устройство отключено", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        catch (Exception)
        {
        }
      }
      else
      {
        try
        {
          dmxController.port.PortName = portComboBox.Text;
          dmxController.port.Open();
        }
        catch (UnauthorizedAccessException)
        {
          MessageBox.Show("Порт уже открыт другим приложением", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        catch (IOException)
        {
          MessageBox.Show("Устройство отключено", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        catch (ArgumentException)
        {
          MessageBox.Show("Выберите COM-порт", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        catch (Exception)
        {
        }
      }

      if (dmxController.port.IsOpen)
      {
        portOpenCloseButton.Text = "Закрыть";
        portIsOpenPanel.BackgroundImage = Properties.Resources.Opened;
      }
      else
      {
        portOpenCloseButton.Text = "Открыть";
        portIsOpenPanel.BackgroundImage = Properties.Resources.Closed;
      }
    }

    private void portComboBox_DropDown(object sender, EventArgs e)
    {
      List<string> names = Utils.GetPortNames();
      portComboBox.Items.Clear();
      foreach (string name in names)
      {
        portComboBox.Items.Add(name);
      }
    }

    private void button1_Click(object sender, EventArgs e)
    {
      //byte[] data = new byte[20];
      //for (int i = 0; i < data.Length; i++)
      //{
      //  data[i] = (byte)(0xAA + i);
      //}
      //dmxController.allChannelSet(data);
    }
  }
}
