﻿namespace DMX_control
{
  partial class StrobeLightControl
  {
    /// <summary> 
    /// Требуется переменная конструктора.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Освободить все используемые ресурсы.
    /// </summary>
    /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Код, автоматически созданный конструктором компонентов

    /// <summary> 
    /// Обязательный метод для поддержки конструктора - не изменяйте 
    /// содержимое данного метода при помощи редактора кода.
    /// </summary>
    private void InitializeComponent()
    {
      this.label1 = new System.Windows.Forms.Label();
      this.addressTextBox = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.speedScroll = new System.Windows.Forms.HScrollBar();
      this.label3 = new System.Windows.Forms.Label();
      this.dimmerTextBox = new System.Windows.Forms.TextBox();
      this.speedTextBox = new System.Windows.Forms.TextBox();
      this.panel = new System.Windows.Forms.Panel();
      this.dimmerScroll = new System.Windows.Forms.VScrollBar();
      this.SuspendLayout();
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(5, 6);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(41, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Адрес:";
      // 
      // addressTextBox
      // 
      this.addressTextBox.Location = new System.Drawing.Point(52, 3);
      this.addressTextBox.Name = "addressTextBox";
      this.addressTextBox.Size = new System.Drawing.Size(61, 20);
      this.addressTextBox.TabIndex = 1;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(2, 32);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(53, 13);
      this.label2.TabIndex = 0;
      this.label2.Text = "Яркость:";
      // 
      // speedScroll
      // 
      this.speedScroll.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.speedScroll.Location = new System.Drawing.Point(24, 228);
      this.speedScroll.Maximum = 255;
      this.speedScroll.Name = "speedScroll";
      this.speedScroll.Size = new System.Drawing.Size(325, 18);
      this.speedScroll.TabIndex = 4;
      // 
      // label3
      // 
      this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(5, 252);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(58, 13);
      this.label3.TabIndex = 3;
      this.label3.Text = "Скорость:";
      // 
      // dimmerTextBox
      // 
      this.dimmerTextBox.Location = new System.Drawing.Point(61, 29);
      this.dimmerTextBox.Name = "dimmerTextBox";
      this.dimmerTextBox.Size = new System.Drawing.Size(52, 20);
      this.dimmerTextBox.TabIndex = 5;
      // 
      // speedTextBox
      // 
      this.speedTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
      this.speedTextBox.Location = new System.Drawing.Point(69, 249);
      this.speedTextBox.Name = "speedTextBox";
      this.speedTextBox.Size = new System.Drawing.Size(81, 20);
      this.speedTextBox.TabIndex = 6;
      // 
      // panel
      // 
      this.panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.panel.BackColor = System.Drawing.Color.White;
      this.panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.panel.Location = new System.Drawing.Point(24, 55);
      this.panel.Name = "panel";
      this.panel.Size = new System.Drawing.Size(325, 170);
      this.panel.TabIndex = 7;
      this.panel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel_MouseDown);
      this.panel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel_MouseMove);
      this.panel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel_MouseUp);
      // 
      // dimmerScroll
      // 
      this.dimmerScroll.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
      this.dimmerScroll.Location = new System.Drawing.Point(5, 55);
      this.dimmerScroll.Maximum = 255;
      this.dimmerScroll.Name = "dimmerScroll";
      this.dimmerScroll.Size = new System.Drawing.Size(16, 170);
      this.dimmerScroll.TabIndex = 8;
      // 
      // StrobeLightControl
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.dimmerScroll);
      this.Controls.Add(this.panel);
      this.Controls.Add(this.speedTextBox);
      this.Controls.Add(this.dimmerTextBox);
      this.Controls.Add(this.speedScroll);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.addressTextBox);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.label1);
      this.Name = "StrobeLightControl";
      this.Padding = new System.Windows.Forms.Padding(2);
      this.Size = new System.Drawing.Size(354, 276);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label1;
    public System.Windows.Forms.TextBox addressTextBox;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.HScrollBar speedScroll;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox dimmerTextBox;
    private System.Windows.Forms.TextBox speedTextBox;
    private System.Windows.Forms.Panel panel;
    private System.Windows.Forms.VScrollBar dimmerScroll;

  }
}
