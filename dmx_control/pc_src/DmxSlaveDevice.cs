﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel;

namespace DMX_control
{
  [DataContract]
  public class DmxSlaveDevice
  {
    #region Fields
    [DataMember]
    public int address { get; set; }

    [DataMember]
    [JsonConverter(typeof(StringEnumConverter))]
    public Type type { get; set; }
    #endregion

    #region Constructors
    public DmxSlaveDevice(int address, Type type)
    {
      this.address = address;
      this.type = type;
    }
    #endregion

    public enum Type
    {
      [Description("Стробоскоп")]
      Stroboscope,
      TestDevice,
    }
  }

  public class DmxStrobeLight : DmxSlaveDevice
  {
    public int speed { get; set; }

    public int dimmer { get; set; }

    public DmxStrobeLight(int address)
      : base(address, Type.Stroboscope)
    {

    }
  }
}
