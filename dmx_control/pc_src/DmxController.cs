﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Reflection;
using System.IO.Ports;
using System.Timers;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DMX_control
{
  [DataContract]
  public class DmxController
  {
    #region Fields
    #endregion

    #region Properties
    public SerialPort port { get; set; }

    [DataMember]
    public List<DmxSlaveDevice> slaveDevices { get; set; }

    [DataMember]
    [JsonProperty(ItemConverterType = typeof(StringEnumConverter))]
    public List<DmxSlaveDevice.Type> enableTypes
    {
      get
      {
        return Enum.GetValues(typeof(DmxSlaveDevice.Type)).Cast<DmxSlaveDevice.Type>().ToList();
      }
    }
    #endregion

    #region Events And Delegates
    public event EventHandler PortDisconnectEvent;
    #endregion

    #region Constructors
    public DmxController()
    {
      port = new SerialPort("COM1", 2000000, Parity.None, 8, StopBits.One);
      port.DataReceived += new SerialDataReceivedEventHandler(dataReceivedHandler);

      slaveDevices = new List<DmxSlaveDevice>();
    }
    #endregion

    public void allChannelSet(byte[] data)
    {
      TbusPortPacket packet = new TbusPortPacket(TbusPortPacket.FrameID.AllChannelSet);
      packet.data = data;
      txPacket(packet);
    }

    protected void txPacket(TbusPortPacket packet)
    {
      if (!port.IsOpen) return;

      byte[] p = packet.get(true);
      try
      {
        port.Write(p, 0, p.Length);
      }
      catch (System.IO.IOException)
      {
        if (PortDisconnectEvent != null)
        {
          PortDisconnectEvent(this, new EventArgs());
        }
        return;
      }
      catch (Exception)
      {
      }
    }

    public void txPacket(TbusPortPacket[] packets)
    {
      if (!port.IsOpen) return;

      int size = 0;
      foreach (TbusPortPacket packet in packets)
      {
        size += packet.get(true).Length;
      }

      byte[] p = new byte[size];
      int idx = 0;
      foreach (TbusPortPacket packet in packets)
      {
        Array.ConstrainedCopy(packet.get(true), 0, p, idx, packet.get(true).Length);
        idx += packet.get(true).Length;
      }

      try
      {
        port.Write(p, 0, p.Length);
      }
      catch (System.IO.IOException)
      {
        if (PortDisconnectEvent != null)
        {
          PortDisconnectEvent(this, new EventArgs());
        }
      }
      catch (Exception)
      {
      }
    }

    void ProcessDataReceivedTbus(SerialPort port)
    {
      byte[] a;
      a = new byte[port.BytesToRead];
      port.Read(a, 0, a.Length);
    }

    /// <summary>
    /// Обработчик приёма байт с порта.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void dataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
    {
      try
      {
        ProcessDataReceivedTbus((SerialPort)sender);
      }
      catch (Exception)
      {
      }
    }
  }
}
