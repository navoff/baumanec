﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Reflection;
using System.IO;
using System.IO.Ports;
using System.Text.RegularExpressions;

namespace DMX_control
{
  public class Utils
  {
    public static bool CheckTimestamp(DateTime stamp, int ms)
    {
      return (((TimeSpan)(DateTime.Now - stamp)).TotalMilliseconds >= ms);
    }

    public static string GetDescription(object enumValue)
    {
      string defDesc = "";
      FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());

      if (null != fi)
      {
        object[] attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);
        if (attrs != null && attrs.Length > 0)
        {
          defDesc = ((DescriptionAttribute)attrs[0]).Description;
        }
      }

      return defDesc;
    }

    private static byte crc8_byte(byte sum, byte data)
    {
      byte res = (byte)(sum ^ data);

      for (int i = 0; i < 8; i++)
      {
        res = (byte)((res & 0x80) != 0 ? (res << 1) ^ 0x31 : res << 1);
      }

      return res;
    }

    public static byte crc8(byte[] data, int size)
    {
      byte res = 0x5A;

      for (int i = 0; i < size; i++)
      {
        res = crc8_byte(res, data[i]);
      }

      return res;
    }

    public static List<string> GetPortNames()
    {
      List<string> names = SerialPort.GetPortNames().ToList();

      for (int i = 0; i < names.Count; i++)
      {
        string name = Regex.Replace(names[i], @"[^\u0000-\u007F]", string.Empty);
        names[i] = name;
      }

      return names;
    }
  }
}
