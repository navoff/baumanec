﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Reflection;

namespace DMX_control
{
  public class TbusPortPacket
  {
    #region Properties
    public FrameID frameID { get; set; }
    public byte[] data { get; set; }
    public ErrorType errorType { get; set; }
    public static int maxDataLength = 64;
    #endregion

    #region Feilds
    private byte[] rawData;
    #endregion

    #region Constructor
    public TbusPortPacket()
    {
      data = new byte[0];
    }

    public TbusPortPacket(FrameID frameID)
    {
      this.frameID = frameID;
      data = new byte[0];
    }

    public TbusPortPacket(FrameID frameID, byte value)
    {
      this.frameID = frameID;
      data = new byte[1];
      data[0] = value;
    }
    #endregion

    #region Implimentation
    public enum FrameID : byte
    {
      [Description("Установить значение всех каналов")]
      AllChannelSet = 0x01,
    };


    public enum ErrorType
    {
      [Description("Ошибки нет")]
      OK,
      [Description("Не верная КС")]
      CRC,
      [Description("Слишком мальнькая длинна пакета")]
      ToShort,
    }

    public string getString()
    {
      byte[] p;
      if (errorType == ErrorType.OK)
      {
        p = get(false);
      }
      else
      {
        p = rawData;
      }

      string res = "";

      foreach (byte data in p)
      {
        res += "0x" + String.Format("{0:X2}", data) + " ";
      }
      return res;
    }

    public string getDescription()
    {
      string res;

      if (errorType == ErrorType.OK)
      {
        res = Enum.GetName(typeof(TbusPortPacket.FrameID), frameID);
      }
      else
      {
        res = "В пакете ошибка";
      }

      return res;
    }

    public string getComment()
    {
      string res;

      if (errorType == ErrorType.OK)
      {
        res = Utils.GetDescription(frameID);

        switch (frameID)
        {
          case FrameID.AllChannelSet:
            break;
        }
      }
      else
      {
        res = Utils.GetDescription(errorType);
      }

      return res;
    }

    public void set(Queue<byte> rx_fifo)
    {
      byte crc = 0;
      rawData = new byte[rx_fifo.Count];
      for (int i = 0; i < rawData.Length; i++)
      {
        rawData[i] = rx_fifo.Dequeue();
      }

      crc = Utils.crc8(rawData, rawData.Length);

      if (rawData.Length < 2)
      {
        errorType = ErrorType.ToShort;
      }
      else if (crc != 0)
      {
        errorType = ErrorType.CRC;
      }
      else
      {
        frameID = (FrameID)rawData[0];

        data = new byte[rawData.Length - 2];
        for (int i = 0; i < data.Length; i++)
        {
          data[i] = rawData[1 + i];
        }
        errorType = ErrorType.OK;
      }
    }

    public byte[] get(bool with_staffing)
    {
      int idx = 0;
      byte[] res = new byte[data.Length + 2];

      res[idx++] = (byte)frameID;

      for (int i = 0; i < data.Length; i++)
      {
        res[idx++] = data[i];
      }

      res[idx] = Utils.crc8(res, idx);
      idx++;

      if (with_staffing)
      {
        List<byte> tx_list = new List<byte>();
        tx_list.Add(0xAA);
        foreach (byte tx_byte in res)
        {
          if ((tx_byte == 0xAA) || (tx_byte == 0xA8))
          {
            tx_list.Add(0xA8);
            tx_list.Add((byte)(tx_byte ^ 0x01));
          }
          else
          {
            tx_list.Add(tx_byte);
          }
        }
        tx_list.Add(0xAA);

        res = tx_list.ToArray();
      }

      return res;
    }
    #endregion
  }
}
