﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DMX_control
{
  public partial class StrobeLightControl : UserControl
  {
    private DmxStrobeLight _dmxStrobeLight;

    private bool isPositionChanging;

    public DmxStrobeLight dmxStrobeLight 
    {
      get { return _dmxStrobeLight; }
      set
      {
        _dmxStrobeLight = value;

        if (_dmxStrobeLight == null) return;

        addressTextBox.DataBindings.Add(
          "Text", _dmxStrobeLight, "address", false, DataSourceUpdateMode.OnPropertyChanged);


        dimmerScroll.DataBindings.Add(
          "Value", _dmxStrobeLight, "dimmer", false, DataSourceUpdateMode.OnPropertyChanged);
        dimmerTextBox.DataBindings.Add(
          "Text", _dmxStrobeLight, "dimmer", false, DataSourceUpdateMode.OnPropertyChanged);

        speedScroll.DataBindings.Add(
          "Value", _dmxStrobeLight, "speed", false, DataSourceUpdateMode.OnPropertyChanged);
        speedTextBox.DataBindings.Add(
          "Text", _dmxStrobeLight, "speed", false, DataSourceUpdateMode.OnPropertyChanged);
      }
    }

    public StrobeLightControl()
    {
      InitializeComponent();
    }

    private int getSpeedFromMouse(Point location)
    {
      int res = Math.Max(Math.Min(location.X, panel.Width), 0);
      res = res * 255 / panel.Width;
      return res;
    }

    private int getDimmerFromMouse(Point location)
    {
      int res = Math.Max(Math.Min(panel.Height - location.Y, panel.Height), 0);
      res = res * 255 / panel.Height;
      return res;
    }

    private void panel_MouseDown(object sender, MouseEventArgs e)
    {
      isPositionChanging = true;

      speedScroll.Value = getSpeedFromMouse(e.Location);
      dimmerScroll.Value = getDimmerFromMouse(e.Location);
    }

    private void panel_MouseMove(object sender, MouseEventArgs e)
    {
      if (isPositionChanging)
      {
        speedScroll.Value = getSpeedFromMouse(e.Location);
        dimmerScroll.Value = getDimmerFromMouse(e.Location);
      }
      //cursor = e.Location;
    }

    private void panel_MouseUp(object sender, MouseEventArgs e)
    {
      isPositionChanging = false;
    }
  }
}
