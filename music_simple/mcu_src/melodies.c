/**
 * @file melodies.c
 * @brief
 * @author ������� �.�.
 * @date 30.05.2011
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/io.h>
#include <avr/pgmspace.h>

#include "melodies.h"

//================================= Definition ================================

//================================ Declaration ================================

// ============================== Implementation ==============================

PROGMEM Nota_Type Melody_Silence[]={
	{Silence, SQ},
	{0xffff, 0xff},
};

PROGMEM Nota_Type Melody_March[] = {
	{ G1, SEM },
	{ Silence, SQ },
	{ G1, SEM },
	{ Silence, SQ },
	{ G1, SEM },
	{ Silence, SQ },
	{ Ddi1, QV },
	{ Ddi1, Cr },
	{ Silence, SQ },
	{ Adi1, Cr },

	{ Silence, SQ },
	{ G1, SEM },
	{ Silence, SQ },
	{ Ddi1, QV },
	{ Ddi1, Cr },
	{ Silence, SQ },
	{ Adi1, Cr },
	{ Silence, SQ },
	{ G1, CEL },
	{ Silence, SQ },

	{ D2, SEM },
	{ Silence, SQ },
	{ D2, SEM },
	{ Silence, SQ },
	{ D2, SEM },
	{ Silence, SQ },
	{ Ddi2, QV },
	{ Ddi2, Cr },
	{ Silence, SQ },
	{ Adi1, Cr },

	{ Silence, SQ },
	{ Fdi1, SEM },
	{ Silence, SQ },
	{ Ddi1, QV },
	{ Ddi1, Cr },
	{ Silence, SQ },
	{ Adi1, Cr },
	{ Silence, SQ },
	{ G1, CEL },
	{ Silence, SQ },

	{ G2, SEM },
	{ Silence, SQ },
	{ G1, QV },
	{ G1, Cr },
	{ Silence, SQ },
	{ G1, Cr },
	{ Silence, SQ },
	{ G2, SEM },
	{ Silence, SQ },
	{ Fdi2, QV },

	{ Fdi2, Cr },
	{ Silence, SQ },
	{ F2, Cr },
	{ Silence, SQ },
	{ E2, Cr },
	{ Silence, SQ },
	{ Ddi2, Cr },
	{ Silence, SQ },
	{ E2, QV },
	{ Silence, QV },

	{ Gdi1, QV },
	{ Silence, SQ },
	{ Cdi2, SEM },
	{ Silence, SQ },
	{ C2, QV },
	{ C2, Cr },
	{ Silence, SQ },
	{ H1, Cr },
	{ Silence, SQ },
	{ Adi1, Cr },

	{ Silence, SQ },
	{ A1, Cr },
	{ Silence, SQ },
	{ Adi1, QV },
	{ Silence, QV },
	{ Ddi1, QV },
	{ Silence, SQ },
	{ Fdi1, SEM },
	{ Silence, SQ },
	{ Ddi1, QV },

	{ Ddi1, Cr },
	{ Silence, SQ },
	{ A1, QV },
	{ Silence, SQ },
	{ G1, SEM },
	{ Silence, SQ },
	{ Ddi1, QV },
	{ Ddi1, Cr },
	{ Silence, SQ },
	{ A1, QV },

	{ Silence, SQ },
	{ G1, CEL },
	{ Silence, SEM },
	{ 0xffff, 0xff},
};

PROGMEM Nota_Type Melody_Pantera[] ={
	{ Ddi1, Cr },
	{ E1, SEM },
	{ Silence, SEM },
	{ Fdi1, Cr },
	{ G1, SEM },
	{ Silence, SEM },
	{ Ddi1, Cr },
	{ E1, QV },
	{ Fdi1, Cr },
	{ G1, QV }, // 10
	{ C2, Cr },
	{ H1, QV },
	{ E1, Cr },
	{ G1, QV },
	{ H1, Cr },
	{ Bdi1, SEM },
	{ Silence, QV },
	{ B1, Cr },
	{ A1, Cr },
	{ E1, Cr }, // 20
	{ D1, Cr },
	{ E1, Cr },
	{ E1, SEM },
	{ Silence, SEM },
	{ 0xffff, 0xff},
};

PROGMEM Nota_Type Melody_Kelize[] ={
	{ Silence, SQ },
	{ E2, Cr },
	{ Ddi2, Cr },

	{ E2, Cr },
	{ Ddi2, Cr },
	{ E2, Cr },
	{ H1, Cr },
	{ D2, Cr },
	{ C2, Cr },

	{ A1, QV }, // 10
	{Silence, QV},
	{ C1, Cr },
	{ E1, Cr },
	{ A1, Cr },

	{ H1, QV },
	{Silence, Cr},
	{E1, Cr},
	{Gdi1, Cr},
	{H1, Cr},

	{C2, QV}, // 20
	{Silence, Cr},
	{E1, Cr},
	{E2, Cr},
	{Ddi2, Cr},

	{ E2, Cr },
	{ Ddi2, Cr },
	{ E2, Cr },
	{ H1, Cr },
	{ D2, Cr },
	{ C2, Cr }, // 30

	{ A1, QV },
	{ Silence, QV},
	{ C1, Cr },
	{ E1, Cr },
	{ A1, Cr },

	{ H1, QV},
	{ Silence, Cr},
	{ E1, Cr },
	{ C2, Cr },
	{ H1, Cr }, // 40

	{ A1, SEM },
	{ 0xffff, 0xff},
};

PROGMEM Nota_Type Melody_Bumer[] ={
	{ E2, Cr },
	{ G2, SEM },
	{ Silence, SEM },
	{ G2, Cr },
	{ E2, SEM },
	{ Silence, SEM },
	{ A2, Cr },
	{ G2, Cr },
	{ A2, Cr },
	{ G2, Cr },
	{ A2, Cr },
	{ G2, Cr },
	{ A2, Cr },
	{ G2, Cr },
	{ A2, Cr },
	{ H2, QV },
	{ Silence, SEM },
	{ E2, Cr },
	{ G2, QV },
	{ Silence, SEM },
	{ G2, Cr },
	{ E2, QV },
	{ Silence, SEM },
	{ A2, Cr },
	{ G2, Cr },
	{ A2, Cr },
	{ G2, Cr },
	{ A2, Cr },
	{ G2, Cr },
	{ A2, Cr },
	{ G2, Cr },
	{ A2, Cr },
	{ H2, QV },
	{ Silence, SEM },
	{ 0xffff, 0xff},
};

PROGMEM Nota_Type Melody_Dzeltentem[] ={
	{ Silence, SQ },
	{ E2, Cr },
	{ F2, Cr },
	{ E2, Cr },
	{ Ddi2, Cr },
	{ E2, Cr },
	{ Silence, SQ },
	{ C2, QV },
	{ Silence, SEM },

	{ E2, Cr },
	{ F2, Cr },
	{ E2, Cr },
	{ Ddi2, Cr },
	{ E2, Cr },
	{ Silence, SQ},
	{ H1, QV},
	{ Silence, SEM },

	{ E2, Cr },
	{ C3, Cr },
	{ H2, Cr },
	{ A2, Cr },

	{ E2, Cr },
	{ G2, Cr },
	{ Silence, Qu },
	{ F2, Qu },
	{ E2, Cr },
	{ F2, Cr },
	{ E2, Qu },
	{ F2, Qu },
	{ E2, Qu },
	{ D2, SEM },
	{ Silence, SEM },
	{ 0xffff, 0xff},
};

PROGMEM Nota_Type Melody_Slavyanka[]={
	{ F2, SEM },
	{ Cdi2, SEM },
	{ C2, SEM },
	{ Adi1, QV },
	{ Adi1, Cr },
	{ G1, QV },
	{ Gdi1, QV },
	{ C2, QV },
	{ G1, QV },
	{ C2, QV },
	{ F1, QV },
	{ Silence, QV },

	{ C2, QV },
	{ C2, Cr },
	{ Cdi2, Cr },
	{ C2, SEM },
	{ Gdi1, QV },
	{ Gdi1, Cr },
	{ G1, Cr },
	{ F1, SEM },

	{ E1, QV },
	{ F1, QV },
	{ G1, SEM },
	{ G1, QV },
	{ E1, QV },
	{ C1, QV },

	{ Silence, QV },

	{ C2, QV },
	{ C2, Cr },
	{ Cdi2, Cr },
	{ C2, SEM },

	{ Ddi1, QV },
	{ Ddi1, Cr },
	{ G1, Cr },
	{ C2, QV },

	{ Silence, QV },

	{ G1, QV },
	{ G1, Cr },
	{ Gdi1, QV },
	{ F1, SEM },
	{ F1, QV },

	{ Silence, SQ },
	{ F1, QV },
	{ F1, Cr },
	{ E1, Cr },
	{ G1, SEM },
	{ F1, QV },
	{ Cdi1, QV },
	{ Adi0, SEM },
	{ Cdi1, QV },
	{ F1, QV },
	{ C1, SEM },
	{ C1, QV },
	{ A0, QV },
	{ F0, SEM },

	{ Silence, Qu },

	{ C1, QV },
	{ C1, Cr },
	{ Cdi1, Cr },
	{ C1, QV },
	{ C1, Cr },

	{ Silence, Cr },

	{ Ddi1, QV },
	{ Ddi1, Cr },
	{ G1, Cr },
	{ C2, QV },
	{ C2, Cr },

	{ Silence, Cr },

	{ G1, QV },
	{ G1, Cr },
	{ Gdi1, Cr },
	{ F1, CEL },

	{ Silence, CEL },
	{ 0xffff, 0xff},
};

Nota_Type* Melodies[Melody_Amount]={
	Melody_Silence,
	Melody_March,
	Melody_Bumer,
	Melody_Dzeltentem,
	Melody_Kelize,
	Melody_Pantera,
//	Melody_Slavyanka,
};

/** @} */
