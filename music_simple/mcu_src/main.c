/**
 * @file main.c
 * @brief
 * @author ������� �.�.
 * @date 30.05.2011
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/pgmspace.h>
#include <util/delay.h>

#include "melodies.h"

//================================= Definition ================================

//================================ Declaration ================================

// ============================== Implementation ==============================

int main (void){

	// ��� ����������� ���������
	DDRB |= (1<<PB1);

	// ��� ��� ������
	PORTD |= (1<<PD4);

	/*
	 * ��������� �������.
	 * ��� �������. ����� ��� ����������. ������� ����� ��� ����������.
	 */
	OCR1A = 0;
	TCCR1A = (0<<COM1A1)|(1<<COM1A0)|(0<<WGM11)|(0<<WGM10);
	TCCR1B = (0<<WGM13)|(1<<WGM12)|(0<<CS22)|(0<<CS21)|(1<<CS20);

	uint16_t counter = 0;
	uint8_t Melody_idx = 0;
	uint8_t buf_pin = 0;
	Nota_Type *p_Current_nota = (Nota_Type *)Melodies[Melody_idx];

	while(1)
	{

		// ������� ���� �� flash
		uint16_t freq = pgm_read_word(&p_Current_nota[counter].freq);
		uint8_t duration = pgm_read_byte(&p_Current_nota[counter].paus);

		// ���� ������� �����������
		if ((freq == 0xffff) && (duration == 0xff))
		{
			counter = 0;
			continue;
		}

		counter++;

		// ������ ����
		OCR1A = freq;
		_delay_ms(duration*15);

		if (bit_is_set(PIND,PD4))
		{
			buf_pin = 1;
		}

		if (bit_is_clear(PIND,PD4) && (buf_pin == 1))
		{
		  buf_pin = 0;
			// ������� � ��������� �������
			counter = 0;
			Melody_idx++;
			if (Melody_idx >= Melody_Amount){
				Melody_idx = 0;
			}
			p_Current_nota = Melodies[Melody_idx];
		}
	} // end while
}

/** @} */
