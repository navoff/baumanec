/**
 * @file melodies.h
 * @brief
 * @author ������� �.�.
 * @date 30.05.2011
 * @addtogroup
 * @{
*/

#ifndef MELODIES_H_
#define MELODIES_H_

//================================= Definition ================================

#define	CEL		64	//	1/1
#define SEM		32	//	1/2
#define QV		16	//	1/4
#define Cr		8	//	1/8
#define Qu		4	//	1/16
#define SQ		2	//	1/32

#define Silence	0


#define F0		37432	// ��
#define G0		35718	// ����
#define A0		34004	// ��
#define Adi0	33147
#define B0		32290	// ��

#define C1		30576	// ��
#define Cdi1	28862
#define D1		27058	// ��
#define Ddi1	25714
#define E1		24270	// ��
#define F1		22907	// ��
#define Fdi1	21622
#define G1		20408	// ����
#define Gdi1	19324
#define A1		18182	// ��
#define Adi1	17671
#define B1		17161	// ��
#define Bdi1	16677
#define H1		16194	// ��

#define C2		15289
#define Cdi2	14431
#define D2		13621
#define Ddi2	12856
#define E2		12135
#define F2		11454
#define Fdi2	10811
#define G2		10204
#define Gdi2	9632
#define A2		9091
#define B2		8581
#define H2		8099

#define C3		7645

#define Melody_Amount		6

typedef struct{
	uint16_t freq;
	uint8_t paus;
}Nota_Type;

//================================ Declaration ================================

extern Nota_Type* Melodies[];

#endif /* MELODIES_H_ */

/** @} */
