/**
 * @file ir.c
 * @brief
 * @author ������� �.�.
 * @date 11.06.2016
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#include "ir.h"

//================================= Definition ================================

//================================ Declaration ================================

volatile uint8_t tx_is_on;

// ============================== Implementation ==============================

void ir_tx_init(void)
{
  // ���������� �����������
  DDRB |= (1 << PB0);

  TIFR = (1 << OCF2);
  TIMSK |= (1 << OCIE2);

  TCNT2 = 0;
  OCR2 = 111;
  TCCR2 = (1 << WGM21) | (0 << WGM20) | (0 << CS22) | (0 << CS21) | (1 << CS20);

  tx_is_on = 0;
}

void ir_tx_on(void)
{
  PORTB |= (1 << PB0);
  tx_is_on = 1;
}

void ir_tx_off(void)
{
  tx_is_on = 0;
  PORTB &= ~(1 << PB0);
}

ISR(TIMER2_COMP_vect)
//ISR(_VECTOR(2))
{
  if (tx_is_on)
  {
    PORTB ^= (1 << PB0);
  }
}

/** @} */
