/**
 * @file tick_timer.h
 * @brief
 * @author ������� �.�.
 * @date 23.05.2012
 * @addtogroup
 * @{
*/

#ifndef TICK_TIMER_H_
#define TICK_TIMER_H_

//================================= Definition ================================

#define TICK_PERIOD_100_US
#define MS_TO_TICKS(ms)                 (ms * 10)
#define START_TIMER(timestamp)          (timestamp = tick_timer)
#define CHECK_TIMER(timestamp, period)  (tick_timer >= (timestamp + period))

//================================ Declaration ================================

void tick_init(void);
uint8_t tick_cnt(void);
void tick_cnt_dec(void);

extern uint16_t tick_timer;

#endif /* TICK_TIMER_H_ */

/** @} */
