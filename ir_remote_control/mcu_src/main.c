/**
 * @file main.c
 * @brief
 * @author ������� �.�.
 * @date 14.06.2016
 * @addtogroup
 * @{
*/

#include <stdint.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "tick_timer.h"
#include "ir.h"

//================================= Definition ================================

#define BTN1_DDR              DDRD
#define BTN1_PORT             PORTD
#define BTN1_PIN              PIND
#define BTN1_P                PD3
#define BTN1_IS_ACTIVE()      bit_is_clear(BTN1_PIN, BTN1_P)

#define BTN2_DDR              DDRD
#define BTN2_PORT             PORTD
#define BTN2_PIN              PIND
#define BTN2_P                PD4
#define BTN2_IS_ACTIVE()      bit_is_clear(BTN2_PIN, BTN2_P)

#define TX_AMOUNT             5

typedef struct{
  enum{
    IR_CONTROL_ST__WAIT_ACTIVE,
    IR_CONTROL_ST__WAIT_DEACTIVE,
    IR_CONTROL_ST__BTN_WAIT_ACTIVE,
    IR_CONTROL_ST__TX_ON,
    IR_CONTROL_ST__TX_OFF,
  } state;
  uint16_t period;
  uint8_t idx;
  uint8_t tx_idx;
  uint16_t timestamp;
} ir_control_t;

//================================ Declaration ================================

ir_control_t ir_control;

// ============================== Implementation ==============================

int main(void)
{
  // ������ ����������
  BTN1_PORT |= (1 << BTN1_P);
  BTN2_PORT |= (1 << BTN2_P);

  ir_tx_init();
  tick_init();

  sei();

  while(1)
  {
    if (tick_cnt())
    {
      tick_cnt_dec();
    }

    switch (ir_control.state)
    {
      case IR_CONTROL_ST__WAIT_ACTIVE:
        if (BTN1_IS_ACTIVE())
        {
          START_TIMER(ir_control.timestamp);
          ir_control.idx = 0;
          ir_control.state = IR_CONTROL_ST__BTN_WAIT_ACTIVE;
        }

        if (BTN2_IS_ACTIVE())
        {
          START_TIMER(ir_control.timestamp);
          ir_control.idx = 1;
          ir_control.state = IR_CONTROL_ST__BTN_WAIT_ACTIVE;
        }
      break;

      case IR_CONTROL_ST__WAIT_DEACTIVE:
        if ((BTN1_IS_ACTIVE() == 0) && (BTN2_IS_ACTIVE() == 0))
        {
          ir_control.state = IR_CONTROL_ST__WAIT_ACTIVE;
        }
      break;

      case IR_CONTROL_ST__BTN_WAIT_ACTIVE:
        if (ir_control.idx == 0)
        {
          if (BTN1_IS_ACTIVE() == 0)
          {
            ir_control.state = IR_CONTROL_ST__WAIT_ACTIVE;
            break;
          }
        }

        if (ir_control.idx == 1)
        {
          if (BTN2_IS_ACTIVE() == 0)
          {
            ir_control.state = IR_CONTROL_ST__WAIT_ACTIVE;
            break;
          }
        }

        if (CHECK_TIMER(ir_control.timestamp, 50))
        {
          ir_control.period = ir_control.idx * 10 + 10;
          ir_control.tx_idx = 0;
          START_TIMER(ir_control.timestamp);
          ir_control.state = IR_CONTROL_ST__TX_OFF;
        }
      break;

      case IR_CONTROL_ST__TX_OFF:
        if (CHECK_TIMER(ir_control.timestamp, ir_control.period))
        {
          ir_tx_on();
          START_TIMER(ir_control.timestamp);
          ir_control.state = IR_CONTROL_ST__TX_ON;
        }
      break;

      case IR_CONTROL_ST__TX_ON:
        if (CHECK_TIMER(ir_control.timestamp, ir_control.period))
        {
          ir_tx_off();
          ir_control.tx_idx++;

          if (ir_control.tx_idx >= TX_AMOUNT)
          {
            ir_control.state = IR_CONTROL_ST__WAIT_DEACTIVE;
          }
          else
          {
            START_TIMER(ir_control.timestamp);
            ir_control.state = IR_CONTROL_ST__TX_OFF;
          }
        }
      break;
    }
  }
}

/** @} */
